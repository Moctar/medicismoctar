package sn.org.seysoo.medicis.test;
import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.importer.ZipImporter;

public class Deployments {
   public static EnterpriseArchive medicisDeployment() {
      return ShrinkWrap.create(ZipImporter.class, "medicis.ear").importFrom(new File("../medicis-ear/target/medicis.ear"))
            .as(EnterpriseArchive.class);
   }
}