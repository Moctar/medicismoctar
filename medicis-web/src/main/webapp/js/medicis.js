/*********************************************************
 * Fonction qui execute l'action click d'un bouton (cach�) 
 *********************************************************/
function execAction(cmdId) {
	var fireOnThis = document.getElementById(cmdId);
	if (document.createEvent) {
		var evObj = document.createEvent('MouseEvents');
		evObj.initEvent( 'click', true, false );
		fireOnThis.dispatchEvent(evObj);
	}
	else if (document.createEventObject) {
		fireOnThis.fireEvent('onclick');
	}
}
