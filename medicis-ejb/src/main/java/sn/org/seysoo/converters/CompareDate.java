/*
  Auteur : Mouhamadou
  Projet : medicis-ejb
  Fichier : sn.org.seysoo.converters.CompareDate.java
  Date : 22 janv. 2014

 * ================================================================================================================================
 * Copyright (c) 2013 by Seysoo SARL,
 * seysoo.com
 * République du Senegal
 * 
 * This software is the propertary information of Seysoo
 * All rights reserved

 */
package sn.org.seysoo.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HP PROBOOK
 *
 */
public class CompareDate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			 
    		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        	Date date1 = sdf.parse("07-01-2014");
        	Date date2 = sdf.parse("10-01-2014");
        	Date date3 = sdf.parse("10-01-2014");
 
        	System.out.println(sdf.format(date1));
        	System.out.println(sdf.format(date2));
        	System.out.println(sdf.format(date3));
 
        	/*if(date1.compareTo(date2)>0){
        		System.out.println("Date1 is after Date2");
        	}else if(date1.compareTo(date2)<0){
        		System.out.println("Date1 is before Date2");
        	}else if(date1.compareTo(date2)==0){
        		System.out.println("Date1 is equal to Date2");
        	}else{
        		System.out.println("How to get here?");
        	}*/
        	
        	if(date2.compareTo(date3)>=0)
        		System.out.println("OKKKKKKKK");
        	else
        		System.out.println("NnnnnnnnnnnnnnnnN");
        		
 
    	}catch(ParseException ex){
    		ex.printStackTrace();
    	}

	}

}
