package sn.org.seysoo.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.entity.ExamenBean;

@Name("examenConverter")
@Converter(forClass = ExamenBean.class)
@BypassInterceptors
public class ExamenConverter implements javax.faces.convert.Converter
{

	public Object getAsObject(FacesContext context, UIComponent component, String value)
	{
		if (value != null)
		{
			try
			{
				Integer id = Integer.parseInt(value);
				if (id != null)
				{
					IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
							.getInstance("diagnosticService");
					ExamenBean examen = serviceDiagnostic.findExamen(id);
					return examen;
				}
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value)
	{
		if (value instanceof ExamenBean)
		{
			ExamenBean examen = (ExamenBean) value;
			return examen.getId().toString();
		}
		else
		{
			return null;
		}
	}

}
