package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;

import javax.ejb.Local;

import sn.org.seysoo.medicis.entity.BanqueBean;



@Local
public interface IBanqueService {

	
	public List <BanqueBean> getAllBanques ();
}
