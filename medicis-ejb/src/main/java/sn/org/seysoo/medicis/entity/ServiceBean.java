package sn.org.seysoo.medicis.entity;

// Generated 9 oct. 2013 11:24:03 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.search.annotations.Indexed;
import org.jboss.seam.annotations.Name;
import org.jboss.solder.core.Veto;

/**
 * Service generated by hbm2java
 */
@Entity
@Veto
@Indexed
@Table(name = "service")
@Name("serviceBean")
public class ServiceBean implements java.io.Serializable {

	private Integer id;
	private String libelle;
	private String qualificatif;
	private String etat;
	private Set<MotifBean> motifs = new HashSet<MotifBean>(0);
	private Set<SalleBean> salles = new HashSet<SalleBean>(0);
	private Set<GroupeExamenBean> groupeExamens = new HashSet<GroupeExamenBean>(0);
	private Set<MedecinBean> medecins = new HashSet<MedecinBean>(0);

	public ServiceBean() {
	}

	public ServiceBean(String libelle, String qualificatif, String etat) {
		this.libelle = libelle;
		this.qualificatif = qualificatif;
		this.etat = etat;
	}

	public ServiceBean(String libelle, String qualificatif, String etat,
			Set<MotifBean> motifs, Set<SalleBean> salles,
			Set<GroupeExamenBean> groupeExamens, Set<MedecinBean> medecins) {
		this.libelle = libelle;
		this.qualificatif = qualificatif;
		this.etat = etat;
		this.motifs = motifs;
		this.salles = salles;
		this.groupeExamens = groupeExamens;
		this.medecins = medecins;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "libelle", nullable = false, length = 50)
	@NotNull
	@Size(max = 50)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Column(name = "qualificatif", nullable = false, length = 50)
	@NotNull
	@Size(max = 50)
	public String getQualificatif() {
		return this.qualificatif;
	}

	public void setQualificatif(String qualificatif) {
		this.qualificatif = qualificatif;
	}

	@Column(name = "etat", nullable = false, length = 50)
	@NotNull
	@Size(max = 50)
	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	@OneToMany(mappedBy = "service")
	public Set<MotifBean> getMotifs() {
		return this.motifs;
	}

	public void setMotifs(Set<MotifBean> motifs) {
		this.motifs = motifs;
	}

	@OneToMany(mappedBy = "service")
	public Set<SalleBean> getSalles() {
		return this.salles;
	}

	public void setSalles(Set<SalleBean> salles) {
		this.salles = salles;
	}

	@OneToMany(mappedBy = "service")
	public Set<GroupeExamenBean> getGroupeExamens() {
		return this.groupeExamens;
	}

	public void setGroupeExamens(Set<GroupeExamenBean> groupeExamens) {
		this.groupeExamens = groupeExamens;
	}

	@OneToMany(mappedBy = "service")
	public Set<MedecinBean> getMedecins() {
		return this.medecins;
	}

	public void setMedecins(Set<MedecinBean> medecins) {
		this.medecins = medecins;
	}

}
