/**
 * 
 */
package sn.org.seysoo.medicis.services;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IUtilisateurService;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RoleBean;
import sn.org.seysoo.medicis.entity.UtilisateurBean;
import org.jboss.seam.annotations.Destroy;

import bsh.util.Util;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * @author Khadija
 *
 */
@Stateful
@Name("utilisateurService")
@Scope(ScopeType.SESSION)
public class UtilisateurService implements IUtilisateurService{
	
	@In(create=true,required=false)
	private DB db;
	
	@PersistenceContext
	private EntityManager em;

	public UtilisateurBean findUtilisateurByLoginMotdepasse(String login,String password) {
		
		
		UtilisateurBean user=null;
		try
		{
//			UtilisateurBean util=new UtilisateurBean();
//			util.setDateCreation(new Date());
//			util.setNom("Ba");
//			util.setPrenom("Cledor");
//			util.setEmail("ba@cledor.com");
//			util.setTelephone1("54545454");
//			util.setIdentifiant("Cledor");
//			util.setSafal("thiakhangoune");
//			util.setMotDePasse("cledor"+util.getSafal());
//			util.setId(1);
//			util.setRole(em.merge(new RoleBean("Admin", 0)));
//			em.merge(util);
			
			DBCollection dbCollection = db.getCollection("utilisateur");
			BasicDBObject query = new BasicDBObject("identifiant", login).append("motDePasse", password);
			System.out.println("Find all documents in collection: utilisateur");
			DBObject cursor = dbCollection.findOne(query);
				if(cursor!=null){
					user=em.find(UtilisateurBean.class, (Integer) cursor.get("_id"));
					System.out.println(cursor);
				}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return user;
	}


    @Destroy
    public void destroy() {
    }

    @Remove
    public void remove() {
    }
}
