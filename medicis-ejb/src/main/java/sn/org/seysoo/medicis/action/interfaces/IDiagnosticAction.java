package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;
import java.util.Map;

import org.jboss.seam.jsf.SetDataModel;
import org.primefaces.model.DualListModel;

import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.ImageBean;
import sn.org.seysoo.medicis.entity.InterpretationBean;
import sn.org.seysoo.medicis.entity.InterpretationTypeBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.TechniqueUtiliseBean;

public interface IDiagnosticAction {

	public Boolean getExamenIRM();

	public void setExamenIRM(Boolean examenIRM);

	public String saveDiagnosticRadiologie();

	public List<GroupeExamenBean> listeTypesNonIrm();

	public DualListModel<ExamenBean> getListeExamensType();

	public void setListeExamensType(DualListModel<ExamenBean> listeExamensType);

	public int getGroupeExamen();

	public void setGroupeExamen(int groupeExamen);

	public int getEtapeSaisie();

	public void setEtapeSaisie(int etapeSaisie);

	public String etapeSuivant();

	public String etapePrecedente();

	public SetDataModel getDiagnosticsDataModel();

	public void setDiagnosticsDataModel(SetDataModel diagnosticsDataModel);

	public List<MedecinBean> getListeMedecinsRadiologie();

	public void setListeMedecinsRadiologie(
			List<MedecinBean> listeMedecinsRadiologie);

	public List<SalleBean> getListeSallesRadiologie();

	public void setListeSallesRadiologie(List<SalleBean> listeSallesRadiologie);

	public List<DiagnosticBean> getListeDiagnostics();

	public void setListeDiagnostics(List<DiagnosticBean> listeDiagnostics);

	public String initialisationFicheConsultationRadiologie();

	public void remove();

	public void destroy();

	public InterpretationBean getNouvelleInterpretation();

	public void setNouvelleInterpretation(
			InterpretationBean nouvelleInterpretation);

	public void saveInterpretation();

	public List<InterpretationBean> getListeInterpretation();

	public void setListeInterpretation(
			List<InterpretationBean> listeInterpretation);

	public String nouveauDiagnosticRadiologie();

	public DiagnosticBean getNouveauDiagnostic();

	public void setNouveauDiagnostic(DiagnosticBean nouveauDiagnostic);

	public List<DiagnosticBean> getDiagnosticsAnterieursOdontoligie();

	public void setDiagnosticsAnterieursOdontoligie(
			List<DiagnosticBean> diagnosticsAnterieursOdontoligie);

	public List<DiagnosticBean> getDiagnosticsProchainsOdontoligie();

	public void setDiagnosticsProchainsOdontoligie(
			List<DiagnosticBean> diagnosticsProchainsOdontoligie);

	public List<DiagnosticBean> getDiagnosticsAnterieursRadiologie();

	public void setDiagnosticsAnterieursRadiologie(
			List<DiagnosticBean> diagnosticsAnterieursRadiologie);

	public List<DiagnosticBean> getDiagnosticsProchainsRadiologie();

	public void setDiagnosticsProchainsRadiologie(
			List<DiagnosticBean> diagnosticsProchainsRadiologie);

	public List<DiagnosticBean> getDiagnosticsAnterieursGynecologie();

	public void setDiagnosticsAnterieursGynecologie(
			List<DiagnosticBean> diagnosticsAnterieursGynecologie);

	public List<DiagnosticBean> getDiagnosticsProchainsGynecologie();

	public void setDiagnosticsProchainsGynecologie(
			List<DiagnosticBean> diagnosticsProchainsGynecologie);

	public String dossierPatient();

	/*
	 * public DiagnosticBean getDiagnosticSelected();
	 * 
	 * public void setDiagnosticSelected(DiagnosticBean diagnosticSelected);
	 */

	public RendezVousBean getRendezVous();

	public void setRendezVous(RendezVousBean rendezVous);

	public String ficheConsultationRadiologie();

	public String selectFicheConsultationRadiologie();

	public List<ImageBean> listeImagesDiagnostic();

	public List<ImageBean> getListeImages();

	public void setListeImages(List<ImageBean> listeImages);

	public void saveInterpretationRv();

	public List<RendezVousBean> getRendezVousToday();

	public void setRendezVousToday(List<RendezVousBean> rendezVousToday);

	public String onTrouveRendezVous();

	public boolean isInfoIrmDejaPresente();

	public void setInfoIrmDejaPresente(boolean infoIrmDejaPresente);

	public DiagnosticBean getDernierDiagnosticIrm();

	public void setDernierDiagnosticIrm(DiagnosticBean dernierDiagnosticIrm);
	
	public Map<String,String> getImagesMap();

	public void setImagesMap(Map<String, String> imagesMap);

	public void affImages();

	public List<String> getSelectedImages();

	public void setSelectedImages(List<String> selectedImages);

	public void initialisationNouveauDiagnosticRadiologie();

	public String validationNouvelExamenRadiologie();

	public DiagnosticBean getDiagnosticChoisis();

	public void setDiagnosticChoisis(DiagnosticBean diagnosticChoisis);

	public String modifierDiagnosticRadiologie();

	public String supprimerDiagnostic();

	public List<ExamenBean> listeExamenNonIrm();

	public InterpretationBean getInterpretationSelected();

	public void setInterpretationSelected(
			InterpretationBean interpretationSelected);
	
	public InterpretationTypeBean getInterpretationType();

	public void setInterpretationType(InterpretationTypeBean interpretationType);
	
	public String changeInterpretationType();
	
	public List<InterpretationTypeBean> getListeInterpretationsTypeRadiologie();

	public void setListeInterpretationsTypeRadiologie(
			List<InterpretationTypeBean> listeInterpretationsTypeRadiologie);
	
	public List<TechniqueUtiliseBean> getListeTechniquesRadiologie();

	public void setListeTechniquesRadiologie(
			List<TechniqueUtiliseBean> listeTechniquesRadiologie);
}
