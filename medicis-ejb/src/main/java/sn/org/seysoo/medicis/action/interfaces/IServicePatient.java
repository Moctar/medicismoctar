/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:46:23
 * @URL		 :IServicePatient.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du S�negal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.action.interfaces;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.CaracteristiquesBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.ParametreBean;
import sn.org.seysoo.medicis.entity.PatientBean;

/**
 * @author hp
 *
 */
@Local
public interface IServicePatient {
	public List<PatientBean> listeTousLesPatient();
	public PatientBean findPatient(Integer id);
	public List<PatientBean> findPatient(String nom,String prenom,String cni,Date dateNaissnce);
	public void savepatient(PatientBean patient);
	public PatientBean saveAndReturnpatient(PatientBean patient);
	public List<GroupeSanguinBean> getAllGoupeSanguins();
	public PatientBean archiver(PatientBean patient);
	public PatientBean restaurer(PatientBean patient);
	public List<PatientBean> listeTousLesPatientArchve();
	public ParametreBean findParametreByCode(String code);
	public void saveparametreNumDossier(ParametreBean para);
	public Integer findDernierNumDoss();
	public void saveCaracteristiques(CaracteristiquesBean caracteristiques);
	public List<CaracteristiquesBean> getAllCaracteristiquesPatient(Integer patient_id);
	public List<CaracteristiquesBean> getAllCaracteristiquesPatientByType(Integer patient_id, Integer type_caracteristique_id);
	public BeneficiaireBean findBeneficiaireByMatricule(String matricule);
	public BeneficiaireBean findBeneficiaireByMatricule(Integer client,String matricule);
	public List<PatientBean> listePatientsByBeneficiaire(BeneficiaireBean beneficiaire);
	public void destroy();
}
