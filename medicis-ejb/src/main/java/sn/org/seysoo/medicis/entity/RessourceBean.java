package sn.org.seysoo.medicis.entity;

// Generated 9 oct. 2013 11:24:03 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.annotations.Indexed;
import org.jboss.seam.annotations.Name;
import org.jboss.solder.core.Veto;

/**
 * Ressource generated by hbm2java
 */
@Entity
@Veto
@Indexed
@Table(name = "ressource")
@Name("ressourceBean")
public class RessourceBean implements java.io.Serializable {

	private Integer id;
	private String page;
	private Set<PermissionBean> permissions = new HashSet<PermissionBean>(0);

	public RessourceBean() {
	}

	public RessourceBean(String page, Set<PermissionBean> permissions) {
		this.page = page;
		this.permissions = permissions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "page")
	public String getPage() {
		return this.page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	@OneToMany( mappedBy = "ressource")
	public Set<PermissionBean> getPermissions() {
		return this.permissions;
	}

	public void setPermissions(Set<PermissionBean> permissions) {
		this.permissions = permissions;
	}

}
