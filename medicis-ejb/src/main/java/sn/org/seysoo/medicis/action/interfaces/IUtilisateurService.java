/**
 * 
 */
package sn.org.seysoo.medicis.action.interfaces;

import javax.ejb.Local;
import javax.ejb.Remove;

import org.jboss.seam.annotations.Destroy;

import sn.org.seysoo.medicis.entity.UtilisateurBean;

/**
 * @author Khadija
 *
 */
@Local
public interface IUtilisateurService {
	public UtilisateurBean findUtilisateurByLoginMotdepasse(String login,String password);
    public void destroy();
    public void remove();

}
