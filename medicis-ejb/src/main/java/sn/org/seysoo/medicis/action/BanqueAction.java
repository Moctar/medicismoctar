package sn.org.seysoo.medicis.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Stateful;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IBanque;
import sn.org.seysoo.medicis.action.interfaces.IBanqueService;
import sn.org.seysoo.medicis.action.interfaces.IRendezvousServices;
import sn.org.seysoo.medicis.entity.BanqueBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.datamodel.DataModel;

import javax.ejb.Remove;

@Stateful
@Scope(ScopeType.SESSION)
@Name("banqueAction")
public class BanqueAction  implements IBanque{

	private Map<String,String> banques = new HashMap<String,String>();

	@DataModel("listBanques")
	private List<BanqueBean> listBanques;

	
	
	
	
	
	
	public Map<String,String> getBanques() {
		IBanqueService ibanque = (IBanqueService) Component.getInstance("banqueService");

		listBanques = ibanque.getAllBanques(); 
		for (int i=0; i<listBanques.size(); i++ ){
			banques.put(listBanques.get(i).getNomBanque(),""+listBanques.get(i).getId());
		}

		Map<String, String> treeMap = new TreeMap<String, String>(banques);

		banques = treeMap;

		return banques;
	}

	public void setBanques(Map<String,String> banques) {
		this.banques = banques;
	}
	
    @Destroy
    public void destroy() {
    }

    @Remove
    public void remove() {
    }

	public List<BanqueBean> getListBanques() {
		return listBanques;
	}

	public void setListBanques(List<BanqueBean> listBanques) {
		this.listBanques = listBanques;
	}


}
