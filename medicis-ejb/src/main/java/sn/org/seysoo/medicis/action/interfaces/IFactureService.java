package sn.org.seysoo.medicis.action.interfaces;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.FactureBean;
import sn.org.seysoo.medicis.entity.FactureClientBean;
import sn.org.seysoo.medicis.entity.FacturePatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;

public interface IFactureService {

	public ArrayList<FactureClientBean> listFacturesClient();
	
	public List <FacturePatientBean> listFacturesPatient() ;
	
	public void save_FacturePatient () ;
	
	public List <DiagnosticBean> getDiagnostics (RendezVousBean rendezVous);
	public void save_Diagnostic ();
	public List <FactureClientBean> getFactureClient ();
	public void save_FactureClient ();
	public FactureBean EnregistrerFacture(FactureBean facture);
	public FacturePatientBean EnregistrerFacturePatient(FacturePatientBean facturepatient);
	public String testEnvoiMail ();
	public void insererBanq();
	public List<DiagnosticBean> getDiagnosticClientParPeriode(int idclient,Date deateDeb,Date debFin);
	public FactureClientBean EnregistrerFactureClient(FactureClientBean factureClient);
	public ClientBean findClientById(int idClient);
	
}
