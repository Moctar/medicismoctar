package sn.org.seysoo.medicis.util;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener  {

	public SessionListener() {
	}
	  
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Current Session created : "	 + event.getSession().getId()+ " at "+ new Date());  	  
	}  
	
	public void sessionDestroyed(HttpSessionEvent event) {  
		  
		HttpSession session = event.getSession();  
		
		System.out.println("Current Session destroyed :"  + session.getId()+ " Logging out user...");  
		
		try {			
			prepareLogoutInfoAndLogoutActiveUser(session);  			
		}
		catch(Exception e) {  
			System.out.println("Error while logging out at session destroyed : " + e.getMessage());  
		}  
		  
	}
		
	public void prepareLogoutInfoAndLogoutActiveUser(HttpSession httpSession) {  
		// Only if needed  
	}
}
