package sn.org.seysoo.medicis.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SessionTimeoutFilter implements Filter {

	// page par d�faut de redirection
	private String timeoutPageIndex = "/session.seam";  
	private String timeoutUrl = "";
	private String indexSeam = "index.seam"; 
	private String homeSeam = "home.seam";
	  
	public void init(FilterConfig filterConfig) throws ServletException {  
		
	}  
	  
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,ServletException {  
	  
		if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {  
	  
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;  
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;  

			String requestPath = httpServletRequest.getRequestURI();
			timeoutUrl = httpServletRequest.getContextPath();
			
			// session expiree?
			if (isSessionControlRequiredForThisResource(httpServletRequest)) {
				
				// session invalide?  
				if (isSessionInvalid(httpServletRequest)) {
					
					//System.out.println("***SessionTimeoutFilter - requestPath : "+requestPath);
					String[] seg = requestPath.split(ConstUtil.SLASHNOSPACE);
					
					if(seg!=null && seg.length>1 && ((seg.length == 2) || seg[2].equals(indexSeam) || seg[2].equals(homeSeam)))
						timeoutUrl += "/";
					else 
						timeoutUrl += timeoutPageIndex;
					
					httpServletResponse.sendRedirect(timeoutUrl);  
					return;  
				}
			}
		}  
		
		filterChain.doFilter(request, response);  
	}  
	  
	/*
	 * Controle de l'expiration de la session
	 */
	private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {  
		String requestPath = httpServletRequest.getRequestURI();  
		boolean controlRequired = !requestPath.contains(timeoutPageIndex);  
		return controlRequired;  
	}  
	  
	/*
	 * Controle de la validation de la session
	 */
	private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {  
		boolean sessionInValid = (httpServletRequest.getRequestedSessionId() != null) && !httpServletRequest.isRequestedSessionIdValid();  
		return sessionInValid;  
	}  
	  
	public void destroy() {  
	}  	
}
