/**
 * 
 */
package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;

import javax.ejb.Local;

import org.primefaces.model.chart.CartesianChartModel;

import sn.org.seysoo.medicis.entity.CaracteristiquesBean;

/**
 * @author OmGue
 *
 */

@Local
public interface IChart {
	
	public void initChart();
	
	public void initCharte(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract);
	
	public CartesianChartModel getCategoryModel();
	
	public void createCategoryModel(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract);
	
	public int getIndiceMax();

	public int getIndiceMin();
	
	public String getCategoryTitle();
	
	public void destroy();
}
