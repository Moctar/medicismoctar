package sn.org.seysoo.medicis.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IApplicationContexte;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.StringUtil;

@Stateful
@Scope(ScopeType.APPLICATION)
@Name("ApplicationContexte")
public class ApplicationContexteAction implements IApplicationContexte {
	
	@Factory("applicationPath")
	public String getPathContext() {
		ServletContext servletContext = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
		return servletContext.getContextPath();
	}
	public static ResourceBundle getBundle() {
		ResourceBundle bundle = ResourceBundle.getBundle("seam");
		return bundle;
	}
		
	// Retourne le path complet de l'application
	public static String getRealPathAppli() {
		ServletContext servletContext = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
		return servletContext.getRealPath("") + "/";
	}
	
		
	// Retourne le path du dossier pdf
	public static String getRealPathPdf() {
		ServletContext servletContext = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
		return StringUtil.adaptPathToSystem(getRealPathAppli() + "pdf/");
	}
	
	// Retourne le path du dossier des photos des patients
		public static String getRealPathPhoto() {
			ServletContext servletContext = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
			return StringUtil.adaptPathToSystem(getRealPathAppli() + "img/photo/");
		}
	
	// Retourne le path du dossier jasper
		public static String getRealPathJasper() {
			return StringUtil.adaptPathToSystem(getRealPathAppli() + "jasper/");
		}
		
	// Retourne le path du dossier img
	public static String getRealPathImg() {
		return getRealPathAppli() + "img/";
	}
	
	// Retourne le path du dossier img
	public static String getRealPathlogo() {
		return getRealPathAppli() + "img/logo-medicis.png";
	}

	
	// D�nifition de l'heure dans les �crans de portail
	@In(required=false)
	String dateAujourhui;
	
	public String getDateAujourhui(){
		Date dateToday = new Date();
		SimpleDateFormat sdfToday = new SimpleDateFormat("EEEE dd/MM/yyyy - HH:mm");
		String [] tabStr =  sdfToday.format(dateToday).toString().split(ConstUtil.SPACE);

		String today = new String(tabStr[0]);
		for(int i=1; i<tabStr.length; i++)
			today += ConstUtil.SPACE + new String(tabStr[i]);
		
		dateAujourhui = today.toUpperCase();
		return dateAujourhui;
	}
	
	/**
	 * 
	 */
	public String getUriPreviousPage() {
		//System.out.println("---getUriPreviousPage - redirect");
		String uri="";
		
		try{
			FacesContext ctx = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
			
			if(request.getHeader("Referer") == null) {
				uri = "/home.seam";
				return uri;
			}
				
			String urlPrevious = request.getHeader("Referer").toString();
			int indexofContextPath = urlPrevious.indexOf(request.getContextPath());
			if(indexofContextPath > 0) {
				String subsStartindex = urlPrevious.substring(indexofContextPath);
				uri = subsStartindex.substring(request.getContextPath().length());
			}
			else {
				int nb = 0;
				int i=0;
				
				if(urlPrevious.contains("//")){
					while(i<urlPrevious.length() && nb<3) {
						if(String.valueOf(urlPrevious.charAt(i)).equals("/"))
							nb++;
						i++;
					}
				}
				else{
					while(i<urlPrevious.length() && nb<1) {
						if(String.valueOf(urlPrevious.charAt(i)).equals("/"))
							nb++;
						i++;
					}
				}
				
				if(i>0 && nb==3) {
					i--;
					uri = urlPrevious.substring(i);
				}
				else 
					uri = "/home.seam";
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			//System.out.println("---getUriPreviousPage - Exception : "+e.getMessage());
			uri = "/home.seam";
		}
		
		return uri;
	}
	
	@Factory("CODE_TYPE_CARACT_TAILLE")
	public int getCodeTypeCaractTaille() {
		return ConstUtil.CODE_TYPE_CARACT_TAILLE;
	}
	
	@Factory("CODE_TYPE_CARACT_TEMPERATURE")
	public int getCodeTypeCaractTemperature() {
		return ConstUtil.CODE_TYPE_CARACT_TEMPERATURE;
	}
	
	@Factory("CODE_TYPE_CARACT_POIDS")
	public int getCodeTypeCaractPoids() {
		return ConstUtil.CODE_TYPE_CARACT_POIDS;
	}
	
	@Factory("CODE_TYPE_CARACT_PA")
	public int getCodeTypeCaractPa() {
		return ConstUtil.CODE_TYPE_CARACT_PA;
	}
	
	@Factory("CODE_TYPE_CARACT_POULS")
	public int getCodeTypeCaractPouls() {
		return ConstUtil.CODE_TYPE_CARACT_POULS;
	}
	
	@Destroy
	@Remove
	public void destroy(){
	}
}
