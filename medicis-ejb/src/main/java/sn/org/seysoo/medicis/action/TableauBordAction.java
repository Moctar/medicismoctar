/**
 * 
 */
package sn.org.seysoo.medicis.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import sn.org.seysoo.medicis.action.interfaces.IChart;
import sn.org.seysoo.medicis.action.interfaces.IServicePatient;
import sn.org.seysoo.medicis.action.interfaces.ITableauBord;
import sn.org.seysoo.medicis.entity.CaracteristiquesBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.TypeCaracteristiqueBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.ConvertUtil;

/**
 * @author OmGue
 *
 */

@Stateful
@Scope(ScopeType.SESSION)
@Name("tableauBordAction")
public class TableauBordAction implements ITableauBord {

	private PatientBean patient;
	
	private CaracteristiquesBean caractTaille = new CaracteristiquesBean();
	
	private CaracteristiquesBean caractTemperature = new CaracteristiquesBean();
	
	private CaracteristiquesBean caractPoids = new CaracteristiquesBean();
	
	private CaracteristiquesBean caractPa = new CaracteristiquesBean();
	
	private CaracteristiquesBean caractPouls = new CaracteristiquesBean();
	
	private List<CaracteristiquesBean> caractList = new ArrayList<CaracteristiquesBean>();
	
	private List<CaracteristiquesBean> caractTailleList = new ArrayList<CaracteristiquesBean>();
	
	private List<CaracteristiquesBean> caractTemperatureList = new ArrayList<CaracteristiquesBean>();
	
	private List<CaracteristiquesBean> caractPoidsList = new ArrayList<CaracteristiquesBean>();
	
	private List<CaracteristiquesBean> caractPaList = new ArrayList<CaracteristiquesBean>();
	
	private List<CaracteristiquesBean> caractPoulsList = new ArrayList<CaracteristiquesBean>();
	
	private String paValues = "0/0";
	
	/**
	 * initTableauBord(PatientBean patient)
	 * 
	 * Restaure les caract�ristiques et ant�c�dents du patient
	 */
	public void initTableauBord(PatientBean patient) {
		//System.out.println("---Init Tableau de bord---");
		this.patient = patient;
	}
	
	/**
	 * getAllCaracteristiquesPatient()
	 * 
	 * Toutes les caracteristiques d'un patient sont récupérés puis sont regroupées par types dans
	 * diffentes listes
	 */
	public String getAllCaracteristiquesPatient() {
		//System.out.println("---reinitListCaracteristiquesTaille---");
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");

		try {
			if(this.patient!=null) {
				caractList = servicepatient.getAllCaracteristiquesPatient(this.patient.getId());

				if(caractList!=null) {
					
					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractList.add(map.getValue());

					for(CaracteristiquesBean caracteristiquesBean : caractList) {
						switch (caracteristiquesBean.getTypeCaracteristiqueId()) {
							case ConstUtil.CODE_TYPE_CARACT_TAILLE:
								caractTailleList.add(caracteristiquesBean);
								break;
	
							case ConstUtil.CODE_TYPE_CARACT_TEMPERATURE:
								caractTemperatureList.add(caracteristiquesBean);
								break;
	
							case ConstUtil.CODE_TYPE_CARACT_POIDS:
								caractPoidsList.add(caracteristiquesBean);
								break;
	
							case ConstUtil.CODE_TYPE_CARACT_PA:
								caractPaList.add(caracteristiquesBean);
								break;
	
							case ConstUtil.CODE_TYPE_CARACT_POULS:
								caractPoulsList.add(caracteristiquesBean);
								break;
	
							default:
								break;
						}
					}
					
					loadCaractTemperatureList(false);
					loadCaractPoidsList(false);
					loadCaractPaList(false);
					loadCaractPoulsList(false);
					loadCaractTailleList(false);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesTaille - Exception : "+e.getMessage());
		}
		
		return "";
	}
	
	/**
	 * loadCaractTailleList(boolean newRequest)
	 * 
	 * charge la liste des valeurs de la taille à partir de la liste de toutes caractéristiques du patient
	 * ou à partir une nouvelle requete
	 */
	public String loadCaractTailleList(boolean newRequest) {
		//System.out.println("---reinitListCaracteristiquesTaille---");
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		IChart chartAction = (IChart) Component.getInstance("chartAction");

		try {
			if(this.patient!=null) {
				if(newRequest)
					caractTailleList = servicepatient.getAllCaracteristiquesPatientByType(this.patient.getId(), new Integer(ConstUtil.CODE_TYPE_CARACT_TAILLE));

				if(caractTailleList!=null && caractTailleList.size()!=0) {

					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractTailleList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractTailleList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractTailleList.add(map.getValue());

					this.caractTaille = caractTailleList.get(caractTailleList.size()-1);

					chartAction.initCharte(caractTailleList,null,"",0,200, ConstUtil.CODE_TYPE_CARACT_TAILLE);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesTaille - Exception : "+e.getMessage());
			chartAction.initChart();
		}
		
		return "";
	}
	
	/**
	 * loadCaractTemperatureList(boolean newRequest)
	 * 
	 * charge la liste des valeurs de la temperature à partir de la liste de toutes caractéristiques du patient
	 * ou à partir une nouvelle requete
	 */
	public void loadCaractTemperatureList(boolean newRequest) {
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		IChart chartAction = (IChart) Component.getInstance("chartAction");
		
		try {
			if(this.patient!=null) {
				if(newRequest)
					caractTemperatureList = servicepatient.getAllCaracteristiquesPatientByType(this.patient.getId(), new Integer(ConstUtil.CODE_TYPE_CARACT_TEMPERATURE));

				if(caractTemperatureList!=null && caractTemperatureList.size()!=0) {

					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractTemperatureList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractTemperatureList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractTemperatureList.add(map.getValue());

					this.caractTemperature = caractTemperatureList.get(caractTemperatureList.size()-1);

					chartAction.initCharte(caractTemperatureList,null,"",0,150, ConstUtil.CODE_TYPE_CARACT_TEMPERATURE);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesTemperature - Exception : "+e.getMessage());
			chartAction.initChart();
		}
	}

	/**
	 * loadCaractPoidsList(boolean newRequest)
	 * 
	 * charge la liste des valeurs du poids à partir de la liste de toutes caractéristiques du patient
	 * ou à partir une nouvelle requete
	 */
	public void loadCaractPoidsList(boolean newRequest) {
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		IChart chartAction = (IChart) Component.getInstance("chartAction");
		
		try {
			if(this.patient!=null) {
				if(newRequest)
					caractPoidsList = servicepatient.getAllCaracteristiquesPatientByType(this.patient.getId(), new Integer(ConstUtil.CODE_TYPE_CARACT_POIDS));

				if(caractPoidsList!=null && caractPoidsList.size()!=0) {

					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractPoidsList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractPoidsList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractPoidsList.add(map.getValue());

					this.caractPoids = caractPoidsList.get(caractPoidsList.size()-1);

					chartAction.initCharte(caractPoidsList,null,"",0,150, ConstUtil.CODE_TYPE_CARACT_POIDS);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesPoids - Exception : "+e.getMessage());
			chartAction.initChart();
		}
	}

	/**
	 * loadCaractPaList(boolean newRequest)
	 * 
	 * charge la liste des valeurs de la pression artérielle à partir de la liste de toutes caractéristiques du patient
	 * ou à partir une nouvelle requete
	 */
	public void loadCaractPaList(boolean newRequest) {
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		IChart chartAction = (IChart) Component.getInstance("chartAction");
		
		try {
			if(this.patient!=null) {
				if(newRequest)
					caractPaList = servicepatient.getAllCaracteristiquesPatientByType(this.patient.getId(), new Integer(ConstUtil.CODE_TYPE_CARACT_PA));

				if(caractPaList!=null && caractPaList.size()!=0) {

					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractPaList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractPaList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractPaList.add(map.getValue());

					this.caractPa = caractPaList.get(caractPaList.size()-1);
					paValues = ""+this.caractPa.getValeur()+ConstUtil.SLASHNOSPACE+this.caractPa.getValeur2();

					chartAction.initCharte(caractPaList,null,"",0,150, ConstUtil.CODE_TYPE_CARACT_PA);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesPa - Exception : "+e.getMessage());
			chartAction.initChart();
		}
	}

	/**
	 * loadCaractPoulsList(boolean newRequest)
	 * 
	 * charge la liste des valeurs du pouls à partir de la liste de toutes caractéristiques du patient
	 * ou à partir une nouvelle requete
	 */
	public void loadCaractPoulsList(boolean newRequest) {
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		IChart chartAction = (IChart) Component.getInstance("chartAction");
		
		try {
			if(this.patient!=null) {
				if(newRequest)
					caractPoulsList = servicepatient.getAllCaracteristiquesPatientByType(this.patient.getId(), new Integer(ConstUtil.CODE_TYPE_CARACT_POULS));

				if(caractPoulsList!=null && caractPoulsList.size()!=0) {

					Map<Integer, CaracteristiquesBean> caractMap = new HashMap<Integer, CaracteristiquesBean>(); 
					for(CaracteristiquesBean caracteristiquesBean : caractPoulsList) {
						caractMap.put(caracteristiquesBean.getId(), caracteristiquesBean);
					}

					Map<Integer, CaracteristiquesBean> caractTreedMap = new TreeMap<Integer, CaracteristiquesBean>(caractMap);  

					caractPoulsList = new ArrayList<CaracteristiquesBean>();

					for(Map.Entry<Integer, CaracteristiquesBean> map : caractTreedMap.entrySet())
						caractPoulsList.add(map.getValue());

					this.caractPouls = caractPoulsList.get(caractPoulsList.size()-1);

					chartAction.initCharte(caractPoulsList,null,"",0,150, ConstUtil.CODE_TYPE_CARACT_POULS);
				}
			}
		}
		catch (Exception e) {
			//System.out.println("---reinitListCaracteristiquesPa - Exception : "+e.getMessage());
			chartAction.initChart();
		}
	}

	/**
	 * saveAntecdentsPatient()
	 * 
	 * Enregistre les ant�c�dents saisis du patient
	 */
	public void saveAntecdentsPatient() {
		try{
			IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
			servicepatient.savepatient(patient);

		} catch (Exception e) {
			//System.out.println(e.getMessage());
			//e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Ajoute ou met � jours les caract�ristiques prises lors de la derni�re consultation du patient
	 */
	public void saveCaracteristique(int typeCaract){
		try{
			IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
			CaracteristiquesBean caract = new CaracteristiquesBean();
			caract.setPatient(patient);
			caract.setDate(new Date());
			caract.setTypeCaracteristique(new TypeCaracteristiqueBean(typeCaract));
			
			switch (typeCaract) {
				case ConstUtil.CODE_TYPE_CARACT_TAILLE:
					caract.setValeur(caractTaille.getValeur());
					servicepatient.saveCaracteristiques(caract);
					loadCaractTailleList(true);
					break;
				case ConstUtil.CODE_TYPE_CARACT_TEMPERATURE:
					caract.setValeur(caractTemperature.getValeur());
					servicepatient.saveCaracteristiques(caract);
					loadCaractTemperatureList(true);
					break;
				case ConstUtil.CODE_TYPE_CARACT_POIDS:
					caract.setValeur(caractPoids.getValeur());
					servicepatient.saveCaracteristiques(caract);
					loadCaractPoidsList(true);
					break;	
				case ConstUtil.CODE_TYPE_CARACT_POULS:
					caract.setValeur(caractPouls.getValeur());
					servicepatient.saveCaracteristiques(caract);
					loadCaractPoulsList(true);
					break;
				case ConstUtil.CODE_TYPE_CARACT_PA:
					String [] vals = paValues.split(ConstUtil.SLASHNOSPACE);
					caract.setValeur(ConvertUtil.convertToInteger(vals[0]));
					caract.setValeur2(ConvertUtil.convertToInteger(vals[1]));
					servicepatient.saveCaracteristiques(caract);
					loadCaractPaList(true);
					break;
				default:
					break;
			}
		} catch (Exception e) {
			//System.out.println(e.getMessage());
			//e.printStackTrace();
		}
	}
	
	@Destroy
	@Remove
	public void destroy(){
	}
	
	/* getters and setters */
	
	public PatientBean getPatient() {
		return patient;
	}

	public void setPatient(PatientBean patient) {
		this.patient = patient;
	}

	public CaracteristiquesBean getCaractTaille() {
		return caractTaille;
	}

	public void setCaractTaille(CaracteristiquesBean caractTaille) {
		this.caractTaille = caractTaille;
	}

	public CaracteristiquesBean getCaractTemperature() {
		return caractTemperature;
	}

	public void setCaractTemperature(CaracteristiquesBean caractTemperature) {
		this.caractTemperature = caractTemperature;
	}

	public CaracteristiquesBean getCaractPoids() {
		return caractPoids;
	}

	public void setCaractPoids(CaracteristiquesBean caractPoids) {
		this.caractPoids = caractPoids;
	}

	public CaracteristiquesBean getCaractPa() {
		return caractPa;
	}

	public void setCaractPa(CaracteristiquesBean caractPa) {
		this.caractPa = caractPa;
	}

	public CaracteristiquesBean getCaractPouls() {
		return caractPouls;
	}

	public void setCaractPouls(CaracteristiquesBean caractPouls) {
		this.caractPouls = caractPouls;
	}

	public List<CaracteristiquesBean> getCaractTailleList() {
		return caractTailleList;
	}

	public void setCaractTailleList(List<CaracteristiquesBean> caractTailleList) {
		this.caractTailleList = caractTailleList;
	}

	public List<CaracteristiquesBean> getCaractTemperatureList() {
		return caractTemperatureList;
	}

	public void setCaractTemperatureList(
			List<CaracteristiquesBean> caractTemperatureList) {
		this.caractTemperatureList = caractTemperatureList;
	}

	public List<CaracteristiquesBean> getCaractPoidsList() {
		return caractPoidsList;
	}

	public void setCaractPoidsList(List<CaracteristiquesBean> caractPoidsList) {
		this.caractPoidsList = caractPoidsList;
	}

	public List<CaracteristiquesBean> getCaractPaList() {
		return caractPaList;
	}

	public void setCaractPaList(List<CaracteristiquesBean> caractPaList) {
		this.caractPaList = caractPaList;
	}

	public List<CaracteristiquesBean> getCaractPoulsList() {
		return caractPoulsList;
	}

	public void setCaractPoulsList(List<CaracteristiquesBean> caractPoulsList) {
		this.caractPoulsList = caractPoulsList;
	}

	public String getPaValues() {
		return paValues;
	}

	public void setPaValues(String paValues) {
		this.paValues = paValues;
	}
}
