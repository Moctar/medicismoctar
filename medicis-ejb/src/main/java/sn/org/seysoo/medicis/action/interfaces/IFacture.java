package sn.org.seysoo.medicis.action.interfaces;

import java.util.Date;
import java.util.List;

import javax.faces.event.ActionListener;

import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.FactureClientBean;
import sn.org.seysoo.medicis.entity.FacturePatientBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;

public interface IFacture {
	
	public void listFacturesClient();
	
	public void listFacturesPatient();
	
	public List<FactureClientBean> getListFacturesClient();

	public void setListFacturesClient(List<FactureClientBean> listFacturesClient);

	public List<FacturePatientBean> getListFacturesPatient();

	public void setListFacturesPatient(List<FacturePatientBean> listFacturesPatient);

	public FactureClientBean getFactureClientSelected();

	public void setFactureClientSelected(FactureClientBean factureClientSelected);

	public FacturePatientBean getFacturePatientSelected();

	public void setFacturePatientSelected(FacturePatientBean facturePatientSelected);
	
	public List<DiagnosticBean> getListDiagnostic() ;

	public void setListDiagnostic(List<DiagnosticBean> listDiagnostic) ;
	
	public void listDiagnostic();
	
	public double getTotal();

	public void setTotal(double total);
	
	public String getChiffreEnLettre() ;
	

	public void setChiffreEnLettre(String chiffreEnLettre) ;
	
	public double getMontantDu();

	public void setMontantDu(double montantDu);
	

	public int getChoixRegelement();

	public void setChoixRegelement(int choixRegelement) ;
	
	public String getNumeroCheque() ;

	public void setNumeroCheque(String numeroCheque) ;
	
	public String getBanque() ;

	public void setBanque(String banque) ;

	public String getDevise() ;
	
	public void setDevise(String devise);
	
	public void saveFactureP ();

	public double getMontantEscpece() ;

	public void setMontantCheque(double montantCheque);
	
	public double getMontantCheque();
	
	public void setMontantEscpece(double montantEscpece);
	
	
	public List<PatientBean> getListPatients();
	
	public void setListPatients(List<PatientBean> listPatients) ;
	
	public void infoClients () ;
	
	public List<RendezVousBean> getListRendezvou();

	public void setListRendezvou(List<RendezVousBean> listRendezvou);
	
	
	public List<DiagnosticBean> getListDiagnostiq();

	public void setListDiagnostiq(List<DiagnosticBean> listDiagnostiq);
	
	public double getTotalFC() ;

	public void setTotalFC(double totalFC);
	
	public double getNetApayer() ;

	public void setNetApayer(double netApayer) ;
	public Date getDateDebutFactureClient();
	public void setDateDebutFactureClient(Date dateDebutFactureClient);
	public Date getDateFinFactureClient();
	public void setDateFinFactureClient(Date dateFinFactureClient);
	public int getIdclient();
	public void setIdclient(int idclient);
	public void genereFactureClient();
	public String genegerePDFPatient();
	public void imprimerPDFPatient();
	public double getTauxp();
	public void setTauxp(double tauxp);


}
