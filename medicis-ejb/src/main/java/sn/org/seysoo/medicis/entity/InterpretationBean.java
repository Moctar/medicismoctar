package sn.org.seysoo.medicis.entity;

// Generated 9 oct. 2013 11:24:03 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.search.annotations.Indexed;
import org.jboss.seam.annotations.Name;
import org.jboss.solder.core.Veto;

/**
 * Interpretation generated by hbm2java
 */
@Entity
@Veto
@Indexed
@Table(name = "interpretation")
@Name("interpretationBean")
public class InterpretationBean implements java.io.Serializable {

	private Integer id;
	private String libelle;
	private Date date;
	private TechniqueUtiliseBean techniqueUtilise;
	private DiagnosticBean diagnostic;
	private UtilisateurBean utilisateur;

	public InterpretationBean() {
	}

	public InterpretationBean(String libelle, Date date) {
		this.libelle = libelle;
		this.date = date;
	}

	public InterpretationBean(String libelle,Date date, TechniqueUtiliseBean techniqueUtilise,
			DiagnosticBean diagnostic, UtilisateurBean utilisateur) {
		this.libelle = libelle;
		this.date = date;
		this.techniqueUtilise = techniqueUtilise;
		this.diagnostic = diagnostic;
		this.utilisateur = utilisateur;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = false, length = 19)
	@NotNull
	public Date getDate() {
		return this.date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "libelle", nullable = false, length = 65535)
	@NotNull
	@Size(max = 65535)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle){
		this.libelle = libelle;
	}

	@ManyToOne()
	@JoinColumn(name = "diagnostic_id", nullable = true)
	public DiagnosticBean getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(DiagnosticBean diagnostic) {
		this.diagnostic = diagnostic;
	}
	
	@ManyToOne()
	@JoinColumn(name = "technique_utilise_id", nullable = true)
	public TechniqueUtiliseBean getTechniqueUtilise() {
		return this.techniqueUtilise;
	}

	public void setTechniqueUtilise(TechniqueUtiliseBean techniqueUtilise) {
		this.techniqueUtilise = techniqueUtilise;
	}
	
	@ManyToOne()
	@JoinColumn(name = "utilisateur_id", nullable = false)
	@NotNull
	public UtilisateurBean getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(UtilisateurBean utilisateur) {
		this.utilisateur = utilisateur;
	}

}
