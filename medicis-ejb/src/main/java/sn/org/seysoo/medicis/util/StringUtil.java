package sn.org.seysoo.medicis.util;
import java.text.DecimalFormat;

/**
 * @author OmarGueye
 * @version 0.1
 * 
 * Classe utilitaire pour les op�rations sur les chaines de caract�res
 * 
 */
public class StringUtil {

	/**
     * methode isNullOrEmpty - Verifier si une chaine est null ou vide
     * 
     * @param string
     * @return boolean
     */
	public static String OS = System.getProperty("os.name").toLowerCase();
	public static boolean isWindows = false;
    public static boolean isNullOrEmpty(String string) {
        if (string == null || string.trim().length() <= 0)
            return true;
        return false;
    }

    /**
     * methode isEmpty - Verifier si une chaine est vide
     * 
     * @param string
     * @return boolean
     */
    public static boolean isEmpty(String string) {
        return string.trim().length() == 0;
    }
    
    
    /**
     * getValueDecimalFormat - retourner un numero en chaine de caracteres complet� par des zero
     * @param nbZero
     * @param value
     * @return String
     */
    public static String getValueDecimalFormat(int nbZero, int value) {
    	String strZero = "";
    	if(nbZero > 0)
    		for(int i=0; i<nbZero; i++)
    			strZero += "0";
    	else
    		strZero = "0";
    	
    	DecimalFormat df = new DecimalFormat(strZero);
    	
    	return df.format(value);
    }
    
    public static String adaptPathToSystem(String realpath){
    	String path = null;
    	// Detection du systeme
    			System.out.println("---MedicisInit--- OS : "+OS);
    			
    			if (isWindows()) {
    				isWindows = true;
    				System.out.println("This is Windows");
    			} else if (isMac()) {
    				System.out.println("This is Mac");
    			} else if (isUnix()) {
    				System.out.println("This is Unix or Linux");
    			} else if (isSolaris()) {
    				System.out.println("This is Solaris");
    			} else {
    				System.out.println("Your OS is not support!!");
    			}
    	if(isWindows)
    		path = realpath.replace("/","\\");
    	else
    		path = realpath.replace("\\","/");
    		
    	return path;
    }
    
    public static String deleteAccent(String src) {
        StringBuffer result = new StringBuffer();
        if(src!=null && src.length()!=0) {
            int index = -1;
            char c = (char)0;
            String chars= "���������������";
            String replace= "aaaeeeeiioouuuc";
            for(int i=0; i<src.length(); i++) {
                c = src.charAt(i);
                if( (index=chars.indexOf(c))!=-1 )
                    result.append(replace.charAt(index));
                else
                    result.append(c);
            }
        }
        return result.toString();
    }
    private static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}
 
	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}
 
	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}
 
	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
	public static String formater(String ref,int size,char c){
		if (ref.length()<size){
			int limit = size-ref.length();
			String chaine = "";
			for (int j=0;j<limit;j++)
				chaine += c;
			ref = chaine.concat(ref);
		}
		return ref;
	}
}
