/*
  Auteur : Mouhamadou
  Projet : medicis
  Fichier : sn.org.seysoo.medicis.action.RadiogieAction.java
  Date : 23 d�c. 2013

 * ================================================================================================================================
 * Copyright (c) 2013 by Seysoo SARL,
 * seysoo.com
 * R�publique du Senegal
 * 
 * This software is the propertary information of Seysoo
 * All rights reserved

 */
package sn.org.seysoo.medicis.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.Renderer;
import org.primefaces.context.RequestContext;

import sn.org.seysoo.medicis.action.interfaces.IRadiologie;
import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.entity.ImageBean;
/**
 * @author  Mouhamadou
 *
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("radiologie")

public class RadiogieAction implements IRadiologie {

	private String emailMedecin;
	private String objetMail;
	private String contenuMail;


	private List<ImageBean> listeImages;
	private List<ImageBean> chekedImages;

	private List<ImageBean> checkedImages;
	private List<String> selectedImages;

	private Map<String,String> imagesMap = new HashMap<String,String>();


	@In
	private Renderer renderer;

	@PersistenceContext
	private EntityManager em;

	//Envoi email pour le medecin pr�scrispteur avec piece jointe
	public void envoiEmailMedecin (){

		System.out.println("---------- info -----------");
		System.out.println("email :  "+emailMedecin);
		System.out.println("objet :  "+objetMail);
		System.out.println("contenu :  "+contenuMail);

		checkedImages=new ArrayList<ImageBean>();
		if (chekedImages != null && chekedImages. size()>0){
			System.out.println("taille des images  coch�es:   "   +chekedImages.size()); 

			for (int i = 0; i < chekedImages.size(); i++) {
				ImageBean img = new ImageBean();
				img.setImage(chekedImages.get(i)+"");
				checkedImages.add(img);
				System.out.println("chekedImages  "  +chekedImages.get(i)); 
			}

		}

		//renderer.render("/email/envoiEmailmedecin.xhtml");
		
		//envoi email (pour lever l'exception lanc�e par primefaces 4.0)
		RequestContext rc = RequestContext.getCurrentInstance();
		Renderer.instance().render("/email/envoiEmailmedecin.xhtml");
		RequestContext.setCurrentInstance(rc);

	}
	
	//Setter et Getter

	public String getEmailMedecin() {
		return emailMedecin;
	}

	public void setEmailMedecin(String emailMedecin) {
		this.emailMedecin = emailMedecin;
	}

	public String getObjetMail() {
		return objetMail;
	}

	public void setObjetMail(String objetMail) {
		this.objetMail = objetMail;
	}

	public String getContenuMail() {
		return contenuMail;
	}


	public void setContenuMail(String contenuMail) {
		this.contenuMail = contenuMail;
	}



	public List<ImageBean> getListeImages() {
		return listeImages;
	}



	public void setListeImages(List<ImageBean> listeImages) {
		this.listeImages = listeImages;
	}



	public List<String> getSelectedImages() {
		return selectedImages;
	}




	public void setSelectedImages(List<String> selectedImages) {
		this.selectedImages = selectedImages;
	}


	public Map<String,String> getImagesMap() {

		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component.getInstance("diagnosticService");
		listeImages = serviceDiagnostic.findListeImagesDiagnostic();


		for (int i=0; i<listeImages.size(); i++ ){
			imagesMap.put(listeImages.get(i).getImage() , listeImages.get(i).getImage());
			//checkedImages.add(em.find(ImageBean.class, listeImages.get(i).getId()));
		}

		Map<String,String> treeMap = new TreeMap<String,String>(imagesMap);

		imagesMap = treeMap;


		return imagesMap;
	}


	public void setImagesMap(Map<String,String> imagesMap) {
		this.imagesMap = imagesMap;
	}

	public List<ImageBean> getChekedImages() {
		return chekedImages;
	}


	public void setChekedImages(List<ImageBean> chekedImages) {
		this.chekedImages = chekedImages;
	}

	public List<ImageBean> getCheckedImages() {
		return checkedImages;
	}

	public void setCheckedImages(List<ImageBean> checkedImages) {
		this.checkedImages = checkedImages;
	}


	@Destroy
	public void destroy() {
	}

	@Remove
	public void remove() {
	}

}
