package sn.org.seysoo.medicis.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.apache.lucene.search.Query;
import org.hibernate.SessionFactory;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.DatabaseRetrievalMethod;
import org.hibernate.search.query.ObjectLookupMethod;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IRendezvousServices;
import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;
import sn.org.seysoo.medicis.util.ConstUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;


@Stateful
@Name("rendezvousServices")
@Scope(ScopeType.SESSION)

public class RendezvousServices implements IRendezvousServices{

	@PersistenceContext
	private EntityManager em;


	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	@Inject  
	private Provider<FullTextEntityManager> lazyFEM;

	@In(create=true,required=false)
	private DB db;


	public RendezVousBean findRendezvous (int id){
		return em.find(RendezVousBean.class, id);
	}
	public List<RendezVousBean> listRendezvous (){

		List<RendezVousBean> list = null;

		try {
			System.out.println("-->try liste rv --");

			FullTextEntityManager fem = Search.getFullTextEntityManager(em);
			//Search.getFullTextEntityManager(em);

			QueryBuilder builder = fem.getSearchFactory().buildQueryBuilder().forEntity(RendezVousBean.class).get();

			Query luceneQuery = builder.all().createQuery();


			//keyword(). onFields("etat").  matching("defaut"). createQuery();
			//builder.keyword(). onField("etat"). matching("defaut"). createQuery();

			//builder.all().createQuery();

			//keyword(). onField("etat"). matching("defaut"). createQuery();

			System.out.println(" --> luceneQuery  *** :   	"+luceneQuery);

			FullTextQuery query = fem.createFullTextQuery(luceneQuery,RendezVousBean.class);


			query.initializeObjectsWith(ObjectLookupMethod.SKIP, DatabaseRetrievalMethod.FIND_BY_ID);

			list = query.getResultList();

			/*System.out.println("-->try liste client getClients --");

			DBCollection dbCollection = db.getCollection("rendez_vous");
			System.out.println("Find all documents in collection rendez_vous:");
				DBCursor cursor = dbCollection.find();


				list=new ArrayList<RendezVousBean>();
				if(cursor.size()>0){
					while (cursor.hasNext()) {

						DBObject item = cursor.next();
						RendezVousBean rv=new RendezVousBean();
						rv.setId((Integer) item.get("_id"));
						rv.setDate((Date) item.get("date"));
						rv.setHeure((String) item.get("heure"));


						rv.getPatient().setNumDossier((String) item.get("numDossier"));
						rv.getPatient().setPrenom((String) item.get("prenom"));

						rv.getMotif().setLibelle((String) item.get("libelle"));
						list.add(rv);
					    System.out.println("Rendez vous -> "+item);


					}
				}*/


			//list = myList.toArray();

			System.out.println("SIZE lRendez_vous  :  "  + list.size()); 

		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection RendezVous--- "); 
			e.printStackTrace();
		}

		return list;
	}

	//@Produces
	// Recherche de tous les rendez-vous
	public List<RendezVousBean> searchAllRendezvous(String etat){
		List<RendezVousBean> listrv=null;
		try
		{
			DBCollection dbCollection = db.getCollection("rendez_vous");
			BasicDBObject query = new BasicDBObject("etat", etat);
			System.out.println("Find all documents in collection: searchRvAnnule");
			DBCursor cursor = dbCollection.find(query);
			listrv=new ArrayList<RendezVousBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					RendezVousBean rv=new RendezVousBean();
					rv=em.find(RendezVousBean.class, (Integer) item.get("_id"));
					listrv.add(rv);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listrv;

	}
	// Recherche des rendez-vous par periode
	public List<RendezVousBean> searchRendezvousByPeriod(String etat, Date dateDeb,Date dateFin){
		List<RendezVousBean> listrv=null;
		try
		{
			DBCollection dbCollection = db.getCollection("rendez_vous");
			BasicDBObject query = new BasicDBObject("etat", etat);
			System.out.println("Find all documents in collection: searchRvAnnule");
			DBCursor cursor = dbCollection.find(query);
			listrv=new ArrayList<RendezVousBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					RendezVousBean rv=new RendezVousBean();
					rv=em.find(RendezVousBean.class, (Integer) item.get("_id"));
					
					if(rv.getDate().after(dateDeb) && rv.getDate().before(dateFin))
					 listrv.add(rv);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listrv;

	}
	
	public List <ClientBean> getAllClients (){

		List<ClientBean> list = null;

		try {
			System.out.println("-->try liste client getClients --");

			DBCollection dbCollection = db.getCollection("client");
			System.out.println("Find all documents in collection client_taux:");
			DBCursor cursor = dbCollection.find();
			list=new ArrayList<ClientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					ClientBean cli=new ClientBean();
					cli.setId((Integer) item.get("_id"));
					cli.setDesignation((String) item.get("designation"));
					list.add(cli);
					System.out.println(item);
				}
			}



			System.out.println("SIZE clients *****   :  "  + list.size()); 

		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection Client--- "); 
			e.printStackTrace();
		}

		return list;
	}


	public List<ClientTauxBean> getTauxAssur (){

		List<ClientTauxBean> list = null;

		try {
			System.out.println("-->try liste taux --");


			System.out.println("-->try liste client getClients --");

			DBCollection dbCollection = db.getCollection("client_taux");
			System.out.println("Find all documents in collection  client_taux:");
			DBCursor cursor = dbCollection.find();
			list=new ArrayList<ClientTauxBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					ClientTauxBean cli=new ClientTauxBean();
					cli.setId((Integer) item.get("_id"));
					cli.setTaux((Double) item.get("taux"));
					list.add(cli);
					System.out.println(item);
				}
			}




			System.out.println("SIZE clients taux   :  "  + list.size()); 

		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection Client--- "); 
			e.printStackTrace();
		}

		return list;
	}


	public List<ServiceBean> getAllServices (){

		List<ServiceBean> list = null;

		try {



			DBCollection dbCollection = db.getCollection("service");
			DBCursor cursor = dbCollection.find();
			list=new ArrayList<ServiceBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					ServiceBean service=new ServiceBean();
					service= em.find(ServiceBean.class, (Integer) item.get("_id"));
					list.add(service);
				}
			}




			System.out.println("SIZE service   :  "  + list.size()); 

		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection Service--- "); 
			e.printStackTrace();
		}

		return list;
	}

	//Liste des motifs 
	public List<MotifBean> getAllMotif (){

		List<MotifBean> list = null;

		try {
			System.out.println("-->try liste taux --");


			System.out.println("-->try liste client getServices --");

			DBCollection dbCollection = db.getCollection("motif");
			System.out.println("Find all documents in collection  service:");
			DBCursor cursor = dbCollection.find();
			list=new ArrayList<MotifBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					MotifBean mo=new MotifBean();
					mo= em.find(MotifBean.class, (Integer) item.get("_id"));
					list.add(mo);
					System.out.println(item);
				}
			}




			System.out.println("SIZE Motif   :  "  + list.size()); 

		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection motif--- "); 
			e.printStackTrace();
		}

		return list;
	}
	
	public void saveRendezVous_test() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
			RendezVousBean rendezVousBean = new RendezVousBean();
			PatientBean patientBean = new PatientBean();
			MotifBean motifBean = new MotifBean();
			ServiceBean serviceBean = new ServiceBean();
			GroupeSanguinBean groupeSanguinBean = new GroupeSanguinBean();

			try {

				Date parsedDate = sdf.parse("10/10/2010 10:10");
				Date parsedDate2 = sdf2.parse("26/02/1991");

				/** ServiceBean*/
				//serviceBean.setId(new Integer(1));
				serviceBean.setEtat("etat service2222") ;
				serviceBean.setLibelle("Odontologie"); 
				serviceBean.setQualificatif("qualif22");

				///Date parsedDate = sdf.parse("18/10/2013 10:30");
				//Date parsedDate2 = sdf2.parse("05/09/1985");

				/** ServiceBean*/
				//serviceBean.setId(new Integer(1));
				serviceBean.setEtat("etat service1") ;
				serviceBean.setLibelle("Radiologie"); 
				serviceBean.setQualificatif("test");
				serviceBean=em.merge(serviceBean);

				/** Motifs */
				//motifBean.setId(new Integer(1));
				motifBean.setDesription("description1") ;
				motifBean.setLibelle("Consultation");
				motifBean.setService(serviceBean);
				motifBean=em.merge(motifBean);

				/** Groupe sanguin*/
				//groupeSanguinBean.setId(new Integer(1));
				groupeSanguinBean.setLibelle("O-");

				groupeSanguinBean.setLibelle("AB+");
				groupeSanguinBean=em.merge(groupeSanguinBean);

				/*Patient*/
				//patientBean.setId(new Integer(1));
				patientBean.setAdresse("Parcelles Assainies U20") ;
				patientBean.setAffectionsLd("affection pulmonaire");
				patientBean.setAllergies("allergie fum�e") ;
				patientBean.setAntecedentsFamiliaux("antecedant fam");
				patientBean.setAntecedentsPersonnels("antecedant per");
				//patientBean.setBeneficiaire(beneficiaire);
				patientBean.setCni("1795198500124");
				patientBean.setDateNissance(parsedDate2);
				patientBean.setEmail("mfall91@gmail.com");
				patientBean.setEtatDossier(new Integer(3)); 
				patientBean.setEmail("paerz@gmail.com");
				patientBean.setEtatDossier(new Integer(2)); 
				patientBean.setGroupeSanguin(groupeSanguinBean);
				patientBean.setNom("Fall");
				patientBean.setPrenom("Ndeye Maimouna");
				patientBean.setNumDossier("000127");
				patientBean.setEmail("paerz@gmail.com");
				patientBean.setEtatDossier(new Integer(2)); 
				patientBean.setGroupeSanguin(groupeSanguinBean);
				patientBean.setNom("Cledor");
				patientBean.setPrenom("Moctar Ba");
				//patientBean.setNumDossier("000127");
				patientBean.setPoids(70.2);
				patientBean.setPreventionsDepistages("prevention3640");
				patientBean.setProfession("Geometre");
				patientBean.setPreventionsDepistages("prevention15404");
				patientBean.setProfession("informaticien");
				patientBean.setSexe("M");
				patientBean.setTaille(1.70);
				patientBean.setUnitePoids("Kg");
				patientBean.setUniteTaille("metre");
				patientBean.setTelephonePatient("775677979");
				patientBean.setTelephonePatient("775674141");
				//patientBean.set
				patientBean=em.merge(patientBean);

				/** Rendez vous */
				//rendezVousBean.setId(new Integer(1));
				rendezVousBean.setDate(parsedDate);
				rendezVousBean.setEtat("etat22") ;

				rendezVousBean.setEtat("etat1") ;
				rendezVousBean.setMotif(motifBean);
				rendezVousBean.setPatient(patientBean);
				rendezVousBean=em.merge(rendezVousBean);

				System.out.println("rendez vous id : "+rendezVousBean.getId());


			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			em.merge(rendezVousBean);
		}
		catch(PersistenceException e) {
			System.out.println("---saveRV exception : "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void save_RendezVous () {

	}
	public void saveRendezvous(RendezVousBean rv) {
		try {
			em.merge(rv);
		}
		catch(PersistenceException e) {
			System.out.println("---saveRendezvous exception : "+e.getMessage());
			e.printStackTrace();
		}
	}
	public RendezVousBean EnregistrerRendezvous(RendezVousBean rv) {
		try {
			return em.merge(rv);
		}
		catch(PersistenceException e) {
			System.out.println("---saveRendezvous exception : "+e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	/*public void save_Rendezvous() {

}*/

	public void save_Client_test(){

		ClientTauxBean clientTauxBean = new  ClientTauxBean();
		ClientBean clientBean = new ClientBean();
		TypeClientBean typeClientBean = new TypeClientBean() ;

		// setting....1

		/*typeClientBean.setLibelle("AXA Assurance");
	typeClientBean=em.merge(typeClientBean); 

	clientBean.setAdresse("dakar plateau");
	clientBean.setContact("axa_assurance@axa.sn");
	clientBean.setDesignation("designation 0001");
	clientBean.setEcheance(1);
	clientBean.setTelephone("00221338521041");
	clientBean.setSolde(89000000);
	clientBean.setTypeClient(typeClientBean);
	clientBean = em.merge(clientBean);

	clientTauxBean.setTaux(0.8);
	clientTauxBean.setClient(clientBean);
	clientTauxBean=em.merge(clientTauxBean);*/

		// setting....2

		typeClientBean.setLibelle("AMSA Assurance");
		typeClientBean=em.merge(typeClientBean); 

		clientBean.setAdresse("place de lindenpence");
		clientBean.setContact("amsa_assurance@amsa.sn");
		clientBean.setDesignation("designation 0002");
		clientBean.setEcheance(1);
		clientBean.setTelephone("00221338215856");
		clientBean.setSolde(81000000);
		clientBean.setTypeClient(typeClientBean);
		clientBean = em.merge(clientBean);

		clientTauxBean.setTaux(0.75);
		clientTauxBean.setClient(clientBean);
		clientBean=em.merge(clientBean);


	}

	public void saveMotif(MotifBean motifBean) {
		try {
			em.merge(motifBean);
		}
		catch(PersistenceException e) {
			System.out.println("---saveMotif exception : "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void saveService(ServiceBean serviceBean) {
		try {
			em.merge(serviceBean);
		}
		catch(PersistenceException e) {
			System.out.println("---saveService exception : "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void saveGroupeSanguin(GroupeSanguinBean groupeSanguinBean) {
		try {
			em.merge(groupeSanguinBean);
		}
		catch(PersistenceException e) {
			System.out.println("---saveGroupeSanguinBean exception : "+e.getMessage());
			e.printStackTrace();
		}
	}	
	public void savePatients(PatientBean patientBean) {
		try {
			em.merge(patientBean);
		}
		catch(PersistenceException e) {
			System.out.println("---savePatientBean exception : "+e.getMessage());
			e.printStackTrace();
		}
	}

	@Remove
	public void remove(){
	}

	@Destroy
	public void destroy() {
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	public Provider<FullTextEntityManager> getLazyFEM() {
		return lazyFEM;
	}

	public void setLazyFEM(Provider<FullTextEntityManager> lazyFEM) {
		this.lazyFEM = lazyFEM;
	}

	public List<RendezVousBean> randezVousPeriode(int idPatient, Date dateDebut, Date dateFin) {
		// TODO Auto-generated method stub
		System.out.println("appel " + dateDebut+ " et "+ dateFin);
		try {
			List<RendezVousBean> listRV = new ArrayList<RendezVousBean>();
			DBCollection dbCollection = db.getCollection("rendez_vous");
			BasicDBObject query = new BasicDBObject("date", new BasicDBObject("$gt", dateDebut).append("$lte", dateFin));
			DBCursor cursor = dbCollection.find(query);
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idRv = (Integer) item.get("_id");
				RendezVousBean rv = em.find(RendezVousBean.class, idRv);
				if(rv.getPatient().getId() == idPatient){
					System.out.println("Rv "+ rv.getId() +  " date "+rv.getDate() );
					listRV.add(rv);
				}
			}
			return listRV;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	public List<MotifBean> listMotifbyService(Integer idService ){
		List<MotifBean> list=null;
		try
		{
			DBCollection dbCollection = db.getCollection("motif");
			BasicDBObject query = new BasicDBObject("service_id", idService);
			System.out.println("Find all documents in collection: motif");
			DBCursor cursor = dbCollection.find(query);
			list=new ArrayList<MotifBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					MotifBean rv=new MotifBean();
					rv=em.find(MotifBean.class, (Integer) item.get("_id"));
					list.add(rv);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return list;

	}
	
	/**
	 * renvoi la liste des types de clients assureurs
	 * @return List of TypeClientBean
	 */
	public List<TypeClientBean> getTypesClient(){
		List<TypeClientBean> resultList = null;
		try {
			DBCollection dbCollection = db.getCollection("type_client");
			DBCursor cursor = dbCollection.find();
			if (cursor.size()>0){
				resultList = new ArrayList<TypeClientBean>();
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					TypeClientBean tClient=new TypeClientBean();
					tClient=em.find(TypeClientBean.class, (Integer) item.get("_id"));
					resultList.add(tClient);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return resultList;
	}
	
	/**
	 * Renvoi la liste des clients de type typeClient
	 * @param typeClient
	 * @return List of ClientBean
	 */
	public List<ClientBean> getClientsByType(Integer typeClient){
		List<ClientBean> list = null;

		try {
			DBCollection dbCollection = db.getCollection("client");
			BasicDBObject query = new BasicDBObject("type_client_id", typeClient);
			DBCursor cursor = dbCollection.find(query);
			list=new ArrayList<ClientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					ClientBean cli=new ClientBean();
					cli = em.find(ClientBean.class,  item.get("_id"));
					list.add(cli);
				}
			}
		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection Client--- "); 
			e.printStackTrace();
		}
		return list;
	}
	
	public TypeClientBean findTypeClientById(Integer id){
		TypeClientBean client = null;
		
		try {
			client=em.find(TypeClientBean.class, id);
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return client;
	}
	public SalleBean findSalleById(Integer id){
		SalleBean salle=null;
		try{
			salle=em.find(SalleBean.class, id);
		}catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return salle;
		
	}
	public MedecinBean findMedecinById(Integer id){
		MedecinBean medecin=null;
		try{
			medecin=em.find(MedecinBean.class, id);
		}catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return medecin;
	}
	public ServiceBean findServiceById(Integer id){
		ServiceBean service=null;
		try{
			service=em.find(ServiceBean.class, id);
		}catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return service;
	}

}
