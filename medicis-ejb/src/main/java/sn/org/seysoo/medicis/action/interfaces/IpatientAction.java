/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:26:05
 * @URL		 :IpatientAction.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du S�negal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.action.interfaces;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.jboss.seam.Component;

import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.StringUtil;

/**
 * @author hp
 *
 */
@Local
public interface IpatientAction {
	public void debutNouveauPatient();
	public void nouveauPatient();
	public String modifierPatient();
	public void listePatient();
	public List<PatientBean> getListepatient();
	public void setListepatient(List<PatientBean> listepatient);
	public PatientBean getPatientSelectioner();
	public void setPatientSelectioner(PatientBean patientSelectioner);
	public List<PatientBean> getFiltrelistepatient();
	public void setFiltrelistepatient(List<PatientBean> filtrelistepatient);
	public PatientBean getPatientBean();
	public void setPatientBean(PatientBean patientBean);
	public void verifierExistancePatient();
	public void savepatient();
	public PatientBean getPatienttrouve();
	public void setPatienttrouve(PatientBean patienttrouve);
	public void onTrouvePatient();
	
	public Map<String, String> getGroupeSanguins();
	public void setGroupeSanguins(Map<String, String> groupeSanguins);
	public boolean isAssure();
	public void setAssure(boolean assure);
	
	public PatientBean getPatientSelectionerArchiver();
	public void setPatientSelectionerArchiver(PatientBean patientSelectionerArchiver);
	
	public List<PatientBean> getAutresBeneficiares();

	public void setAutresBeneficiares(List<PatientBean> autresBeneficiares);

	public void archiver();
	public void restaurer();
	public void listePatientArchive();
	
	public String GenererNumeroDossier();
	public void incParametreNumeroDossier();
	
	public void verifierExistanceMatricule();
	public void listeAutresAyantsDroit();
	public List<PatientBean> getListeAyantDroits();
	public void setListeAyantDroits(List<PatientBean> listeAyantDroits);
	public boolean isIdentique();
	public void setIdentique(boolean identique);
	public void updateBeneficiaire();
	public byte[] getFile();
	public void setFile(byte[] file);
	public String getTypeFichier();
	public void setTypeFichier(String typeFichier);
	public void enregistrerPhotoPatient();
	public void verifierPhoto();
	public void remove();
	public void destroy();
}
