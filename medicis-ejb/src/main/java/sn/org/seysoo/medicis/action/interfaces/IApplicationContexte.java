//interface test
package sn.org.seysoo.medicis.action.interfaces;

public interface IApplicationContexte {
	public String getPathContext();
	
	public String getUriPreviousPage();
	
	public int getCodeTypeCaractTaille();
	
	public int getCodeTypeCaractTemperature();
	
	public int getCodeTypeCaractPoids();
	
	public int getCodeTypeCaractPa();
	
	public int getCodeTypeCaractPouls();
	
	public void destroy();
}

