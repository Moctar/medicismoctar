package sn.org.seysoo.medicis.action;
import java.io.File;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IConversionDcmToPngService;
import sn.org.seysoo.medicis.action.interfaces.IServiceDcmQuery;
import sn.org.seysoo.medicis.action.interfaces.IServicePatient;
import sn.org.seysoo.medicis.action.interfaces.Itest;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.FactureBean;
import sn.org.seysoo.medicis.entity.FacturePatientBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.InterpretationBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;

@Stateful
@Scope(ScopeType.SESSION)
@Name("testManager")
public class testAction implements Itest{

	@PersistenceContext
	private EntityManager em;
	private PatientBean infoPatient;
	private List<RendezVousBean> rvAnterieurs;
	public String ajouterEx(){
		
		
		return null;
	}

	public String ajouter() {
		try{

			PatientBean p=new PatientBean();
			p.setAdresse("ok");

			p.setAdresse("okkkkk");
			GroupeSanguinBean gs=new GroupeSanguinBean();
			gs.setLibelle("teste");
			gs=em.merge(gs);
			p.setGroupeSanguin(gs);
			p.setNom("nom");
			p.setPrenom("prenom");
			p.setAdresse("adresse");
			p.setAffectionsLd("affectionsLd");
			p.setAllergies("allergies");
			p.setAntecedentsFamiliaux("antecedentsFamiliaux");
			p.setAntecedentsPersonnels("antecedentsPersonnels");

			TypeClientBean typeClient = new TypeClientBean("Assurences");
			typeClient = em.merge(typeClient);
			ClientBean client = new ClientBean( typeClient,"Designation client",0,0);
			client.setTelephone("telephoneC");
			client = em.merge(client);
			ClientTauxBean clientTaux = new ClientTauxBean(client , 15);
			clientTaux = em.merge(clientTaux);
			BeneficiaireBean beneficiaire = new BeneficiaireBean(clientTaux ,"nomB","prenomB");
			beneficiaire.setStatut("statut");
			beneficiaire.setMatricule("matricule");
			beneficiaire.setPolice("police");
			beneficiaire.setTelephone("telephoneB");
			beneficiaire.setEmail("emailB");
			beneficiaire = em.merge(beneficiaire);	
			p.setBeneficiaire(beneficiaire );
			p.setDateNissance(new Date());
			p.setTelephonePatient("54545454");
			p.setSexe("m");
			
			p.setCni("cni");
			p.setEmail("email");
			p.setEtatDossier(0);
			p.setPoids(50);
			p.setPreventionsDepistages("preventionsDepistages");
			p.setProfession("profession");
			p.setSexe("sexe");
			p.setSignesParticuliers("signesParticuliers");
			p.setTaille(120);
			p.setTelephonePatient("telephone");
			p.setUnitePoids("Kg");
			p.setUniteTaille("cm");

			em.persist(p);


			
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}

		return"";
	}

	@Remove
	public void remove() {
	}

	@Destroy
	public void destroy(){
	}

	/**
	 * @return the infoPatient
	 */
	@Factory("infoPatient")
	public PatientBean getInfoPatient(){
		infoPatient = em.find(PatientBean.class, 6);
		return infoPatient;
	}

	/**
	 * @param infoPatient the infoPatient to set
	 */
	public void setInfoPatient(PatientBean infoPatient){
		this.infoPatient = infoPatient;
	}

	/**
	 * @return the rvAnterieurs
	 */
	

	/**
	 * @param rvAnterieurs the rvAnterieurs to set
	 */
	public void setRvAnterieurs(List<RendezVousBean> rvAnterieurs) {
		this.rvAnterieurs = rvAnterieurs;
	}


	public String testdcm(){
		IServiceDcmQuery serviceDcm= (IServiceDcmQuery) Component
				.getInstance("serviceDcmQuery");
		System.out.print("test Dcm");
		serviceDcm.query("OSIRIX", "127.0.0.1",4006);
		return null;

	}
	public void testConve(){
		try {
		IConversionDcmToPngService serviceConversion = (IConversionDcmToPngService) Component.getInstance("conversionDcmToPngService");
		//String dirname = ApplicationContexteAction.getRealPathImg() +"/ct.10.dcm";
		String dirname = "/ct.10.dcm";
		
		File af = new File(dirname);
		System.out.println("aaaaaaaaaaaaaaaaaaaaaa"+af.getTotalSpace());
		File aff = new File(ApplicationContexteAction.getRealPathImg());		
		serviceConversion.dcmconvpng3(af, 0, aff);
	} catch (Exception e) {
		e.printStackTrace();
		}
		
	}
}