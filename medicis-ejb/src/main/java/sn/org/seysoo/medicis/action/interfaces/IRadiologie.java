package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import sn.org.seysoo.medicis.entity.ImageBean;

@Local
public interface IRadiologie {
	
	    public void envoiEmailMedecin ();

	    public String getEmailMedecin() ;

		public void setEmailMedecin(String emailMedecin);

		public String getObjetMail();

		public void setObjetMail(String objetMail);

		public String getContenuMail() ;

		public void setContenuMail(String contenuMail);
		
		public void destroy() ;

	    public void remove();
	    
		public List<ImageBean> getListeImages() ;

		public void setListeImages(List<ImageBean> listeImages);


		public List<String> getSelectedImages() ;


		public void setSelectedImages(List<String> selectedImages) ;


		public Map<String,String> getImagesMap() ;

		public void setImagesMap(Map<String,String> imagesMap);
		
		
		public List<ImageBean> getChekedImages() ;


		public void setChekedImages(List<ImageBean> chekedImages) ;
		
		
		public List<ImageBean> getCheckedImages();
		
		public void setCheckedImages(List<ImageBean> checkedImages);



}
