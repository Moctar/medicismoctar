/**
 * 
 */
package sn.org.seysoo.medicis.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author OmGue
 *
 */
public class ConvertUtil {

	/**
	 * Methode convertToString - convertir de Double vers String
	 * 
	 * @param val - Double
	 * @return String
	 */
	public static String convertToString(Double val) {
		if (val == null) {
			return "";
		}
		else {
			return String.valueOf(val);
		}
	}

	/**
	 * Methode convertToString - convertir de Integer vers String
	 * 
	 * @param val - Integer
	 * @return String
	 */
	public static String convertToString(Integer val) {
		if (val == null) {
			return "";
		}
		else {
			return String.valueOf(val);
		}
	}

	/**
	 * Methode convertToString - convertir de BigDecimal vers String
	 * 
	 * @param val - BigDecimal
	 * @return String
	 */
	public static String convertToString(BigDecimal val) {
		if (val == null) {
			return "";
		}
		else {
			return String.valueOf(val);
		}
	}

	/**
	 * Methode convertToString - convertir de Object vers String
	 * 
	 * @param val - Object
	 * @return String
	 */
	public static String convertToString(Object val) {
		if (val == null) {
			return "";
		}
		else {
			return String.valueOf(val);
		}
	}

	/**
	 * Methode convertToDouble - convertir de String vers Double
	 * 
	 * @param val - String
	 * @return Double
	 */
	public static Double convertToDouble(String val) {
		if (val == null) {
			return null;
		}
		else {
			if (val.compareTo("") == 0) {
				return null;
			}
			else {
				String t[] = val.split(" "), valeur = "";
				for (int i = 0; i < t.length; i++)
					valeur += t[i];
				if (!valeur.equals(""))
					val = valeur;
				return new Double(val);
			}
		}
	}

	/**
	 * Methode convertToInteger - convertir de String vers Integer
	 * 
	 * @param val - String
	 * @return Integer
	 */
	public static Integer convertToInteger(String val) {
		if (val == null) {
			return null;
		}
		else {
			if (val.compareTo("") == 0) {
				return null;
			}
			else if (val.compareTo("NULL") == 0) {
				return null;
			}
			else {
				return new Integer(val);
			}
		}
	}

	/**
	 * Methode convertToLong - convertir de String vers Long
	 * 
	 * @param val - String
	 * @return Long
	 */
	public static Long convertToLong(String val) {
		if (val == null) {
			return null;
		}
		else {
			if (val.compareTo("") == 0) {
				return null;
			}
			else {
				return new Long(val);
			}
		}
	}
	
	/**
	 * Methode convertToLong - convertir de String vers long
	 * 
	 * @param val - String
	 * @return long
	 */
	public static long convertTolong(String val) {
		if (val == null) {
			return 0;
		}
		else {
			if (val.compareTo("") == 0) {
				return 0;
			}
			else {
				return new Long(val).longValue();
			}
		}
	}

	/**
	 * Methode convertDateToString - convertir de java.util.Date vers String
	 * 
	 * @param date - Date
	 * @return String
	 */
	public static String convertToString(Date date) {
		try {
			if (date == null) {
				return null;
			}

			return String.valueOf(date);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * Methode convertToString - convertir de java.sql.Date vers String
	 * 
	 * @param date - java.sql.Date
	 * @return String
	 */
	public static String convertToString(java.sql.Date date) {
		try {
			if (date == null) {
				return null;
			}

			return String.valueOf(date);
		}
		catch (Exception e) {			
			return null;
		}
	}

	/**
	 * Methode convertToString - convertir de java.sql.Date vers String
	 * 
	 * @param date - java.util.Date, pattern - String (avec le format de la date correspondant � dd/MM/yyyy, MM/dd/yyyy, yyyy-MM-dd, etc)
	 * @return String
	 */
	public static String convertToString(Date date, String pattern) {
		try {
			if (date == null) {
				return null;
			}
			DateFormat df = new SimpleDateFormat(pattern);
			return df.format(date);
		}
		catch (Exception e) {			
			return null;
		}
	}

	/**
	 * Methode convertStringToDate - convertir de String vers java.util.Date
	 * 
	 * @param date - String
	 * @return Date
	 */
	public static Date convertStringToDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConstUtil.DATE_PATTERN);

		if (date == null) {
			return null;
		}

		if (date.compareTo("") == 0) {
			return null;
		}

		try {
			return sdf.parse(date);
		}
		catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Methode getintValue - convertir de String vers int
	 * 
	 * @param value - String
	 * @return int
	 * 
	 */
	public static int getIntValue(String value) {
		try {
			return (Integer.valueOf(value).intValue());
		}
		catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Methode getintValue - convertir de Integer vers int
	 * 
	 * @param value - Integer
	 * @return int
	 */
	public static int getIntValue(Integer value) {
		try {
			return value.intValue();
		}
		catch (NullPointerException e) {
			return 0;
		}
	}

	/**
	 * Methode getdoubleValue - convertir de String vers double
	 * 
	 * @param value - String
	 * @return double
	 */
	public static double getDoubleValue(String value) {
		try {
			return (Double.valueOf(value).doubleValue());
		}
		catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Methode getdoubleValue - convertir de Double vers double
	 * 
	 * @param value - Double
	 * @return double
	 */
	public static double getDoubleValue(Double value) {
		try {
			return value.doubleValue();
		}
		catch (NullPointerException e) {
			return 0;
		}
	}

	/**
	 * Methode getlongValue - convertir de String vers long
	 * 
	 * @param value - String
	 * @return long
	 */
	public static long getLongValue(String value) {
		try {
			return (Long.valueOf(value).longValue());
		}
		catch (NumberFormatException e) {
			return 0;
		}
		catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Methode getlongValue - convertir de Long vers long
	 * 
	 * @param value - Long
	 * @return long
	 */
	public static long getLongValue(Long value) {
		try {
			return value.longValue();
		}
		catch (NullPointerException e) {
			return 0;
		}
	}

	/**
	 * Methode getStringfromIntValue - convertir de int vers String
	 * @param value - int
	 * @return String
	 */
	public static String getStringfromIntValue(int value) {
		return convertToString(new Integer(value));
	}
	
	
	public static BigDecimal convertToBigDecimal(long value){
		return new BigDecimal(value);
	}
	
	public static BigDecimal convertToBigDecimal(String val) {
		if (val == null) {
			return null;
		}
		else {
			if (val.compareTo("") == 0) {
				return null;
			}
			else {
				return new BigDecimal(val);
			}
		}
	}
	
	public static long convertIntToLong(int value) {
		return (new Integer(value)).longValue();
	}
	
	public static BigDecimal convertStringToBigDecimal(String value){
		return new BigDecimal(value);
	}

	/**
	 * Methode convertToDate - convertir de String vers Date
	 * @param strDate - String
	 * @param pattern - String
	 * @return Date
	 */
	public static Date convertToDate(String strDate, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		Date date = null;
		
		if ("".compareTo(strDate) == 0) {
			return date;
		}

		try {
			date = df.parse(strDate);
		} catch (ParseException pe) {
			pe.printStackTrace();
		}

		return date;
	}
	
	/**
	 * Methode convertToDate - convertir de Timestamp vers Date
	 * @param time - Timestamp
	 * @return Date
	 */
	public static Date convertToDate(Timestamp time){
		long milliseconds = time.getTime() + (time.getNanos() / 1000000);
		Date date = new Date(milliseconds);
		return date;
	}
	
	/**
	 * Methode convertToTimestamp - convertir de Date vers Timestamp
	 * @param date - Date
	 * @return Timestamp
	 */
	public static Timestamp convertToTimestamp(Date date) {
		Timestamp time = new Timestamp(date.getTime());
		return time;
		
	}
}
