/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:23:33
 * @URL		 :PatientAction.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du Sénegal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.action;

import static org.jboss.seam.ScopeType.SESSION;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.primefaces.context.RequestContext;

import sn.org.seysoo.medicis.action.interfaces.IGenerateReport;
import sn.org.seysoo.medicis.action.interfaces.IRendezvousServices;
import sn.org.seysoo.medicis.action.interfaces.IServicePatient;
import sn.org.seysoo.medicis.action.interfaces.IpatientAction;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.ParametreBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.StringUtil;

import com.jaspersoft.mongodb.MongoDbDataSource;
import com.jaspersoft.mongodb.connection.MongoDbConnection;
/**
 * @author hp
 * 
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("patientAction")
public class PatientAction implements IpatientAction {
	@In
	Redirect redirect;

	@In(value = "#{facesContext}")
	FacesContext facesContext;

	@In(value = "#{facesContext.externalContext}")
	private ExternalContext extCtx;

	@DataModel(value = "listepatient")
	List<PatientBean> listepatient;

	@DataModel(value = "listepatientarchive")
	List<PatientBean> listepatientarchive;

	@DataModel(value = "listeRecherchepatient")
	List<PatientBean> listeRecherchepatient;

	@DataModelSelection(value = "listeRecherchepatient")
	private PatientBean patienttrouve;

	@DataModel(value = "filtrelistepatient")
	List<PatientBean> filtrelistepatient;

	@DataModelSelection(value = "listepatient")
	@In(create = true, required = false)
	@Out(required=false, scope = SESSION)
	private PatientBean patientSelectioner;

	@DataModelSelection(value = "listepatientarchive")
	private PatientBean patientSelectionerArchiver;

	@In
	private FacesMessages facesMessages;

	@In(create = true, required = false)
	@Out(required=false, scope = SESSION)
	private PatientBean patientBean;
	
	private ParametreBean paraNumDossier = null;
	
	@DataModel(value = "listeAyantDroits")
	List<PatientBean> listeAyantDroits;
	private boolean identique;
	
	private byte[] file = null;
	private String typeFichier;
	/**
	 * @return the autresBeneficiares
	 */
	public List<PatientBean> getAutresBeneficiares() {
		if(patientSelectioner != null){
		System.out.println("patient "+patientSelectioner.getId() + " Beneficiaire "+patientSelectioner.getBeneficiaire().getId());
		autresBeneficiares = new ArrayList<PatientBean>();
		Set<PatientBean> listePatientsBeneficiares = patientSelectioner.getBeneficiaire().getPatients();
		for (Iterator iterator = listePatientsBeneficiares.iterator(); iterator
				.hasNext();) {
			PatientBean patient = (PatientBean) iterator.next();
			if(patient.getId() != patientSelectioner.getId()){
				autresBeneficiares.add(patient);
			}
			
		}
		return autresBeneficiares;
		}
		else{
			return null;
		}
	}

	/**
	 * @param autresBeneficiares the autresBeneficiares to set
	 */
	public void setAutresBeneficiares(List<PatientBean> autresBeneficiares) {
		this.autresBeneficiares = autresBeneficiares;
	}

	@In(create=true,required=false)
	private MongoDbConnection connection;
	
	List<PatientBean> autresBeneficiares;

	boolean assure=true;

	@In(create=true,required=false)
	String date1;

	private Map<String, String> groupeSanguins = new HashMap<String, String>();

	public void nouveauPatient() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String patientId = request.getParameter("patientId");
			if (patientId != null) {
				Integer id = Integer.parseInt(patientId);
				IServicePatient servicepatient = (IServicePatient) Component
						.getInstance("patientService");
				patientBean = servicepatient.findPatient(id);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void debutNouveauPatient() {
		patientBean = new PatientBean();
		patientBean.setBeneficiaire(new BeneficiaireBean());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sn.org.seysoo.medicis.action.interfaces.IpatientAction#listePatient()
	 */
	@Factory("listepatient")
	public void listePatient() {
		try {

			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			listepatient = servicepatient.listeTousLesPatient();
			if(listepatient!=null && listepatient.size()>0)
			System.out.println("Taille liste patient = " + listepatient.size());

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	@Factory("listepatientarchive")
	public void listePatientArchive() {
		try {

			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			listepatientarchive = servicepatient.listeTousLesPatientArchve();
			System.out.println("Taille liste patient Archive = "
					+ listepatientarchive.size());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	public void onTrouvePatient(){
		System.out.println("Patient trouv�");
		if (patienttrouve != null) {
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();
			this.patientBean = servicepatient.findPatient(Integer
					.parseInt(request
							.getParameter("formNouveauPatient:pat_selection")));
			if (patientBean.getBeneficiaire() == null){
				this.assure = false;
				patientBean.setBeneficiaire(new BeneficiaireBean());
			}
			System.out.println("id patientBean =" + patientBean.getId());
			System.out.println("Taille et poids patientBean ="
					+ patientBean.getTaille() + " - " + patientBean.getPoids());
		}

		else {
			System.out.println("Aucun patient trouvé");
		}

	}

	public void verifierExistancePatient(){
		try {
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			this.patientBean.setNom(request
					.getParameter("formNouveauPatient:nom"));
			this.patientBean.setPrenom(request
					.getParameter("formNouveauPatient:prenom"));
			this.patientBean.setCni(request
					.getParameter("formNouveauPatient:cni"));
			listeRecherchepatient = servicepatient.findPatient(
					request.getParameter("formNouveauPatient:nom"),
					request.getParameter("formNouveauPatient:prenom"),
					request.getParameter("formNouveauPatient:cni"), new Date());
			System.out
			.println("----------- Patiennnnnnnnnnnnnnnnnnnnnnt ----------");
			System.out.println("Nom = "
					+ request.getParameter("formNouveauPatient:nom"));
			System.out.println("Taille liste trouv� = "
					+ listeRecherchepatient.size());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void savepatient() {
		System.out
		.println("------------------------ apel de save patient----------------------------------");
		try {
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			IRendezvousServices rvService = (IRendezvousServices) Component.getInstance("rendezvousServices");
			HttpServletRequest request = (HttpServletRequest) extCtx.getRequest();
			System.out.println("Id beforeInsertion =" + patientBean.getId());
			
			String numDoss = GenererNumeroDossier();
			System.out.println("Numero Dossier >>>>> "+ numDoss);
			
			patientBean.setNumDossier(numDoss);
			if (!this.assure)
				this.patientBean.setBeneficiaire(null);
//			else{
//				if (!StringUtil.isNullOrEmpty(request.getParameter("formNouveauPatient:typeClient_input"))){
//					typeClient = rvService.findTypeClientById(new Integer(request.getParameter("formNouveauPatient:typeClient_input")));
//					patientBean.getBeneficiaire().getClientTaux().getClient().set
//				}
//			}
			servicepatient.savepatient(patientBean);
			incParametreNumeroDossier();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,"Le dossier patient est cr�� avec succ�s.", ""));
			facesMessages.add("Le dossier patient est cr�� avec succ�s..");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public String modifierPatient(){
		String url = "/patient/nouveaupatient.seam?patientId=" + patientSelectioner.getId();
		return url;
	}

	public List<PatientBean> getListepatient() {
		return listepatient;
	}

	public PatientBean getPatientSelectioner() {
		return patientSelectioner;
	}

	public void setPatientSelectioner(PatientBean patientSelectioner) {
		this.patientSelectioner = patientSelectioner;
	}

	public void setListepatient(List<PatientBean> listepatient) {
		this.listepatient = listepatient;
	}

	public List<PatientBean> getFiltrelistepatient() {
		return filtrelistepatient;
	}

	public void setFiltrelistepatient(List<PatientBean> filtrelistepatient) {
		this.filtrelistepatient = filtrelistepatient;
	}

	public PatientBean getPatienttrouve() {
		System.out
		.println("getpatientttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt");
		return patienttrouve;
	}

	public void setPatienttrouve(PatientBean patienttrouve) {
		System.out
		.println("setpatientttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt");
		this.patienttrouve = patienttrouve;
	}

	@Destroy
	public void destroy() {
	}

	public List<PatientBean> getListeRecherchepatient() {
		return listeRecherchepatient;
	}

	public void setListeRecherchepatient(List<PatientBean> listeRecherchepatient) {
		this.listeRecherchepatient = listeRecherchepatient;
	}

	public PatientBean getPatientBean() {
		return patientBean;
	}

	public void setPatientBean(PatientBean patientBean) {
		this.patientBean = patientBean;
	}

	public boolean isAssure() {
		return assure;
	}

	public void setAssure(boolean assure) {
		this.assure = assure;
	}

	public Map<String, String> getGroupeSanguins() {
		IServicePatient servicepatient = (IServicePatient) Component
				.getInstance("patientService");
		try {
			List<GroupeSanguinBean> listegroupeSanguins = servicepatient
					.getAllGoupeSanguins();
			for (int i = 0; i < listegroupeSanguins.size(); i++) {
				groupeSanguins.put(listegroupeSanguins.get(i).getLibelle(), ""
						+ listegroupeSanguins.get(i).getId());
			}

			Map<String, String> treeMap = new TreeMap<String, String>(
					groupeSanguins);

			groupeSanguins = treeMap;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return groupeSanguins;
	}

	public void setGroupeSanguins(Map<String, String> groupeSanguins) {
		this.groupeSanguins = groupeSanguins;
	}

	public void archiver() {


	try {
		HttpServletRequest request = (HttpServletRequest) extCtx
				.getRequest();
		
		IServicePatient servicepatient = (IServicePatient) Component
				.getInstance("patientService");
		patientSelectioner=servicepatient.findPatient(Integer.parseInt(request.getParameter("patsel:pat_selection")));
		servicepatient.archiver(patientSelectioner);
		facesMessages.add("Archivé avec succés.");
		redirect.setViewId("/patient/home.xhtml");
		redirect.execute();
	} catch (Exception e) {
		e.printStackTrace();
		System.err.println("cle ta methode ne fctionne pas "+e.getMessage());
	}
		
	}

	@Remove
	public void remove() {
	}

	public String testePDF(){
		IGenerateReport generateReport = (IGenerateReport) Component.getInstance("GenerateReport");
		Map<String, Object> parameters = new HashMap<String, Object>();
		String PATH_JR_CF03 = ApplicationContexteAction.getRealPathJasper() + "report1.jrxml";
		String fichierPdfCF03 = ApplicationContexteAction.getRealPathPdf() + "report1.pdf";
		try {
			System.out.println("Size patient "+connection.getMongoDatabase().getCollection("patient").find().size());
			parameters.put(MongoDbDataSource.CONNECTION, connection);
			parameters.put("parameter1","Test Mongo DB et JASPER SUR MEDICIS");

			generateReport.exportReportToPdfFile(PATH_JR_CF03, fichierPdfCF03, parameters);
			System.out.println(fichierPdfCF03);
			HttpServletResponse reponse = (HttpServletResponse) extCtx.getResponse();

			ServletOutputStream os=reponse.getOutputStream();

			reponse.setContentType("application/pdf");
			reponse.addHeader("Content-disposition", "attachment; filename=\"" + "test.pdf"+"\"");
			byte[] buf = new byte[1024];
			FileInputStream in = new FileInputStream(fichierPdfCF03);
			int c;
			while((c=in.read())!= -1){
				os.write(c);
			}
			os.flush();
			os.close();
			facesContext.responseComplete();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return fichierPdfCF03;
	}

	public void restaurer() {
		// TODO Auto-generated method stub

		try {
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();

			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			patientSelectionerArchiver=servicepatient.findPatient(Integer.parseInt(request.getParameter("patselection:pat_selection")));
			servicepatient.restaurer(patientSelectionerArchiver);
			facesMessages.add("Restauré avec succés.");
			redirect.setViewId("/patient/liste_patient_archiver.xhtml");
			redirect.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	/**
	 * @return the patientSelectionerArchiver
	 */
	public PatientBean getPatientSelectionerArchiver() {
		return patientSelectionerArchiver;
	}

	/**
	 * @param patientSelectionerArchiver the patientSelectionerArchiver to set
	 */
	public void setPatientSelectionerArchiver(PatientBean patientSelectionerArchiver) {
		this.patientSelectionerArchiver = patientSelectionerArchiver;
	}
	
	public String GenererNumeroDossier(){
		IServicePatient servicepatient = (IServicePatient) Component
				.getInstance("patientService");
		String ref = "";
		paraNumDossier =servicepatient.findParametreByCode(ConstUtil.CodeParaDernierNumDossier);
		Integer dernierNumDoss = paraNumDossier != null ?paraNumDossier.getValeur().intValue():null;
		
		if (dernierNumDoss!=null)
			ref = Integer.toHexString(dernierNumDoss.intValue()).toUpperCase();
		ref = StringUtil.formater(ref, 4, '0');

		if (ref.length()>0)
			return Calendar.getInstance().get(Calendar.YEAR) + ref;
		
		return null;
	}
	
	public void incParametreNumeroDossier(){
		try {
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			if (paraNumDossier != null){
				paraNumDossier.setValeur(new Integer(paraNumDossier.getValeur().intValue()+1));
				servicepatient.saveparametreNumDossier(paraNumDossier);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("--------- Erreur Incrementation Numero Dossier-------");
			e.printStackTrace();
		}
	}
	
	public void verifierExistanceMatricule(){
		try {
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			
			Integer client = new Integer(request.getParameter("formNouveauPatient:client_input"));
			String matricule = request.getParameter("formNouveauPatient:matricule");
			
			System.out.println("Client-> "+client.intValue()+" "+matricule);
			
			BeneficiaireBean beneficiaire =servicepatient.findBeneficiaireByMatricule(client,matricule);

			if (beneficiaire != null)
				this.patientBean.setBeneficiaire(beneficiaire);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void listeAutresAyantsDroit(){
		try {
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();
			IServicePatient servicepatient = (IServicePatient) Component
					.getInstance("patientService");
			if (listeAyantDroits !=null && !listeAyantDroits.isEmpty())
				listeAyantDroits.clear();
			BeneficiaireBean beneficiaire =servicepatient.findBeneficiaireByMatricule(request.getParameter("formNouveauPatient:matricule"));
			if (beneficiaire !=null){
				listeAyantDroits=servicepatient.listePatientsByBeneficiaire(beneficiaire);
				System.out.println("---mess ->: "+listeAyantDroits.size());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}

	public List<PatientBean> getListeAyantDroits() {
		return listeAyantDroits;
	}

	public void setListeAyantDroits(List<PatientBean> listeAyantDroits) {
		this.listeAyantDroits = listeAyantDroits;
	}
	
	public boolean isIdentique() {
		return identique;
	}

	public void setIdentique(boolean identique) {
		this.identique = identique;
	}

	public void updateBeneficiaire(){
		try {
			HttpServletRequest request = (HttpServletRequest) extCtx
					.getRequest();
			String lien = request.getParameter("formNouveauPatient:lien");
			String nom = request.getParameter("formNouveauPatient:nom");
			String prenom = request.getParameter("formNouveauPatient:prenom");
			String mail = request.getParameter("formNouveauPatient:mail");
			String tel = request.getParameter("formNouveauPatient:telephoneMob");
			
			tel = StringUtil.isNullOrEmpty(tel)?request.getParameter("formNouveauPatient:telephoneFixe"):tel;
			
			if (!StringUtil.isNullOrEmpty(lien) && lien.equals("1")){
				setIdentique(true);
				if (!StringUtil.isNullOrEmpty(nom)){
					this.patientBean.getBeneficiaire().setNom(nom);
					RequestContext.getCurrentInstance().update("formNouveauPatient:nommf");
				}
				if (!StringUtil.isNullOrEmpty(prenom)){
					this.patientBean.getBeneficiaire().setPrenom(prenom);
					RequestContext.getCurrentInstance().update("formNouveauPatient:prenommf");
				}
				if (!StringUtil.isNullOrEmpty(mail)){
					this.patientBean.getBeneficiaire().setEmail(mail);
					RequestContext.getCurrentInstance().update("formNouveauPatient:mailmf");
				}
				if (!StringUtil.isNullOrEmpty(tel)){
					this.patientBean.getBeneficiaire().setTelephone(tel);
					RequestContext.getCurrentInstance().update("formNouveauPatient:telephonemf");
				}
			}
			else{
				setIdentique(false);
				RequestContext.getCurrentInstance().update("formNouveauPatient:nommf");
				RequestContext.getCurrentInstance().update("formNouveauPatient:prenommf");
				RequestContext.getCurrentInstance().update("formNouveauPatient:mailmf");
				RequestContext.getCurrentInstance().update("formNouveauPatient:telephonemf");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Exception-> "+e.getMessage());
			e.printStackTrace();
		}
	}
	public void verifierPhoto(){
		facesMessages.add("Photo enregistré avec succès");
		System.out.println("type fichier = "+typeFichier);
		RequestContext.getCurrentInstance().update("form_dialog:msgtof");
		
	}
	public void enregistrerPhotoPatient(){
		try{
			IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
			String photopatient = ApplicationContexteAction.getRealPathPhoto() + "p"+patientSelectioner.getNumDossier()+"."+typeFichier;
			patientSelectioner.setPhoto("p"+patientSelectioner.getNumDossier()+"."+typeFichier);
			servicepatient.savepatient(patientSelectioner);
			FileOutputStream fileOuputStream = new FileOutputStream(photopatient); 
		    fileOuputStream.write(file);
		    fileOuputStream.close();
		    facesMessages.add("La photo est enregistré avec succés.");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("ok type fichier = "+typeFichier);
		
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getTypeFichier() {
		return typeFichier;
	}

	public void setTypeFichier(String typeFichier) {
		this.typeFichier = typeFichier;
	}
	

}
