/**
 * 
 */
package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;

import javax.ejb.Local;

import sn.org.seysoo.medicis.entity.CaracteristiquesBean;
import sn.org.seysoo.medicis.entity.PatientBean;

/**
 * @author OmGue
 *
 */

@Local
public interface ITableauBord {
	
	public void initTableauBord(PatientBean patient);
	
	public String getAllCaracteristiquesPatient();
	
	public void saveAntecdentsPatient();
	
	public void saveCaracteristique(int typeCaract);
	
	public PatientBean getPatient();

	public void setPatient(PatientBean patient);
	
	public CaracteristiquesBean getCaractTaille();

	public void setCaractTaille(CaracteristiquesBean caractTaille);
	
	public CaracteristiquesBean getCaractTemperature();

	public void setCaractTemperature(CaracteristiquesBean caractTemperature);

	public CaracteristiquesBean getCaractPoids();

	public void setCaractPoids(CaracteristiquesBean caractPoids);

	public CaracteristiquesBean getCaractPa();

	public void setCaractPa(CaracteristiquesBean caractPa);

	public CaracteristiquesBean getCaractPouls();

	public void setCaractPouls(CaracteristiquesBean caractPouls);

	public List<CaracteristiquesBean> getCaractTailleList();

	public void setCaractTailleList(List<CaracteristiquesBean> caractTailleList);

	public List<CaracteristiquesBean> getCaractTemperatureList();

	public void setCaractTemperatureList(List<CaracteristiquesBean> caractTemperatureList);

	public List<CaracteristiquesBean> getCaractPoidsList();

	public void setCaractPoidsList(List<CaracteristiquesBean> caractPoidsList);

	public List<CaracteristiquesBean> getCaractPaList();

	public void setCaractPaList(List<CaracteristiquesBean> caractPaList);

	public List<CaracteristiquesBean> getCaractPoulsList();

	public void setCaractPoulsList(List<CaracteristiquesBean> caractPoulsList);
	
	public String getPaValues();

	public void setPaValues(String paValues);

	public void destroy();
}
