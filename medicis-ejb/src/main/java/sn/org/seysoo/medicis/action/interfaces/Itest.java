package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;

import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;

public interface Itest {
	public String ajouter();

	public String ajouterEx();

	public PatientBean getInfoPatient();

	public void setInfoPatient(PatientBean infoPatient);

	public String testdcm();
	
	public void testConve();

}
