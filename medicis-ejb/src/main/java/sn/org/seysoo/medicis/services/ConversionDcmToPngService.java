package sn.org.seysoo.medicis.services;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import javax.ejb.Stateful;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.dcm4che2.imageio.plugins.dcm.DicomImageReadParam;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IConversionDcmToPngService;

import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.PNGEncodeParam;


import javax.ejb.Remove;

/**
 * @author dbc
 * 
 */
@Stateful
@Name("conversionDcmToPngService")
@Scope(ScopeType.SESSION)
public class ConversionDcmToPngService implements IConversionDcmToPngService {
	int n = 0;
	private int number = 0;
	private BufferedImage myPngImage = null;

	/**
	 * @param file
	 * @throws IOException
	 */

	public void dcmconvpng3(File file, int indice, File fileOutput) {
		System.out.println("fichier " + file.getName() + "  Indice " + indice
				+ " path " + fileOutput.getAbsolutePath());
		System.out.println(file.getName());
		try {
			Iterator<ImageReader> iter = ImageIO
					.getImageReadersByFormatName("DICOM");// specifation dicom
			System.out.println(iter.hashCode());
			// while (iter.hasNext()){
			ImageReader readers = iter.next();

			DicomImageReadParam param = (DicomImageReadParam) readers
					.getDefaultReadParam();

			ImageInputStream iis = ImageIO.createImageInputStream(file);

			readers.setInput(iis, true);

			myPngImage = readers.read(indice, param);
			fileOutput.mkdirs();
			File myPngFile = new File(fileOutput + "/" + file.getName()	+ ".png"); // cr�ation du fichier
			OutputStream output = new BufferedOutputStream(
					new FileOutputStream(myPngFile));
			PNGEncodeParam.RGB param2 = new PNGEncodeParam.RGB();
			ImageEncoder enc = ImageCodec.createImageEncoder("PNG", output,
					param2);
			enc.encode(myPngImage);
			output.close();// fermeture du fichier

			// }
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println("message" + e.getMessage());
			e.printStackTrace();
		}
	}
	@Remove
	public void remove() {
	}
}
