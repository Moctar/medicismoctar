package sn.org.seysoo.medicis.action;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.ServletContexts;

import sn.org.seysoo.medicis.Authenticator;
import sn.org.seysoo.medicis.action.interfaces.IUtilisateurService;
import sn.org.seysoo.medicis.entity.UtilisateurBean;
import sn.org.seysoo.medicis.util.ConstUtil;

/**
 * 
 */

/**
 * @author Khadija
 *
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("authenticator")
public class AuthenticatorAction implements Authenticator {

	/* (non-Javadoc)
	 * @see sn.org.seysoo.medicis.Authenticator#authenticate()
	 */
	@In
	Identity identity;
	
	@In(value = "#{facesContext}")
	FacesContext facesContext;
	
	@In(create=true,required=false)
	@Out(required=false, scope =ScopeType.SESSION)
	private UtilisateurBean userconnecte;
	
	@In(create=true,required=false)
	@Out(required=false, scope =ScopeType.SESSION)
	String date1;
	@In(create=true,required=false)
	@Out(required=false, scope =ScopeType.SESSION)
	String date2;
	public boolean authenticate() {
		IUtilisateurService serviceUser = (IUtilisateurService) Component.getInstance("utilisateurService");
		userconnecte = serviceUser.findUtilisateurByLoginMotdepasse(identity.getUsername(), identity.getPassword());
		
		if(userconnecte!=null){
			System.out.println("utilisateur connect�   =  "  + identity.getCredentials().getUsername()+"  " + identity.getCredentials().getUsername());
			
			//Definition de la dur�e de la session utilisateur
	    	ServletContexts.getInstance().getRequest().getSession().setMaxInactiveInterval(ConstUtil.SESSION_TIMEOUT);
	    	System.out.println("---session-timeout : "+ConstUtil.SESSION_TIMEOUT);
	    	
			return true;
		}
		else{
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Login ou mot de passe incorrect.", ""));
			return false;
		}
	}
	
	public String getDate1() {
		Date aujourdhui = new Date();
		SimpleDateFormat formater = null;
		DateFormat fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
		fullDateFormat.format(aujourdhui);
		formater = new SimpleDateFormat("dd MMMM yyyy");
		date1=formater.format(aujourdhui);
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		Date aujourdhui = new Date();
		SimpleDateFormat formater = null;
		DateFormat fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
		fullDateFormat.format(aujourdhui);
		formater = new SimpleDateFormat("EEEE, HH:mm");
		date2=formater.format(aujourdhui);
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	@Remove
	@Destroy
	public void destroy(){
		
	}

}
