/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:23:33
 * @URL		 :PatientAction.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du S�negal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.action;

import static org.jboss.seam.ScopeType.SESSION;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.jsf.SetDataModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

import sn.org.seysoo.medicis.action.interfaces.IDiagnosticAction;
import sn.org.seysoo.medicis.action.interfaces.IRendezvousServices;
import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.action.interfaces.ITableauBord;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.ImageBean;
import sn.org.seysoo.medicis.entity.InfosIrmBean;
import sn.org.seysoo.medicis.entity.InterpretationBean;
import sn.org.seysoo.medicis.entity.InterpretationTypeBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.TechniqueUtiliseBean;
import sn.org.seysoo.medicis.entity.UtilisateurBean;
import sn.org.seysoo.medicis.util.ConstUtil;

/**
 * @author hp
 * 
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("diagnosticAction")
public class DiagnosticAction implements IDiagnosticAction {
	@In
	Redirect redirect;

	@In(value = "#{facesContext}")
	FacesContext facesContext;

	@In
	private FacesMessages facesMessages;

	@In(create = true, required = false)
	@Out(required = false, scope = ScopeType.SESSION)
	private UtilisateurBean userconnecte;

	private RendezVousBean rendezVous;

	private InterpretationTypeBean interpretationType;

	private boolean infoIrmDejaPresente;

	private List<RendezVousBean> rendezVousToday;

	private InterpretationBean interpretationSelected;

	/**
	 * @return the rendezVousToday
	 */
	public List<RendezVousBean> getRendezVousToday() {
		return rendezVousToday;
	}

	/**
	 * @param rendezVousToday
	 *            the rendezVousToday to set
	 */
	public void setRendezVousToday(List<RendezVousBean> rendezVousToday) {
		this.rendezVousToday = rendezVousToday;
	}

	/**
	 * @return the rvSelected
	 */
	public RendezVousBean getRendezVous() {
		return rendezVous;
	}

	/**
	 * @param rvSelected
	 *            the rvSelected to set
	 */
	public void setRendezVous(RendezVousBean rendezVous) {
		this.rendezVous = rendezVous;
	}

	@In(create = true, required = false)
	@Out(required = false, scope = SESSION)
	private PatientBean patientSelectioner;

	private int groupeExamen;

	private int etapeSaisie;

	private Boolean examenIRM;

	private List<GroupeExamenBean> listeTypesNonIrm;

	private List<MedecinBean> listeMedecinsRadiologie;

	private List<SalleBean> listeSallesRadiologie;

	private List<InterpretationTypeBean> listeInterpretationsTypeRadiologie;

	private List<TechniqueUtiliseBean> listeTechniquesRadiologie;

	private DualListModel<ExamenBean> listeExamensType;

	private SetDataModel diagnosticsDataModel;

	@DataModel(value = "listeDiagnostics")
	private List<DiagnosticBean> listeDiagnostics;

	private DiagnosticBean diagnosticChoisis;

	private DiagnosticBean nouveauDiagnostic;

	private InterpretationBean nouvelleInterpretation;

	private List<DiagnosticBean> diagnosticsAnterieursOdontoligie;
	private List<DiagnosticBean> diagnosticsProchainsOdontoligie;
	private List<DiagnosticBean> diagnosticsAnterieursRadiologie;
	private List<DiagnosticBean> diagnosticsProchainsRadiologie;
	private List<DiagnosticBean> diagnosticsAnterieursGynecologie;
	private List<DiagnosticBean> diagnosticsProchainsGynecologie;

	private List<ImageBean> listeImages;
	private List<String> selectedImages;

	private Map<String, String> imagesMap = new HashMap<String, String>();

	private DiagnosticBean dernierDiagnosticIrm;

	public DiagnosticAction() {
		System.out.println("appel constructeur DiagnosticAction");

		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");

		String service = ConstUtil.SERVICE_RADIOLOGIE;
		listeMedecinsRadiologie = serviceDiagnostic
				.findMedecinsService(service);
		listeSallesRadiologie = serviceDiagnostic.findSallesService(service);
		listeInterpretationsTypeRadiologie = serviceDiagnostic.findInterpretationTypeService(service);
		listeTechniquesRadiologie = serviceDiagnostic.findTechniquesService(service);

		rendezVousToday = new ArrayList<RendezVousBean>();

	}

	@Remove
	public void remove() {
	}

	@Destroy
	public void destroy() {
	}

	public String nouveauDiagnosticRadiologie() {
		this.initialisationNouveauDiagnosticRadiologie();
		return "";
	}

	public void initialisationNouveauDiagnosticRadiologie() {
		try {
			System.out
			.println("appel initialisationNouveauDiagnosticRadiologie");
			if (rendezVous != null) {
				examenIRM = true;
				nouveauDiagnostic = new DiagnosticBean();
				InfosIrmBean infosIrm = new InfosIrmBean();
				nouveauDiagnostic.setInfosIrm(infosIrm);
				diagnosticsDataModel = new SetDataModel();
				etapeSaisie = 1;
				groupeExamen = 0;
				if (listeExamensType != null) {
					listeExamensType.setTarget(null);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			rendezVous = null;
			e.printStackTrace();
		}
	}

	/**
	 * @return the examenIRM
	 */
	public Boolean getExamenIRM() {
		return examenIRM;
	}

	/**
	 * @param examenIRM
	 *            the examenIRM to set
	 */
	public void setExamenIRM(Boolean examenIRM) {
		this.examenIRM = examenIRM;
	}

	@Factory("listeTypesNonIrm")
	public List<GroupeExamenBean> listeTypesNonIrm() {
		System.out.println("appel Factory listeTypesNonIrm");
		try {

			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");
			listeTypesNonIrm = serviceDiagnostic.findListeTypesNonIrm();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return listeTypesNonIrm;
	}

	@Factory("listeExamenNonIrm")
	public List<ExamenBean> listeExamenNonIrm() {
		System.out.println("appel Factory listeTypesNonIrm");
		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");
		String service = ConstUtil.SERVICE_RADIOLOGIE;
		List<ExamenBean> examens = serviceDiagnostic
				.findExamensType(0, service);
		return examens;
	}

	@Factory("listeImages")
	public List<ImageBean> listeImagesDiagnostic() {
		try {

			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");
			listeImages = serviceDiagnostic.findListeImagesDiagnostic();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return listeImages;
	}

	public String saveDiagnosticRadiologie() {
		System.out.println("appel saveDiagnosticRadiologie");
		try {
			String etat = "ok";
			Set<DiagnosticBean> listeDiagnostics = (Set<DiagnosticBean>) diagnosticsDataModel
					.getWrappedData();
			for (Iterator iterator = listeDiagnostics.iterator(); iterator
					.hasNext();) {
				DiagnosticBean diagnosticBean = (DiagnosticBean) iterator
						.next();
				System.out.println("Date examen " + diagnosticBean.getDate());

				diagnosticBean.setRendezVous(rendezVous);
				IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
						.getInstance("diagnosticService");
				serviceDiagnostic.saveDiagnostic(diagnosticBean);
			}
			this.initialisationNouveauDiagnosticRadiologie();
			facesMessages.add("Examen ajouté avec succès");
			return etat;
		} catch (Exception e) {
			e.printStackTrace();
			String etat = "non ok";
			return etat;
		}
	}

	public String modifierDiagnosticRadiologie() {
		System.out.println("appel modifierDiagnosticRadiologie"
				+ diagnosticChoisis.getDate());
		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");
		serviceDiagnostic.saveDiagnostic(diagnosticChoisis);

		facesMessages.add("Examen modifié avec succès");
		return "";
	}

	public String supprimerDiagnostic() {
		System.out.println("appel supprimerDiagnostic");
		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");
		serviceDiagnostic.supprimerDiagnostic(diagnosticChoisis);

		facesMessages.add("Examen supprimé avec succès");
		return "";
	}

	/**
	 * @return the listeExamensType
	 */
	public DualListModel<ExamenBean> getListeExamensType() {
		try {
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");
			String service = ConstUtil.SERVICE_RADIOLOGIE;
			List<ExamenBean> source = serviceDiagnostic.findExamensType(
					this.groupeExamen, service);
			if (listeExamensType != null
					&& listeExamensType.getTarget() != null) {
				listeExamensType.setSource(source);
			} else {
				List<ExamenBean> target = new ArrayList<ExamenBean>();
				source = new ArrayList<ExamenBean>();
				listeExamensType = new DualListModel<ExamenBean>(source, target);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return listeExamensType;
	}

	/**
	 * @param listeExamensType
	 *            the listeExamensType to set
	 */
	public void setListeExamensType(DualListModel<ExamenBean> listeExamensType) {
		this.listeExamensType = listeExamensType;
	}

	/**
	 * @return the groupeExamen
	 */
	public int getGroupeExamen() {
		return groupeExamen;
	}

	/**
	 * @param groupeExamen
	 *            the groupeExamen to set
	 */
	public void setGroupeExamen(int groupeExamen) {
		this.groupeExamen = groupeExamen;
	}

	/**
	 * @return the etapeSaisie
	 */
	public int getEtapeSaisie() {
		return etapeSaisie;
	}

	/**
	 * @param etapeSaisie
	 *            the etapeSaisie to set
	 */
	public void setEtapeSaisie(int etapeSaisie) {
		this.etapeSaisie = etapeSaisie;
	}

	public String etapeSuivant() {
		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");
		Set<DiagnosticBean> listDiagnostics = new HashSet<DiagnosticBean>();
		if (examenIRM) {
			ExamenBean examenBean = serviceDiagnostic.findExamenIrm();
			nouveauDiagnostic.setExamen(examenBean);
			MedecinBean medecinBean = new MedecinBean();
			nouveauDiagnostic.setMedecin(medecinBean);
			SalleBean salleBean = new SalleBean();
			nouveauDiagnostic.setSalle(salleBean);
			nouveauDiagnostic.setEtat(ConstUtil.ETAT_DIAGNOSTIC_OUVERT);
			listDiagnostics.add(nouveauDiagnostic);
		} else {
			List<ExamenBean> examensSelectionnes = listeExamensType.getTarget();
			for (Iterator iterator = examensSelectionnes.iterator(); iterator
					.hasNext();) {
				ExamenBean examenBean = (ExamenBean) iterator.next();
				DiagnosticBean diagnosticBean = new DiagnosticBean();
				diagnosticBean.setExamen(examenBean);
				MedecinBean medecinBean = new MedecinBean();
				diagnosticBean.setMedecin(medecinBean);
				SalleBean salleBean = new SalleBean();
				diagnosticBean.setSalle(salleBean);
				diagnosticBean.setInfosIrm(null);
				diagnosticBean.setEtat(ConstUtil.ETAT_DIAGNOSTIC_OUVERT);
				listDiagnostics.add(diagnosticBean);
			}
		}
		if (listDiagnostics.size() != 0) {
			diagnosticsDataModel.setWrappedData(listDiagnostics);
			etapeSaisie = 2;
			// scroll to panel
			RequestContext context = RequestContext.getCurrentInstance();
			context.scrollTo("form-nouvel-examen-radiologie:nouvel_examen_radiologie");
		} else {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation",
					"Veuillez choisir un examen.");
			RequestContext.getCurrentInstance().showMessageInDialog(message);
			etapeSaisie = 1;
		}
		return "";
	}

	public String etapePrecedente() {
		etapeSaisie = 1;
		return "";
	}

	/**
	 * @return the diagnosticsDataModel
	 */
	public SetDataModel getDiagnosticsDataModel() {
		return diagnosticsDataModel;
	}

	/**
	 * @param diagnosticsDataModel
	 *            the diagnosticsDataModel to set
	 */
	public void setDiagnosticsDataModel(SetDataModel diagnosticsDataModel) {
		this.diagnosticsDataModel = diagnosticsDataModel;
	}

	/**
	 * @return the listeMedecinsRadiologie
	 */
	public List<MedecinBean> getListeMedecinsRadiologie() {
		return listeMedecinsRadiologie;
	}

	/**
	 * @param listeMedecinsRadiologie
	 *            the listeMedecinsRadiologie to set
	 */
	public void setListeMedecinsRadiologie(
			List<MedecinBean> listeMedecinsRadiologie) {
		this.listeMedecinsRadiologie = listeMedecinsRadiologie;
	}

	/**
	 * @return the listeSallesRadiologie
	 */
	public List<SalleBean> getListeSallesRadiologie() {
		return listeSallesRadiologie;
	}

	/**
	 * @param listeSallesRadiologie
	 *            the listeSallesRadiologie to set
	 */
	public void setListeSallesRadiologie(List<SalleBean> listeSallesRadiologie) {
		this.listeSallesRadiologie = listeSallesRadiologie;
	}

	/**
	 * @return the listeDiagnostics
	 */
	public List<DiagnosticBean> getListeDiagnostics() {
		return listeDiagnostics;
	}

	/**
	 * @param listeDiagnostics
	 *            the listeDiagnostics to set
	 */
	public void setListeDiagnostics(List<DiagnosticBean> listeDiagnostics) {
		this.listeDiagnostics = listeDiagnostics;
	}

	public String initialisationFicheConsultationRadiologie() {
		
		try {
			String etat = "ok";
			if (rendezVous != null) {
				nouvelleInterpretation = new InterpretationBean();
				System.out.println("init nouvelleInterpretation");
				TechniqueUtiliseBean techniqueUtiliseBean = new TechniqueUtiliseBean();
				interpretationType = new InterpretationTypeBean();
				nouvelleInterpretation.setTechniqueUtilise(techniqueUtiliseBean);
				IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
						.getInstance("diagnosticService");
				int idRv = rendezVous.getId();
				listeDiagnostics = serviceDiagnostic
						.findDiagnosticsRendezVous(idRv);

				nouveauDiagnostic = new DiagnosticBean();
				int idPatient = patientSelectioner.getId();
				dernierDiagnosticIrm = serviceDiagnostic
						.dernierDiagnosticIrm(idPatient);
				if (dernierDiagnosticIrm != null
						&& dernierDiagnosticIrm.getInfosIrm() != null) {
					infoIrmDejaPresente = true;
				} else {
					infoIrmDejaPresente = false;
				}
				InfosIrmBean infosIrm = new InfosIrmBean();
				nouveauDiagnostic.setInfosIrm(infosIrm);
				System.out.println("infoIrmDejaPresente " +(infoIrmDejaPresente ? "Oui " : "Non " ));
			}
			else{
				etat = "non ok";
			}
			return etat;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			rendezVous = null;
			String etat = "non ok";
			return etat;
		}
	}

	public void saveInterpretation() {
		try {
			System.out.println("appel saveInterpretation");
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");

			if(nouvelleInterpretation.getTechniqueUtilise().getId() == 0){
				nouvelleInterpretation.setTechniqueUtilise(null);
			}

			nouvelleInterpretation.setDiagnostic(diagnosticChoisis);
			Date date = new Date();
			nouvelleInterpretation.setDate(date);
			nouvelleInterpretation.setUtilisateur(userconnecte);
			serviceDiagnostic.saveInterpretations(nouvelleInterpretation);
			diagnosticChoisis.getInterpretations().add(nouvelleInterpretation);// Mettre
			// a
			// jour
			// les
			// interpretations
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void modifierInterpretationExamen() {
		try {
			System.out.println("appel modifierInterpretationExamen");
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");
			serviceDiagnostic.saveInterpretations(interpretationSelected);
			diagnosticChoisis = serviceDiagnostic.findDiagnostic(diagnosticChoisis.getId()); // recuperer les interpretations a jour
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void saveInterpretationRv(){
		try {
			System.out.println("appel saveInterpretationRv "+ rendezVous.getResultat());
			IRendezvousServices serviceRV = (IRendezvousServices) Component
					.getInstance("rendezvousServices");
			// a faire : validation de lexamen ou du RV???
			serviceRV.saveRendezvous(rendezVous);
			facesMessages.add("Diagnostic enregistré avec succès");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@DataModel
	private List<InterpretationBean> listeInterpretation;

	/**
	 * @return the nouvelleInterpretation
	 */
	public InterpretationBean getNouvelleInterpretation() {
		return nouvelleInterpretation;
	}

	/**
	 * @param nouvelleInterpretation
	 *            the nouvelleInterpretation to set
	 */
	public void setNouvelleInterpretation(
			InterpretationBean nouvelleInterpretation) {
		this.nouvelleInterpretation = nouvelleInterpretation;
	}

	/**
	 * @return the listeInterpretation
	 */
	public List<InterpretationBean> getListeInterpretation() {
		listeInterpretation = new ArrayList<InterpretationBean>();
		if (diagnosticChoisis != null) {
			Set<InterpretationBean> ints = diagnosticChoisis
					.getInterpretations();
			for (InterpretationBean inter : ints) {
				listeInterpretation.add(inter);
			}
		}
		return listeInterpretation;
	}

	/**
	 * @param listeInterpretation
	 *            the listeInterpretation to set
	 */
	public void setListeInterpretation(
			List<InterpretationBean> listeInterpretation) {
		this.listeInterpretation = listeInterpretation;
	}

	/**
	 * @return the nouveauDiagnostic
	 */
	public DiagnosticBean getNouveauDiagnostic() {
		return nouveauDiagnostic;
	}

	/**
	 * @param nouveauDiagnostic
	 *            the nouveauDiagnostic to set
	 */
	public void setNouveauDiagnostic(DiagnosticBean nouveauDiagnostic) {
		this.nouveauDiagnostic = nouveauDiagnostic;
	}

	/**
	 * @return the diagnosticsAnterieursOdontoligie
	 */
	public List<DiagnosticBean> getDiagnosticsAnterieursOdontoligie() {
		return diagnosticsAnterieursOdontoligie;
	}

	/**
	 * @param diagnosticsAnterieursOdontoligie
	 *            the diagnosticsAnterieursOdontoligie to set
	 */
	public void setDiagnosticsAnterieursOdontoligie(
			List<DiagnosticBean> diagnosticsAnterieursOdontoligie) {
		this.diagnosticsAnterieursOdontoligie = diagnosticsAnterieursOdontoligie;
	}

	/**
	 * @return the diagnosticsProchainsOdontoligie
	 */
	public List<DiagnosticBean> getDiagnosticsProchainsOdontoligie() {
		return diagnosticsProchainsOdontoligie;
	}

	/**
	 * @param diagnosticsProchainsOdontoligie
	 *            the diagnosticsProchainsOdontoligie to set
	 */
	public void setDiagnosticsProchainsOdontoligie(
			List<DiagnosticBean> diagnosticsProchainsOdontoligie) {
		this.diagnosticsProchainsOdontoligie = diagnosticsProchainsOdontoligie;
	}

	/**
	 * @return the diagnosticsAnterieursRadiologie
	 */
	public List<DiagnosticBean> getDiagnosticsAnterieursRadiologie() {
		return diagnosticsAnterieursRadiologie;
	}

	/**
	 * @param diagnosticsAnterieursRadiologie
	 *            the diagnosticsAnterieursRadiologie to set
	 */
	public void setDiagnosticsAnterieursRadiologie(
			List<DiagnosticBean> diagnosticsAnterieursRadiologie) {
		this.diagnosticsAnterieursRadiologie = diagnosticsAnterieursRadiologie;
	}

	/**
	 * @return the diagnosticsProchainsRadiologie
	 */
	public List<DiagnosticBean> getDiagnosticsProchainsRadiologie() {
		return diagnosticsProchainsRadiologie;
	}

	/**
	 * @param diagnosticsProchainsRadiologie
	 *            the diagnosticsProchainsRadiologie to set
	 */
	public void setDiagnosticsProchainsRadiologie(
			List<DiagnosticBean> diagnosticsProchainsRadiologie) {
		this.diagnosticsProchainsRadiologie = diagnosticsProchainsRadiologie;
	}

	/**
	 * @return the diagnosticsAnterieursGynecologie
	 */
	public List<DiagnosticBean> getDiagnosticsAnterieursGynecologie() {
		return diagnosticsAnterieursGynecologie;
	}

	/**
	 * @param diagnosticsAnterieursGynecologie
	 *            the diagnosticsAnterieursGynecologie to set
	 */
	public void setDiagnosticsAnterieursGynecologie(
			List<DiagnosticBean> diagnosticsAnterieursGynecologie) {
		this.diagnosticsAnterieursGynecologie = diagnosticsAnterieursGynecologie;
	}

	/**
	 * @return the diagnosticsProchainsGynecologie
	 */
	public List<DiagnosticBean> getDiagnosticsProchainsGynecologie() {
		return diagnosticsProchainsGynecologie;
	}

	/**
	 * @param diagnosticsProchainsGynecologie
	 *            the diagnosticsProchainsGynecologie to set
	 */
	public void setDiagnosticsProchainsGynecologie(
			List<DiagnosticBean> diagnosticsProchainsGynecologie) {
		this.diagnosticsProchainsGynecologie = diagnosticsProchainsGynecologie;
	}

	public String dossierPatient() {
		try {
			if (patientSelectioner != null) {
				int idPatient = patientSelectioner.getId();
				// IServicePatient servicepatient = (IServicePatient)
				// Component.getInstance("patientService");
				// patientSelectioner = servicepatient.findPatient(idPatient);

				int prochin = ConstUtil.ETAT_DIAGNOSTIC_OUVERT;
				int anterieur = ConstUtil.ETAT_DIAGNOSTIC_FERME;
				IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
						.getInstance("diagnosticService");
				Map<String, List<DiagnosticBean>> listeTousDiagnosticsAnterieurs = serviceDiagnostic
						.listeDiagnostics(idPatient, anterieur);
				Map<String, List<DiagnosticBean>> listeTousDiagnosticsProchains = serviceDiagnostic
						.listeDiagnostics(idPatient, prochin);

				/*
				 * String serviceOdontoligie = ConstUtil.SERVICE_ODONTOLOGIE;
				 * diagnosticsAnterieursOdontoligie =
				 * listeTousDiagnosticsAnterieurs .get(serviceOdontoligie);
				 * diagnosticsProchainsOdontoligie =
				 * listeTousDiagnosticsProchains .get(serviceOdontoligie);
				 */

				String serviceRadiologie = ConstUtil.SERVICE_RADIOLOGIE;
				diagnosticsAnterieursRadiologie = listeTousDiagnosticsAnterieurs!=null ? listeTousDiagnosticsAnterieurs .get(serviceRadiologie) : new ArrayList<DiagnosticBean>();
				diagnosticsProchainsRadiologie = listeTousDiagnosticsProchains!=null ? listeTousDiagnosticsProchains .get(serviceRadiologie) : new ArrayList<DiagnosticBean>();

				/*String serviceGynecologie = ConstUtil.SERVICE_GYNECOLOGIE;
				diagnosticsAnterieursGynecologie = listeTousDiagnosticsAnterieurs
						.get(serviceGynecologie);
				diagnosticsProchainsGynecologie = listeTousDiagnosticsProchains
						.get(serviceGynecologie);*/

			}
			System.out.println("---dossierPatient---");
			ITableauBord tableauBord = (ITableauBord) Component.getInstance("tableauBordAction");
			tableauBord.initTableauBord(patientSelectioner);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("---dossierPatient - Exception : "+e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public String ficheConsultationRadiologie() {
		try {
			String etat = "non ok";
			rendezVous = null;
			if (diagnosticChoisis != null) {
				System.out.println("diagnosticChoisis non null "
						+ diagnosticChoisis.getId());
				rendezVous = diagnosticChoisis.getRendezVous();
			} else {
				System.out.println("diagnosticChoisis null ");
				IRendezvousServices serviceRv = (IRendezvousServices) Component
						.getInstance("rendezvousServices");
				RequestContext context = RequestContext.getCurrentInstance();

				Date dateDebut = new Date();
				Date dateFin = new Date();
				dateDebut.setHours(0);
				dateDebut.setMinutes(0);
				dateDebut.setSeconds(0);

				dateFin.setHours(23);
				dateFin.setMinutes(59);
				dateFin.setSeconds(59);

				int idPatient = patientSelectioner.getId();
				rendezVousToday = serviceRv.randezVousPeriode(idPatient,
						dateDebut, dateFin);
				if (rendezVousToday != null && rendezVousToday.size() != 0) {
					if (rendezVousToday.size() == 1) {
						rendezVous = rendezVousToday.get(0);
					} else { // sil ya plusieurs afficher un popup pour choisir
						// le rv
						context.update("tabViewDossier:form-confirmation-choix-fiche-consultation:dg-choix-rv");
						context.execute("dg_choix_rv.show()");
					}
				} else {
					// update actions
					context.execute("confirmation_fiche.show()");
					rendezVous = null;
				}
			}
			if (rendezVous != null) {
				etat = "ok";
			}
			return etat;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			rendezVous = null;
			String etat = "non ok";
			return etat;
		}
	}

	public String selectFicheConsultationRadiologie() {
		System.out.println("appel selectFicheConsultationRadiologie");
		try {
			if (diagnosticChoisis != null
					&& diagnosticChoisis.getRendezVous() != null) {
				String url = "/patient/dossier_patient/fiche_consultation/radiologie.xhtml?rvId="
						+ diagnosticChoisis.getRendezVous().getId();
				;
				return url;
			} else {
				return "";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "";
		}

	}

	/**
	 * @return the listeImages
	 */
	public List<ImageBean> getListeImages() {
		return listeImages;
	}

	/**
	 * @param listeImages
	 *            the listeImages to set
	 */
	public void setListeImages(List<ImageBean> listeImages) {
		this.listeImages = listeImages;
	}

	public String onTrouveRendezVous() {
		if (rendezVous != null) {
			System.out.println("rv non null " + rendezVous.getDate());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dg_choix_rv.hide()");
			return "ok";
		} else {
			System.out.println("rv null");
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Aucun rendez-vous séléctionné.", ""));
			return "";
		}
	}

	/**
	 * @return the infoIrmDejaPresente
	 */
	public boolean isInfoIrmDejaPresente() {
		return infoIrmDejaPresente;
	}

	/**
	 * @param infoIrmDejaPresente
	 *            the infoIrmDejaPresente to set
	 */
	public void setInfoIrmDejaPresente(boolean infoIrmDejaPresente) {
		this.infoIrmDejaPresente = infoIrmDejaPresente;
	}

	/**
	 * @return the dernierDiagnosticIrm
	 */
	public DiagnosticBean getDernierDiagnosticIrm() {
		return dernierDiagnosticIrm;
	}

	/**
	 * @param dernierDiagnosticIrm
	 *            the dernierDiagnosticIrm to set
	 */
	public void setDernierDiagnosticIrm(DiagnosticBean dernierDiagnosticIrm) {
		this.dernierDiagnosticIrm = dernierDiagnosticIrm;
	}

	public Map<String, String> getImagesMap() {

		IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
				.getInstance("diagnosticService");
		listeImages = serviceDiagnostic.findListeImagesDiagnostic();

		for (int i = 0; i < listeImages.size(); i++) {
			imagesMap.put(listeImages.get(i).getImage(), ""
					+ listeImages.get(i).getImage());
		}

		Map<String, String> treeMap = new TreeMap<String, String>(imagesMap);

		imagesMap = treeMap;

		return imagesMap;
	}

	public void setImagesMap(Map<String, String> imagesMap) {
		this.imagesMap = imagesMap;
	}

	public List<String> getSelectedImages() {
		return selectedImages;
	}

	public void setSelectedImages(List<String> selectedImages) {
		this.selectedImages = selectedImages;
	}

	public void affImages() {
		if (selectedImages.size() > 0)
			for (int i = 0; i < selectedImages.size(); i++) {
				System.out
				.println("------ liste des images s�lectionn�es -------");
				System.out.println("images [" + i + "] =  "
						+ selectedImages.get(i));
				System.out
				.println("------ Fin liste des images s�lectionn�es -------");
			}
	}

	public String validationNouvelExamenRadiologie() {
		System.out.println("appel validationNouvelExamenRadiologie");
		Boolean validationFailed = facesContext.isValidationFailed();
		if (validationFailed) {

			RequestContext context = RequestContext.getCurrentInstance();
			context.update("form-nouvel-examen-radiologie");
			// scroll to panel
			context.scrollTo("form-nouvel-examen-radiologie:actions");
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation",
					"Un ou plusieurs champs ne sont pas valides.");

			RequestContext.getCurrentInstance().showMessageInDialog(message);
		}
		return null;
	}

	/**
	 * @return the diagnosticChoisis
	 */
	public DiagnosticBean getDiagnosticChoisis() {
		System.out.println("appel getdiagnosticChoisis "
				+ (diagnosticChoisis != null ? diagnosticChoisis.getId()
						: "null"));
		return diagnosticChoisis;
	}

	/**
	 * @param diagnosticChoisis
	 *            the diagnosticChoisis to set
	 */
	public void setDiagnosticChoisis(DiagnosticBean diagnosticChoisis) {
		System.out.println("appel setdiagnosticChoisis "
				+ (diagnosticChoisis != null ? diagnosticChoisis.getId()
						: "null"));
		this.diagnosticChoisis = diagnosticChoisis;
	}

	/**
	 * @return the interpretationSelected
	 */
	public InterpretationBean getInterpretationSelected() {
		System.out.println(" appel getInterpretationSelected "
				+ (interpretationSelected == null ? " null"
						: interpretationSelected.getLibelle()));
		return interpretationSelected;
	}

	/**
	 * @param interpretationSelected
	 *            the interpretationSelected to set
	 */
	public void setInterpretationSelected(
			InterpretationBean interpretationSelected) {
		System.out.println(" appel setInterpretationSelected "
				+ (interpretationSelected == null ? " null"
						: interpretationSelected.getLibelle()));
		this.interpretationSelected = interpretationSelected;
	}

	/**
	 * @return the interpretationType
	 */
	public InterpretationTypeBean getInterpretationType() {
		return interpretationType;
	}

	/**
	 * @param interpretationType the interpretationType to set
	 */
	public void setInterpretationType(InterpretationTypeBean interpretationType) {
		this.interpretationType = interpretationType;
	}

	public String changeInterpretationType() {
		int idInterpretationType = interpretationType.getId();
		if(idInterpretationType != 0){
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component
					.getInstance("diagnosticService");
			InterpretationTypeBean interType = serviceDiagnostic.findInterpretationType(idInterpretationType);
			if(interType != null && interType.getContenu() != null){
				System.out.println(" intertype " + interType.getContenu());
				nouvelleInterpretation.setLibelle(interType.getContenu());		
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("tabFicheConsultationRadiologie:form_nouvelle_interpretation:editor_nouvelle_interpretation_examen");
			}
		}
		return "";
	}

	/**
	 * @return the listeInterpretationsTypeRadiologie
	 */
	public List<InterpretationTypeBean> getListeInterpretationsTypeRadiologie() {
		return listeInterpretationsTypeRadiologie;
	}

	/**
	 * @param listeInterpretationsTypeRadiologie the listeInterpretationsTypeRadiologie to set
	 */
	public void setListeInterpretationsTypeRadiologie(
			List<InterpretationTypeBean> listeInterpretationsTypeRadiologie) {
		this.listeInterpretationsTypeRadiologie = listeInterpretationsTypeRadiologie;
	}

	/**
	 * @return the listeTechniquesRadiologie
	 */
	public List<TechniqueUtiliseBean> getListeTechniquesRadiologie() {
		return listeTechniquesRadiologie;
	}

	/**
	 * @param listeTechniquesRadiologie the listeTechniquesRadiologie to set
	 */
	public void setListeTechniquesRadiologie(
			List<TechniqueUtiliseBean> listeTechniquesRadiologie) {
		this.listeTechniquesRadiologie = listeTechniquesRadiologie;
	}

}
