/**
 * 
 */
package sn.org.seysoo.medicis.util;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

import com.jaspersoft.mongodb.connection.MongoDbConnection;
import com.mongodb.DB;
import com.mongodb.Mongo;

/**
 * @author Khadija
 *
 */
@Name("MongoUtil")
@Scope(ScopeType.SESSION)
@Startup
public class MongoUtil {
	
	@In(create=true,required=false)
	@Out(required=false, scope =ScopeType.SESSION)
	private DB db; 
	
	@In(create=true,required=false)
	@Out(required=false, scope =ScopeType.SESSION)
	private MongoDbConnection connection;
	@Create
	public void AuDemarrage() {
		Mongo mongo;
		try {
			mongo = new Mongo("127.0.0.1", 27017);
			db = mongo.getDB("medicis");
			System.out.println("Done..............Initialisation de la base de donn�es medicis");
			
			String mongoURI = "mongodb://localhost:27017/medicis";
			connection = new MongoDbConnection(mongoURI,null,null);
			
			System.out.println("Done..............Initialisation de la source de donn�es pour jasper report");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
