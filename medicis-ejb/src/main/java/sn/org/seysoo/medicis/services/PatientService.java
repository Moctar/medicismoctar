/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:38:16
 * @URL		 :PatientService.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du S�negal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import sn.org.seysoo.medicis.action.interfaces.IServicePatient;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.CaracteristiquesBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.ParametreBean;
import sn.org.seysoo.medicis.entity.PatientBean;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;


/**
 * @author hp
 *
 */
@Stateful
@Name("patientService")
@Scope(ScopeType.SESSION)
public class PatientService implements IServicePatient {


	@PersistenceContext
	private EntityManager em;

	@In(create=true,required=false)
	private DB db;

	@Logger
	Log log;
	/* (non-Javadoc)
	 * @see sn.org.seysoo.medicis.action.interfaces.IServicePatient#listeTousLesPatient()
	 */

	@Produces
	public List<PatientBean> listeTousLesPatient() {
		List<PatientBean> listpatient=null;
		try
		{
			DBCollection dbCollection = db.getCollection("patient");
			BasicDBObject query = new BasicDBObject("etatDossier", 0);
			System.out.println("Find all documents in collection:");
			DBCursor cursor = dbCollection.find(query);
			listpatient=new ArrayList<PatientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					PatientBean patient=new PatientBean();
					patient=em.find(PatientBean.class, (Integer) item.get("_id"));
					listpatient.add(patient);
					System.out.println(item);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listpatient;
	}
	public List<PatientBean> listeTousLesPatientArchve() {
		List<PatientBean> listpatientarchive=null;
		try
		{
			DBCollection dbCollection = db.getCollection("patient");
			BasicDBObject query = new BasicDBObject("etatDossier", 1);
			System.out.println("Find all documents in collection:");
			DBCursor cursor = dbCollection.find(query);
			listpatientarchive=new ArrayList<PatientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					PatientBean patient=new PatientBean();
					patient=em.find(PatientBean.class, (Integer) item.get("_id"));						
					listpatientarchive.add(patient);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listpatientarchive;
	}
	public List<GroupeSanguinBean> getAllGoupeSanguins(){
		List<GroupeSanguinBean> listgroupeSanguins=null;
		try
		{
			DBCollection dbCollection = db.getCollection("groupe_sanguin");
			System.out.println("Find all documents in collection:");
			DBCursor cursor = dbCollection.find();
			listgroupeSanguins=new ArrayList<GroupeSanguinBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					GroupeSanguinBean groupeSanguins=new GroupeSanguinBean();
					groupeSanguins.setId((Integer) item.get("_id"));
					groupeSanguins.setLibelle((String) item.get("libelle"));
					listgroupeSanguins.add(groupeSanguins);
					System.out.println(item);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listgroupeSanguins;
	}

	public PatientBean findPatient(Integer id) {
		// TODO Auto-generated method stub
		PatientBean patient = em.find(PatientBean.class, id);
		return patient;
	}
	@Produces
	public List<PatientBean> findPatient(String nom,String prenom,String cni,Date dateNaissnce){
		List<PatientBean> listpatient=null;
		try
		{
			DBCollection dbCollection = db.getCollection("patient");
			BasicDBObject regexQuery = new BasicDBObject();
			regexQuery.put("nom", new BasicDBObject("$regex", ".*"+nom)
            .append("$options", "i"));
			regexQuery.put("prenom", new BasicDBObject("$regex", ".*"+prenom)
            .append("$options", "i"));
            regexQuery.put("cni", new BasicDBObject("$regex", ".*"+cni+".*")
            .append("$options", "i"));
			System.out.println("Find all documents in collection:" +regexQuery.toString());
			DBCursor cursor = dbCollection.find(regexQuery);
			listpatient=new ArrayList<PatientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					PatientBean patient=new PatientBean();
					patient.setId((Integer) item.get("_id"));
					patient.setNom((String) item.get("nom"));
					patient.setPrenom((String) item.get("prenom"));
					patient.setDateNissance((Date) item.get("dateNissance"));
					patient.setAdresse((String) item.get("adresse"));
					patient.setTelephonePatient((String) item.get("telephonePatient"));
					patient.setPoids((Double) item.get("poids"));
					patient.setTaille((Double) item.get("taille"));
					listpatient.add(patient);
					System.out.println(item);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listpatient;

	}
	public void savepatient(PatientBean patient){
		try{
			System.out.println("Id beforeInsertion ="+patient.getId());
			if(patient.getBeneficiaire()!=null){
				patient.setBeneficiaire(em.merge(patient.getBeneficiaire()));
			}
			patient.setEtatDossier(0);
			em.merge(patient);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	public PatientBean saveAndReturnpatient(PatientBean patient){
		try{
			System.out.println("Id beforeInsertion ="+patient.getId());
			if(patient.getBeneficiaire()!=null){
				patient.setBeneficiaire(em.merge(patient.getBeneficiaire()));
			}
			patient.setEtatDossier(0);
			return em.merge(patient);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Remove
	@Destroy
	public void destroy() {
	}
	public PatientBean archiver(PatientBean patient) {
		try{
			if(patient!=null){
				patient.setEtatDossier(1);
				em.merge(patient);
			}		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return patient;
	}
	public PatientBean restaurer(PatientBean patient) {
		try{
			if(patient!=null){
				patient.setEtatDossier(0);
				em.merge(patient);
			}		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return patient;
	}
	public ParametreBean findParametreByCode(String code){
		ParametreBean para = null;
		
		try {
			DBCollection dbCollection = db.getCollection("parametre");
			BasicDBObject query = new BasicDBObject("code", code);
			DBObject cursor = dbCollection.findOne(query);
			if(cursor!=null){
				para=em.find(ParametreBean.class, (Integer) cursor.get("_id"));
			}
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return para;
	}
	
	public void saveparametreNumDossier(ParametreBean para){
		try{
			System.out.println("Id beforeInsertion ="+para.getId());
			para = em.merge(para);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public Integer findDernierNumDoss(){
		Integer result=null;
		try {
			DBCollection dbCollection = db.getCollection("patient");
			BasicDBObject query = new BasicDBObject("_id", -1);
			DBCursor cursor = dbCollection.find().sort(query);
			if(cursor!=null && cursor.size()>0){
				result = (Integer) cursor.next().get("_id");
			}
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return result;
	}
	
	/**
	 * saveCaracteristiques(CaracteristiquesBean caracteristiques)
	 * @param caracteristiques
	 * 
	 * Sauvegarde dans la BD les carateristiques du patient
	 */
	public void saveCaracteristiques(CaracteristiquesBean caracteristiques) {
		try {
			caracteristiques = em.merge(caracteristiques);
			em.flush();
		}
		catch (Exception e) {
			//System.out.println("---PatientService - saveCaracteristiques : "+e.getMessage());
			//e.printStackTrace();
		}
	}
	
	public CaracteristiquesBean findCaracteristiques(Integer id) {
		CaracteristiquesBean caract = null;
		
		return caract;
	}
	
	/**
	 * getAllCaracteristiquesPatient(Integer patient_id)
	 * 
	 * Retourne toutes les caracteristiques d'un patient
	 */
	public List<CaracteristiquesBean> getAllCaracteristiquesPatient(Integer patient_id) {
		List<CaracteristiquesBean> caractList = null;

		try {
			DBCollection dbCollection = db.getCollection("caracteristiques");
			BasicDBObject query = new BasicDBObject("patient_id", patient_id);
			DBCursor cursor = dbCollection.find(query);
			if(cursor!=null && cursor.size()>0){
				caractList = new ArrayList<CaracteristiquesBean>();
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					CaracteristiquesBean car = new CaracteristiquesBean();
					car.setId((Integer) item.get("_id"));
//					car.setPouls((Double) item.get("pouls"));
					car.setValeur((Integer) item.get("valeur"));
					car.setValeur2((Integer) item.get("valeur2"));
					car.setDate((Date) item.get("date"));
					car.setTypeCaracteristiqueId((Integer) item.get("type_caracteristique_id"));
					caractList.add(car);
				}
			}
		}
		catch (MongoException e) {
			//System.out.println("---PatientService - getAllCaracteristiquesPatient - MongoException : "+e.getMessage());
			//e.printStackTrace();
			return null;
		}
		catch (Exception e) {
			//System.out.println("---PatientService - getAllCaracteristiquesPatient - Exception : "+e.getMessage());
			//e.printStackTrace();
			return null;
		}

		return caractList;
	}
	
	/**
	 * getAllCaracteristiquesPatientByType(Integer patient_id, Integer type_caracteristique_id)
	 * 
	 * Retourne toutes les caracteristiques d'un patient par type
	 */
	public List<CaracteristiquesBean> getAllCaracteristiquesPatientByType(Integer patient_id, Integer type_caracteristique_id) {
		List<CaracteristiquesBean> caractList = null;

		try {
			DBCollection dbCollection = db.getCollection("caracteristiques");
			BasicDBObject query = new BasicDBObject("patient_id", patient_id).append("type_caracteristique_id", type_caracteristique_id);
			DBCursor cursor = dbCollection.find(query);
			if(cursor!=null && cursor.size()>0){
				caractList = new ArrayList<CaracteristiquesBean>();
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					CaracteristiquesBean car = new CaracteristiquesBean();
					car.setId((Integer) item.get("_id"));
					car.setValeur((Integer) item.get("valeur"));
					car.setValeur2((Integer) item.get("valeur2"));
					car.setDate((Date) item.get("date"));
					caractList.add(car);
				}
			}
		}
		catch (MongoException e) {
			//System.out.println("---PatientService - getAllCaracteristiquesPatient - MongoException : "+e.getMessage());
			//e.printStackTrace();
			return null;
		}
		catch (Exception e) {
			//System.out.println("---PatientService - getAllCaracteristiquesPatient - Exception : "+e.getMessage());
			//e.printStackTrace();
			return null;
		}

		return caractList;
	}
	
	/**
	 * findBeneficiaireByMatricule(String matricule)
	 * @param matricule
	 * 
	 * Recherche un beneficiaire (assur�) � partir de son matricule.
	 */
	public BeneficiaireBean findBeneficiaireByMatricule(String matricule){
		BeneficiaireBean beneficiaire = null;
		
		try {
			DBCollection dbCollection = db.getCollection("beneficiaire");
			BasicDBObject query = new BasicDBObject("matricule", matricule);
			DBObject cursor = dbCollection.findOne(query);
			if(cursor!=null){
				beneficiaire=em.find(BeneficiaireBean.class, (Integer) cursor.get("_id"));
			}
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return beneficiaire;
	}
	
	/**
	 * findBeneficiaireByMatricule(Integer typeClient,Integer client,String matricule)
	 * @param matricule
	 * @param typeClient
	 * @param client
	 *  
	 * Recherche un beneficiaire (assur�) � partir de son matricule.
	 */
	public BeneficiaireBean findBeneficiaireByMatricule(Integer client,String matricule){
		BeneficiaireBean beneficiaire = null;
		
		try {
			DBCollection dbCollection = db.getCollection("beneficiaire");
			BasicDBObject query = new BasicDBObject("client_taux_id", client).append("matricule", matricule);
			DBObject cursor = dbCollection.findOne(query);
			if(cursor!=null){
				beneficiaire=em.find(BeneficiaireBean.class, (Integer) cursor.get("_id"));
			}
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return beneficiaire;
	}
	
	/**
	 * listePatientsByBeneficiaire(BeneficiaireBean beneficiaire)
	 * @param beneficiaire
	 * 
	 * recherche tous les patients assur�s par le m�me matricule
	 */
	public List<PatientBean> listePatientsByBeneficiaire(BeneficiaireBean beneficiaire) {
		List<PatientBean> listpatients_matir=null;
		try
		{
			DBCollection dbCollection = db.getCollection("patient");
			BasicDBObject query = new BasicDBObject("beneficiaire_id", beneficiaire.getId().intValue());
			DBCursor cursor = dbCollection.find(query);
			listpatients_matir=new ArrayList<PatientBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					PatientBean patient=new PatientBean();
					patient=em.find(PatientBean.class, (Integer) item.get("_id"));						
					listpatients_matir.add(patient);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return listpatients_matir;
	}
}
