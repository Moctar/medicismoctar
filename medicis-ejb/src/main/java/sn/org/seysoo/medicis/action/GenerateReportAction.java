package sn.org.seysoo.medicis.action;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IGenerateReport;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.mongodb.DB;
import com.mongodb.Mongo;


 /**
 * @author OmarGueye
 * @version
 */

@Stateful
@Scope(ScopeType.SESSION)
@Name("GenerateReport")
public class GenerateReportAction implements IGenerateReport {
	
	
	public JasperPrint printReport(String fichierIn, Map params) {

		// Param�tres de connexion � la base de donn�es
       // Mongo connection = db.getMongo();
        
        JasperPrint jasperPrint = new JasperPrint();
        
        try {
        	// Connexion � la base de donn�es
        	//connection = getConnection();
        	
        	// - Chargement et compilation du rapport
            JasperDesign jasperDesign = JRXmlLoader.load(fichierIn);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            
            System.out.println("g�n�ration pdf chargement");
            
            // - Execution du rapport
            jasperPrint = JasperFillManager.fillReport(jasperReport, params);

            System.out.println("g�n�ration pdf ex�cution");
        } 
        catch (JRException e) {
			System.out.println("printReport JRException : "+e.getMessage());
        	e.printStackTrace();
        }
        finally{
				try {
		System.out.println("mongooo");
				} catch (Exception e) {
					e.printStackTrace();
				}
        }
        return jasperPrint;
	}
	
	public void exportReportToPdfFile(String fichierIn, String fichierOut, Map params) {

		JasperPrint jasperPrint = printReport(fichierIn, params);
		
        try {
        	// Cr�ation du rapport au format PDF
			JasperExportManager.exportReportToPdfFile(jasperPrint, fichierOut);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("exportReportToPdfFile JRException : "+e.getMessage());
			//e.printStackTrace();
		}

        System.out.println("g�n�ration pdf fin");
	}
	
	public void viewReportPdf(String fichierIn, Map params, String name) {

		JasperPrint jasperPrint = printReport(fichierIn, params);
		
        try {
			
        	byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        	
        	FacesContext context = FacesContext.getCurrentInstance();
        	
    		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			
    		response.addHeader("Content-disposition", "inline;filename="+name+".pdf"); // attachment
    		response.setContentLength(bytes.length);
    		response.getOutputStream().write(bytes);
    		response.setContentType("application/pdf");
    		context.responseComplete();
    		
		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("viewReportPdf JRException : "+e.getMessage());
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("viewReportPdf IOException : "+e.getMessage());
			//e.printStackTrace();
		}

        System.out.println("view g�n�ration pdf fin");
	}
	
	public void downloadReportPdf(String fichierIn, Map params, String name) {

		JasperPrint jasperPrint = printReport(fichierIn, params);
		
        try {
			
        	byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        	
        	FacesContext context = FacesContext.getCurrentInstance();
        	
    		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			
    		response.addHeader("Content-disposition", "attachment;filename="+name+".pdf"); // attachment
    		response.setContentLength(bytes.length);
    		response.getOutputStream().write(bytes);
    		response.setContentType("application/pdf");
    		context.responseComplete();
    		
		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("downloadReportPdf JRException : "+e.getMessage());
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("downloadReportPdf IOException : "+e.getMessage());
			//e.printStackTrace();
		}

        System.out.println("download g�n�ration pdf fin");
	}
	
	
	
	// Retourne la session de connection � la base de donn�es
	private Connection getConnection() {
		Connection conn = null;  
        
		try{
			System.out.println("---Connection---test");
			
	        //conn = dataSource.getConnection();
        
	        System.out.println("---Connection---OK");
        }
        catch (Exception e) {
        	System.out.println("---Connection---nonOK");
		}
        
        return conn;  
	}
	
	public List<byte[]> exportReportToPdfByte(String fichierIn, Map params) {

		List<byte[]> reports = new ArrayList<byte[]>();
		
		try {
			JasperPrint jasperPrint = printReport(fichierIn, params);

			// Cr�ation du rapport au format PDF
			reports.add(JasperExportManager.exportReportToPdf(jasperPrint));

		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("exportReportToPdfByte JRException : "+e.getMessage());
			//e.printStackTrace();
		}

        System.out.println("g�n�ration pdf fin");
        
        return reports;
	}
	
	public void combineToDownloadReportsPdf(List<byte[]> reports, String fileName) throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
        int totallength = 0;

        List pdfs = new ArrayList();
        
        response.addHeader("Content-disposition", "attachment;filename="+fileName+".pdf"); // attachment
		response.setContentType("application/pdf");

        for (int i=0; i<reports.size(); i++){
            pdfs.add(new ByteArrayInputStream(reports.get(i)));
        }
        
        BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
        Document document = new Document();
        
		try {
			List readers = new ArrayList();
            int totalPages = 0;
            Iterator iteratorPDFs = pdfs.iterator();
			
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = (InputStream) iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf); 
                readers.add(pdfReader); 
                totalPages += pdfReader.getNumberOfPages();
            }
            
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, out);
            document.open();
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF data PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator iteratorPDFReader = readers.iterator(); // Loop through the PDF files and add to the output.
            
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = (PdfReader) iteratorPDFReader.next(); // Create a new page in the target for each source page.
                 
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage(); 
					pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    PdfImportedPage page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0); totallength += pdfReader.getFileLength();
                }
                pageOfCurrentReaderPDF = 0;
            }
            document.close();
            response.setContentLength(totallength);
            out.flush();
            out.close();
            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("combineToDownloadReportsPdf IOException : "+e.getMessage());
			//e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("combineToDownloadReportsPdf IOException : "+e.getMessage());
			//e.printStackTrace();
		} 
	}
	
	public void combineToGerateFileReportsPdf(List<byte[]> reports, String fichierOut) throws Exception {
		
        int totallength = 0;

        List pdfs = new ArrayList();
        
        for (int i=0; i<reports.size(); i++){
            pdfs.add(new ByteArrayInputStream(reports.get(i)));
        }
        
        //BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
        Document document = new Document();
        
		try {
			List readers = new ArrayList();
            int totalPages = 0;
            Iterator iteratorPDFs = pdfs.iterator();
			
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = (InputStream) iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf); 
                readers.add(pdfReader); 
                totalPages += pdfReader.getNumberOfPages();
            }
            
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fichierOut));
            document.open();
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF data PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator iteratorPDFReader = readers.iterator(); // Loop through the PDF files and add to the output.
            
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = (PdfReader) iteratorPDFReader.next(); // Create a new page in the target for each source page.
                 
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage(); 
					pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    PdfImportedPage page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0); totallength += pdfReader.getFileLength();
                }
                pageOfCurrentReaderPDF = 0;
            }
            document.close();
            
            //out.flush();
            //out.close();
            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} 
	}

	/**
	 * printReportWithJRDS
	 * 
	 * @param fichierIn
	 * @param fichierOut
	 * @param params
	 * @param collection
	 */
	public void printReportWithJRDS(String fichierIn, String fichierOut, Map params, List<Object> collection) {

        JasperPrint jasperPrint = new JasperPrint();
        
        try {
        	// - Chargement et compilation du rapport
            JasperDesign jasperDesign = JRXmlLoader.load(fichierIn);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            
            System.out.println("printReportWithJRDS pdf chargement");
            
            // - Execution du rapport
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(collection));

            // Cr�ation du rapport au format PDF
         	JasperExportManager.exportReportToPdfFile(jasperPrint, fichierOut);
         			
            System.out.println("printReportWithJRDS pdf ex�cution");
        } 
        catch (JRException e) {
			System.out.println("printReportWithJRDS JRException : "+e.getMessage());
        	//e.printStackTrace();
        }
	}
	
	@Destroy
	@Remove
	public void destroy() {
	}
}
