/*
* 
* $Revision: 57 $:
* $Author: thiongane $:
* $Date: 2012-09-25 13:42:08 +0100 (mar., 25 sept. 2012) $:
* $URL: https://aninfga.atlassian.net/svn/ETAX/trunk/workspace/etax/etax-ejb/src/main/java/ga/org/dgi/etax/Authenticator.java $:
* $Id: Authenticator.java 57 2012-09-25 12:42:08Z thiongane $:
* ================================================================
* Copyright (c) 2012 by Direction Generale des Imp�ts,
* Ministere de l'Economie, de l'Emploi,et du Developpement durable
* Republique Gabonaise
* 
* This software is the proprietary information of DGI
* All rights reserved.
*/ 

package sn.org.seysoo.medicis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface Authenticator
{
   boolean authenticate();
   public void destroy();
   public String getDate1();
   public void setDate1(String date1);
   public String getDate2();
   public void setDate2(String date2);
   //
}
