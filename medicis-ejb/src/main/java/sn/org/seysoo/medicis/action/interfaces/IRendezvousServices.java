package sn.org.seysoo.medicis.action.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.inject.Provider;

import org.hibernate.search.jpa.FullTextEntityManager;

import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;


@Local
public interface IRendezvousServices {

	public Provider<FullTextEntityManager> getLazyFEM();

	public void setLazyFEM(Provider<FullTextEntityManager> lazyFEM);

	public List<RendezVousBean> listRendezvous ();

	public void saveRendezVous_test() ;

	public void saveMotif(MotifBean motifBean) ;

	public void saveService(ServiceBean serviceBean) ;

	public void saveGroupeSanguin(GroupeSanguinBean groupeSanguinBean) ;

	public void savePatients(PatientBean patientBean) ;

	public void saveRendezvous(RendezVousBean rv) ;

	public void remove();

	public void destroy() ;
	public RendezVousBean findRendezvous (int id);

	public void save_Client_test();

	public List<ClientBean> getAllClients ();

	public List<ClientTauxBean> getTauxAssur ();
	
	public List<RendezVousBean> searchAllRendezvous(String etat);

	public List<RendezVousBean> randezVousPeriode(int idPatient, Date dateDebut, Date dateFin);
	
	public List<ServiceBean> getAllServices () ;
	
	public List<MotifBean> getAllMotif ();
	
	public List<MotifBean> listMotifbyService(Integer idService );
	public RendezVousBean EnregistrerRendezvous(RendezVousBean rv);
	public List<RendezVousBean> searchRendezvousByPeriod(String etat, Date dateDeb,Date dateFin);
	
	public List<TypeClientBean> getTypesClient();
	public List<ClientBean> getClientsByType(Integer typeClient);
	public TypeClientBean findTypeClientById(Integer id);
	public SalleBean findSalleById(Integer id);
	public MedecinBean findMedecinById(Integer id);
	public ServiceBean findServiceById(Integer id);
}
