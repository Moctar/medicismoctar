package sn.org.seysoo.medicis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import sn.org.seysoo.medicis.action.interfaces.IBanqueService;
import sn.org.seysoo.medicis.entity.BanqueBean;
import sn.org.seysoo.medicis.entity.ClientBean;

import org.jboss.seam.annotations.Destroy;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import javax.ejb.Remove;
import javax.persistence.PersistenceException;




@Stateful
@Scope(ScopeType.SESSION)
@Name("banqueService")

public class BanqueService implements IBanqueService{


	
@In(create=true,required=false)
private DB db;
	

	
	public List <BanqueBean> getAllBanques (){
		
		List<BanqueBean> list = null;
		
		try {
			System.out.println("-->try liste banque-- getAllBanques --");
			
			DBCollection dbCollection = db.getCollection("banque");
			System.out.println("Find all documents in collection banque:");
				DBCursor cursor = dbCollection.find();
				list=new ArrayList<BanqueBean>();
				if(cursor.size()>0){
					while (cursor.hasNext()) {
						DBObject item = cursor.next();
						BanqueBean bq=new BanqueBean();
						bq.setId((Integer) item.get("_id"));
						bq.setNomBanque((String) item.get("nomBanque"));
						list.add(bq);
					System.out.println(item);
					}
				}
			 

			
			System.out.println("SIZE banque *****   :  "  + list.size()); 
			
		} catch (PersistenceException e) {
			// TODO: handle exception
			System.out.println("---Erreur dans la collection banque--- "); 
			e.printStackTrace();
		}
		
		return list;
	}

	
	

    @Destroy
    public void destroy() {
    }

    @Remove
    public void remove() {
    }
}
