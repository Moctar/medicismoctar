/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 16:35:04
 * @URL		 :FTEMProvider.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du S�negal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */
package sn.org.seysoo.medicis.services;

import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;

/**
 * @author hp
 *
 */
@Stateful
@Scope(ScopeType.SESSION)
public class FTEMProvider {
	@PersistenceContext
    EntityManager em;

    @Produces
    public FullTextEntityManager getFTEM() {
            return Search.getFullTextEntityManager(em);
    }

}
