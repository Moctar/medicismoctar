package sn.org.seysoo.medicis.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.DatabaseRetrievalMethod;
import org.hibernate.search.query.ObjectLookupMethod;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.sql.ast.origin.hql.parse.HQLParser.new_key_return;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.Renderer;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import sn.org.seysoo.medicis.action.interfaces.IFactureService;
import sn.org.seysoo.medicis.entity.BanqueBean;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.FactureBean;
import sn.org.seysoo.medicis.entity.FactureClientBean;
import sn.org.seysoo.medicis.entity.FacturePatientBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.RoleBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.UtilisateurBean;

@Stateful
@Name("factureService")
@Scope(ScopeType.SESSION)
public class FactureService implements IFactureService {
	
	@PersistenceContext
	private EntityManager em;

	@In(create=true,required=false)
	private DB db;

	@In
	private Renderer renderer;

/*
 * (non-Javadoc)
 * @see sn.org.seysoo.medicis.action.interfaces.IFactureService#listFacturesClient()
 * 
 * public List<RendezVousBean> searchAllRendezvous(String etat){
	List<RendezVousBean> listrv=null;
	try
	{
		DBCollection dbCollection = db.getCollection("rendez_vous");
				
		
		BasicDBObject query = new BasicDBObject("etat", etat);
		System.out.println("Find all documents in collection: searchRvAnnule");
			DBCursor cursor = dbCollection.find(query);
			listrv=new ArrayList<RendezVousBean>();
			if(cursor.size()>0){
				while (cursor.hasNext()) {
					DBObject item = cursor.next();
					RendezVousBean rv=new RendezVousBean();
					PatientBean patient = new PatientBean();
					MotifBean motif=new MotifBean();
					
					
					rv.setPatient(patient);
					rv.setMotif(motif);
					
					rv.setId((Integer) item.get("_id"));
					rv.setEtat((String) item.get("etat"));
					rv.setDate((Date) item.get("date"));
					rv.setHeure((String) item.get("heure"));
					rv.setPatient(em.find(PatientBean.class, (Integer) item.get("patient_id")));
					rv.setMotif(em.find(MotifBean.class, (Integer) item.get("motif_id")));
									
					listrv.add(rv);
					
				System.out.println(item);
				}
			}
	}
	catch (Exception e) {
		System.out.println("Exception: "+e.getMessage());
		e.printStackTrace();
		return null;
	}
	return listrv;
	
}
 */
	public ArrayList<FactureClientBean> listFacturesClient() {
		try {	

			FullTextEntityManager fem = Search.getFullTextEntityManager(this.em);

			final QueryBuilder builder = fem.getSearchFactory().buildQueryBuilder().forEntity(FactureClientBean.class).get();

			final Query luceneQuery = builder.all().createQuery();
			
			System.out.println(luceneQuery.toString());

			final FullTextQuery query = fem.createFullTextQuery(luceneQuery,FactureClientBean.class);
			
			query.initializeObjectsWith(ObjectLookupMethod.SKIP, DatabaseRetrievalMethod.FIND_BY_ID);

			return (ArrayList<FactureClientBean>) query.getResultList();
			} catch (Exception e) {
				return null;
			}
	}
	
	@Produces
	public List <FacturePatientBean> listFacturesPatient() {
		
		List <FacturePatientBean>  list = null;
		
		try {	

			DBCollection dbCollection = db.getCollection("facture_patient");
			
			
			System.out.println("Find all documents in collection: listFacturesPatient");
				DBCursor cursor = dbCollection.find();
				list=new ArrayList<FacturePatientBean>();
				
					

				if(cursor.size()>0){
					while (cursor.hasNext()) {
						
						DBObject item = cursor.next();
						FacturePatientBean facureP=new FacturePatientBean();
						
						facureP.setId((Integer) item.get("_id")); 
						facureP.setRendezVous(em.find(RendezVousBean.class, (Integer) item.get("rendez_vous_id")));
						facureP.setFacture(em.find(FactureBean.class, (Integer) item.get("facture_id")));
										
						list.add(facureP);
						
					System.out.println(item);
					}
				}
		
		} catch (Exception e) {
			return null;
		}
		
		return list;
	}
	public FactureBean EnregistrerFacture(FactureBean facture){
		FactureBean fact=null;
		try{
			fact= em.merge(facture);
			em.flush();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return fact;
	}
	public ClientBean findClientById(int idClient){
		try {
			return em.find(ClientBean.class, idClient);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public FacturePatientBean EnregistrerFacturePatient(FacturePatientBean facturepatient){
		FacturePatientBean fact=null;
		try{
			fact = em.merge(facturepatient);
			em.flush();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return fact;
	}
	public FactureClientBean EnregistrerFactureClient(FactureClientBean factureClient){
		FactureClientBean fact=null;
		try{
			fact = em.merge(factureClient);
			em.flush();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return fact;
	}
	public List<DiagnosticBean> getDiagnosticClientParPeriode(int idclient,Date dateDeb,Date dateFin){
		List<DiagnosticBean> listediagnostic=new ArrayList<DiagnosticBean>();
		try{
			ClientBean client = em.find(ClientBean.class,idclient);
			System.out.println("Id client ="+client.getId()+" et désignation "+client.getDesignation());
			Set<ClientTauxBean> clientaux = client.getClientTauxes();
			System.out.println("Size client taux ="+clientaux.size());
			for (ClientTauxBean clientTauxBean : clientaux) {
				Set<BeneficiaireBean> beneficiaire = clientTauxBean.getBeneficiaires();
				System.out.println("Béneficiare ="+beneficiaire.size());
				for (BeneficiaireBean beneficiaireBean : beneficiaire) {
					Set<PatientBean> patient = beneficiaireBean.getPatients();
					for (PatientBean patientBean : patient) {
						System.out.println("Patient id ="+patientBean.getId());
						Set<RendezVousBean> rendezvous = patientBean.getRendezVous();
						for (RendezVousBean rendezVousBean : rendezvous) {
							Set<DiagnosticBean> dignostic = rendezVousBean.getDiagnostics();
							System.out.println("Diagnostique Size = "+dignostic.size());
							for (DiagnosticBean diagnosticBean : dignostic) {
								System.out.println("Date Deb = "+dateDeb);
								System.out.println("Date Diagnostic = "+diagnosticBean.getDate());
								if(diagnosticBean.getDate().after(dateDeb) && diagnosticBean.getDate().before(dateFin)){
									System.out.println("Un exam retourvé et ajouté");
									listediagnostic.add(diagnosticBean);
								}
							}
						}
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return listediagnostic;
		
	}
	
	public void save_FacturePatient () {
		
//		FacturePatientBean factureP = new FacturePatientBean();
//		FactureBean   facture = new FactureBean();
//		RendezVousBean rendezVous = em.find(RendezVousBean.class, 6);
//		
//		
//		try {
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			Date dateFacture = sdf.parse("11/11/2010");
//
//
//			  
//			facture.setDate(dateFacture);
//			facture.setEtatFacture(0);
//			facture.setEtatReglement(0);
//			facture.setMontant(Double.parseDouble("471700") );
//			facture.setRemise(0);
//			facture=em.merge(facture);
//			facture.setNumero("0000"+facture.getId());
//			facture=em.merge(facture);
//						
//			
//			factureP.setFacture(facture);
//			factureP.setRendezVous(rendezVous);
//			
//			factureP=em.merge(factureP);
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		/*FacturePatientBean factureP = new FacturePatientBean();
		FactureBean   facture = new FactureBean();
		RendezVousBean rendezVous = em.find(RendezVousBean.class, 7);
		
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date dateFacture = sdf.parse("11/11/2010");


			  
			facture.setDate(dateFacture);
			facture.setEtatFacture(0);
			facture.setEtatReglement(0);
			facture.setMontant(565250.0) ;
			facture.setRemise(0);
			facture=em.merge(facture);
			facture.setNumero("0000"+facture.getId());
			facture=em.merge(facture);
						
			
			factureP.setFacture(facture);
			factureP.setRendezVous(rendezVous);
			
			factureP=em.merge(factureP);
			
		} catch (Exception e){
			// TODO: handle exception
		}*/
	}
	
	public void save_FactureClient () {
		
//		FactureClientBean factureC = new FactureClientBean();
//		FactureBean   facture = em.find(FactureBean.class, 7);
//		ClientBean client = em.find(ClientBean.class, 1);
//		
//		
//		try {
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			Date dateFacture = sdf.parse("22/12/2011");
//			Date dateFacture2 = sdf.parse("07/10/2012");
//
//
//			factureC.setDateDebut(dateFacture);
//			factureC.setDateFin(dateFacture2);
//			factureC.setClient(client);
//			factureC.setFacture(facture);
//			
//			factureC=em.merge(factureC);
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
	}
	
	public void save_Diagnostic () {
		
//		SalleBean salleBean = new SalleBean();
//		MedecinBean medecinBean = new MedecinBean();
//		
//		DiagnosticBean diag = new DiagnosticBean();
//		RendezVousBean rendezVous1 = em.find(RendezVousBean.class, 6);
//		ServiceBean service = em.find(ServiceBean.class, 7);
//		ExamenBean examenBean = new ExamenBean();
//		GroupeExamenBean groupeExamenBean = new GroupeExamenBean();
//		
//		
//		UtilisateurBean user = new UtilisateurBean();
//		RoleBean role = new RoleBean();
//		
//		//RendezVousBean rendezVous2 = em.find(RendezVousBean.class, 6);
//		
//		
//		try {
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			Date dateDiag = sdf.parse("17/07/2012");
//
//			groupeExamenBean.setLibelle("groupe ex2");
//			groupeExamenBean.setService(service);
//			
//			groupeExamenBean=em.merge(groupeExamenBean);
//			
//			examenBean.setLibelle("scanner du genou") ;
//			examenBean.setIndication("scanner ..");
//			examenBean.setPrix(Double.parseDouble("48520")); 
//			examenBean.setGroupeExamen(groupeExamenBean);
//			
//			
//			examenBean=em.merge(examenBean);
//			
//			role.setHerite(1);
//			role.setLibelle("agent");
//			
//			role=em.merge(role);
//			
//			user.setDateCreation(dateDiag);
//			user.setEmail("bibndiaye@gmail.com");
//			user.setPrenom("Moussa");
//			user.setNom("Diouf");
//			user.setIdentifiant("mouhamadou");
//			user.setMotDePasse("passer");
//			user.setRole(role);
//			user.setTelephone1("4554545");
//			
//			user=em.merge(user);
//			
//			salleBean.setNom("salle 07");
//			salleBean.setDescription("salle de scanner");
//			salleBean.setService(service);
//			
//			salleBean=em.merge(salleBean);
//			
//			medecinBean.setService(service);
//			medecinBean.setUtilisateur(user);
//			medecinBean=em.merge(medecinBean);
//			
//            diag.setRendezVous(rendezVous1);
//            diag.setExamen(examenBean);
//            diag.setSalle(salleBean);
//            diag.setMedecin(medecinBean);
//            diag.setDate(dateDiag);
//            
//            diag=em.merge(diag);
//            
//			  
			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
	}
	
	public List <DiagnosticBean> getDiagnostics (RendezVousBean rendezVous){
		
		List <DiagnosticBean> list =null;
		   
		try {


			DBCollection dbCollection = db.getCollection("diagnostic");
					
			
			BasicDBObject query = new BasicDBObject("rendez_vous_id", rendezVous.getId());
			System.out.println("Find all documents in collection: diagnostic");
				DBCursor cursor = dbCollection.find(query);
				list=new ArrayList<DiagnosticBean>();
				if(cursor.size()>0){
					while (cursor.hasNext()) {
						
						DBObject item = cursor.next();
						DiagnosticBean diag = new DiagnosticBean();
						
						diag.setId((Integer) item.get("_id"));
						diag.setDate((Date) item.get("date"));
						diag.setRendezVous(em.find(RendezVousBean.class, (Integer) item.get("rendez_vous_id")));
						diag.setMedecin(em.find(MedecinBean.class, (Integer) item.get("medecin_id")));
						diag.setSalle(em.find(SalleBean.class, (Integer) item.get("salle_id")));
						diag.setExamen(em.find(ExamenBean.class, (Integer) item.get("examen_id")));
						
												
						list.add(diag);
						
					System.out.println(item);
					}
				}
		
		} catch (Exception e) {
			return null;
		}

		
		return list;
		
	}
//	public List <PatientBean> getPatientAssur (BeneficiaireBean ben){
//		
//		List <DiagnosticBean> list =null;
//		   
//		try {
//
//
//			DBCollection dbCollection = db.getCollection("diagnostic");
//					
//			
//			BasicDBObject query = new BasicDBObject("patient_id",ben.getClientTaux().getTaux());
//			System.out.println("Find all documents in collection: diagnostic");
//				DBCursor cursor = dbCollection.find(query);
//				list=new ArrayList<DiagnosticBean>();
//				if(cursor.size()>0){
//					while (cursor.hasNext()) {
//						
//						DBObject item = cursor.next();
//						DiagnosticBean diag = new DiagnosticBean();
//						
//						diag.setId((Integer) item.get("_id"));
//						diag.setDate((Date) item.get("date"));
//						diag.setRendezVous(em.find(RendezVousBean.class, (Integer) item.get("rendez_vous_id")));
//						
//						diag.setExamen(em.find(ExamenBean.class, (Integer) item.get("examen_id")));
//					
//						
//						list.add(diag);
//						
//					System.out.println(item);
//					}
//				}
//		
//		} catch (Exception e) {
//			return null;
//		}
//
//		
//		return list;
//		
//	}
	public List <FactureClientBean> getFactureClient (){
		
		List <FactureClientBean> list =null;
		   
		try {


			DBCollection dbCollection = db.getCollection("facture_client");
			System.out.println("Find all documents in collection facture_client:");
			DBCursor cursor = dbCollection.find();
			
				list=new ArrayList<FactureClientBean>();
				if(cursor.size()>0){
					while (cursor.hasNext()) {
						
						DBObject item = cursor.next();
						FactureClientBean fc=new FactureClientBean();
						fc=em.find(FactureClientBean.class, (Integer) item.get("_id"));
//						fc.setId((Integer) item.get("_id"));
//						fc.setClient(em.find(ClientBean.class, (Integer) item.get("client_id")));
//						fc.setFacture(em.find(FactureBean.class, (Integer) item.get("facture_id")));
//						fc.setDateDebut((Date) item.get("dateDebut")) ;
//						fc.setDateFin((Date) item.get("dateFin")) ;
						
						
						
						list.add(fc);
					    System.out.println("client facture -> "+item);
						
						
					}
				}
			 
		} catch (Exception e) {
			return null;
		}

		
		return list;
		
	}
	
	public String testEnvoiMail (){
		
		//renderer.render("/patient/envoiMail.xhtml");
		
		//return "/rendezvous/listeRendezVous.xhtml";
		return "";
	}
	
	public void insererBanq(){
		
		BanqueBean banqueBean = new BanqueBean();
		banqueBean.setNomBanque("SGBS");
		banqueBean=em.merge(banqueBean);
		
		banqueBean.setNomBanque("CBAO");
		banqueBean=em.merge(banqueBean);
		
		banqueBean.setNomBanque("BHS");
		banqueBean=em.merge(banqueBean);
	}
		@Remove
    public void remove() {
    }

    @Destroy
    public void destroy() {
    }
}
