package sn.org.seysoo.medicis.action.interfaces;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.jboss.seam.jsf.SetDataModel;
import org.primefaces.model.DualListModel;

import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.util.ConstUtil;

@Local
public interface IRendezvous {
	public void save_Rendezvous();
	public RendezVousBean getSelectedRendezvous() ;
	
	public void setSelectedRendezvous(RendezVousBean selectedRendezvous);
	
	public String ouvrirFicheConsultationOdontologie();
	
	public String ouvrirFicheConsultationRadiologie();
	
	public String ouvrirFicheConsultationGynecologie();
	
	public void onSelectRV();
	public List<RendezVousBean> getlRendezvous();

	public void setlRendezvous(List<RendezVousBean> lRendezvous);
	
	public void getAllRendezvous ();
	
	
	public RendezVousBean getRvSelected();
	
	public void setRvSelected(RendezVousBean rvSelected) ;
	
	public String getSexe() ;
	
	public void setSexe(String sexe) ;
	
	public String getDatenais() ;
	
	public void setDatenais(String datenais); 
	
	public String getPrenom() ;

	public void setPrenom(String prenom) ;
	
	public String getNom() ;
	
	public void setNom(String nom) ;

	public String getEmail() ;

	public void setEmail(String email) ;

	public String getDateRV() ;

	public void setDateRV(String dateRV) ;
	
	public String getHeureDeb() ;
	
	public void setHeureDeb(String heureDeb) ;
	
	public String getHeureFin() ;
	
	public void setHeureFin(String heureFin) ;
	
	public String getTel();
	
	public void setTel(String tel);

	public String getMotif() ;

	public void setMotif(String motif) ;

	public String getService() ;
	public void updateFicheRecap();
	public void setService(String service) ;
	
	public RendezVousBean getRendezvs() ;
	
	public void setRendezvs(RendezVousBean rendezvs) ;
	
	public PatientBean getPatien() ;

	public void setPatien(PatientBean patien) ;
	
	public ServiceBean getServi() ;
	
	public void setServi(ServiceBean servi) ;
	
	public MotifBean getMoti();
	
	public void setMoti(MotifBean moti) ;
	
	public GroupeSanguinBean getGrpSanguin() ;
	
	public void setGrpSanguin(GroupeSanguinBean grpSanguin);
	
	public void saveAndmodifyRV ();
	
	
    public void destroy() ;

    public void remove();
    

	public List <RendezVousBean> getListRendezvous() ;
	
	public void setListRendezvous(List <RendezVousBean> listRendezvous);
	
	public String getGs();
	
	public void setGs(String gs) ;
	
	public String getPrfil() ;
	
	public void setPrfil(String prfil) ;
	

	public String getNomPatient() ;
	

	public void setNomPatient(String nomPatient) ;

	public String getSexePatient() ;

	public void setSexePatient(String sexePatient) ;

	public String getTelPatient();
	
	public void setTelPatient(String telPatient);

	public String getEmailPatient();

	public void setEmailPatient(String emailPatient);

	public String getDatenaisPatient() ;

	public void setDatenaisPatient(String datenaisPatient) ;

	public String getDateRVPatient() ;

	public void setDateRVPatient(String dateRVPatient) ;

	public String getHeureDebPatient() ;

	public void setHeureDebPatient(String heureDebPatient);

	public String getMotifPatient() ;
	
	public void setMotifPatient(String motifPatient);
	
	public String getServicePatient();

	public void setServicePatient(String servicePatient);
	
	public String getDateNaiss() ;
	
	public void setDateNaiss(String dateNaiss) ;
	
	public String getProfession() ;
	
	public void setProfession(String profession);
	
	public String getTail();
	
	public void setTail(String tail) ;
	
	public String getCni() ;
	
	public void setCni(String cni) ;
	
	public String getTelp() ;
	
	public void setTelp(String telp);
	
	public String getPoids2();
	
	public void setPoids2(String poids2) ;
	
	public String getAddress() ;
	
	public void setAddress(String address) ;
	
	public String getEmail2() ;
	
	public void setEmail2(String email2);
	
	public String getProfil() ;

	public void setProfil(String profil) ;
	
	public String getSignespart() ;
	
	public void setSignespart(String signespart);
	
	public String getPrenomPatient() ;
	
	public void setPrenomPatient(String prenomPatient);
	
	public String getSexeAff();

	public void setSexeAff(String sexeAff);

	public String getPrenomAff();
	public void setPrenomAff(String prenomAff) ;
	public String getNomAff();
	public void setNomAff(String nomAff);
	public String getMatrAff() ;
	public void setMatrAff(String matrAff) ;
	public String getPoliceAff();
	public void setPoliceAff(String policeAff);
	public String getClienAff();
	public void setClienAff(String clienAff) ;
	public String getTauxAff();
	public void setTauxAff(String tauxAff) ;
	public String getTelAff() ;
	public void setTelAff(String telAff) ;
	public String getEmailAff();
	public void setEmailAff(String emailAff);
	
	public Map<String,String> getClientAssureurs();
	
	public void setClientAssureurs(Map<String,String> clientAssureurs) ;
	
	public List<ClientBean> getListClient() ;

	public void setListClient(List<ClientBean> listClient);
	
	public Map<String,String> getTauxAssurances() ;

	public void setTauxAssurances(Map<String,String> tauxAssurances) ;
	
	public List<ClientTauxBean> getListTaux();

	public void setListTaux(List<ClientTauxBean> listTaux);

	public Date getDateNaissanceP() ;

	public void setDateNaissanceP(Date dateNaissanceP) ;

	public Date getDateRendezvousP() ;
	
	public void setDateRendezvousP(Date dateRendezvousP);
	
	public void confirmerAnnulation (ActionEvent actionEvent);
	
	public void save_Rendezvous2 ();
	
	public Map<String,String> getMotifs() ;
	
	public void setMotifs(Map<String,String> motifs);

	public Map<String,String> getServices() ;

	public void setServices(Map<String,String> services);
	
	public List<ServiceBean> getListService();

	public void setListService(List<ServiceBean> listService);

	
	public List<MotifBean> getListMotif() ;
	
	public void setListMotif(List<MotifBean> listMotif);
	
    public void chargerMotif (ValueChangeEvent event);
    public String getOut();
    public void setOut(String out);
    
    public Map<String, String> getListMotifParService() ;

	public void setListMotifParService(Map<String, String> listMotifParService) ;
	
	public List<MotifBean> getListMotifsService();

	public void setListMotifsService(List<MotifBean> listMotifsService) ;
	public DualListModel<ExamenBean> getListeExamensTypeRv();
	public void setListeExamensTypeRv(DualListModel<ExamenBean> listeExamensTypeRv);
	public void enregistrerRendeVous();
	public void chargergroupeExam (ValueChangeEvent event);
	public List<GroupeExamenBean> getGroupeExam() ;
	public void setGroupeExam(List<GroupeExamenBean> groupeExam);
	public List<GroupeExamenBean> getGroupeExamSelectioner();
	public void setGroupeExamSelectioner(List<GroupeExamenBean> groupeExamSelectioner);
	public int getIdgroupeexam();
	public void setIdgroupeexam(int idgroupeexam);
	public void ongroupeSelectionner();
	public void onchoixExamen();
	public String getChoixExamen();
	public void setChoixExamen(String choixExamen);
	public SetDataModel getDiagnosticsDataModel();
	public void setDiagnosticsDataModel(SetDataModel diagnosticsDataModel);
	public void AfficherPlusInfos();
	public Boolean getPlusInfos();
	public void setPlusInfos(Boolean plusInfos);
	public void AfficherMoinsInfos();
	public void onSelectPatient();
	public String nouveauRendezVous();
	public List<RendezVousBean> getlRendezvousConf();
	public String getNewpat();
	public void setNewpat(String newpat);
	public void setlRendezvousConf(List<RendezVousBean> lRendezvousConf);
	public PatientBean getPatientSelectioner();
	public void setPatientSelectioner(PatientBean patientSelectioner);
	
	public RendezVousBean getSelectedRvConfirme();

	public void setSelectedRvConfirme(RendezVousBean selectedRvConfirme);
	
	public void confirmeRendezvous();
	
	public void getRendezvousConfirmes ();
	public void newPatientSelected();

	public Date getDateDebRv() ;

	public void setDateDebRv(Date dateDebRv) ;

	public Date getDateFinRv();

	public void setDateFinRv(Date dateFinRv) ;
	
	public void getRendezvousByPeriod ();
	
	public Date getDateDebRvc() ;

	public void setDateDebRvc(Date dateDebRvc) ;
	
	public Date getDateFinRvc();
	
	public void setDateFinRvc(Date dateFinRvc);
	
	public List<RendezVousBean> getlRendezvousAnnule() ;

	public void setlRendezvousAnnule(List<RendezVousBean> lRendezvousAnnule);
	
	public Date getDateDebRva() ;

	public void setDateDebRva(Date dateDebRva);

	public Date getDateFinRva();
	
	public void setDateFinRva(Date dateFinRva);
	
	public void getRendezvousAnnules();
	
	public RendezVousBean getSelectedRvannule() ;
	
	public void setSelectedRvannule(RendezVousBean selectedRvannule) ;
	
	public Date getDateRepro();

	public void setDateRepro(Date dateRepro) ;
	
	public void confirmeReprogrammationRV();
	
	public void onSelectRVannule();
	
	public List<DiagnosticBean> getlDiagnostiqueByRv();

	public void setlDiagnostiqueByRv(List<DiagnosticBean> lDiagnostiqueByRv);
	
	public void listDiagnosticByRv ();

	public void onSelectRVConf();
	
	public List<DiagnosticBean> getlDiagnostiqueByRvAn() ;

	public void setlDiagnostiqueByRvAn(List<DiagnosticBean> lDiagnostiqueByRvAn) ;
	

	public List<DiagnosticBean> getlDiagnostiqueByRvCo();
	
	public void setlDiagnostiqueByRvCo(List<DiagnosticBean> lDiagnostiqueByRvCo) ;
	
	public Map<String,String> getTypesClient();
	
	public void setTypesClient(Map<String,String> typesClient);
	
	public void findClientsByType();
}

