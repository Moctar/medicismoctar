package sn.org.seysoo.medicis.entity;

// Generated 9 oct. 2013 11:24:03 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.search.annotations.Indexed;
import org.jboss.seam.annotations.Name;
import org.jboss.solder.core.Veto;

/**
 * Facture generated by hbm2java
 */
@Entity
@Veto
@Indexed
@Table(name = "facture")
@Name("factureBean")
public class FactureBean implements java.io.Serializable {

	private Integer id;
	private String numero;
	private Double montant;
	private Date date;
	private int etatReglement;
	private double remise;
	private int etatFacture;
	private Set<ReglementBean> reglements = new HashSet<ReglementBean>(0);

	private Set<FacturePatientBean> facturePatients = new HashSet<FacturePatientBean>(0);
	private Set<FactureClientBean> factureClients = new HashSet<FactureClientBean>(0);

	public FactureBean() {
	}

	public FactureBean(String numero, Double montant, Date date, int etatReglement,
			int remise, int etatFacture) {
		this.numero = numero;
		this.montant = montant;
		this.date = date;
		this.etatReglement = etatReglement;
		this.remise = remise;
		this.etatFacture = etatFacture;
	}

	public FactureBean(String numero, Double montant, Date date, int etatReglement,
			int remise, int etatFacture, Set<ReglementBean> reglements) {
		this.numero = numero;
		this.montant = montant;
		this.date = date;
		this.etatReglement = etatReglement;
		this.remise = remise;
		this.etatFacture = etatFacture;
		this.reglements = reglements;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "numero", nullable = false, length = 50)
	@NotNull
	@Size(max = 50)
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(name = "montant", nullable = false)
	public Double getMontant() {
		return this.montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = false, length = 10)
	@NotNull
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "etatReglement", nullable = false)
	public int getEtatReglement() {
		return this.etatReglement;
	}

	public void setEtatReglement(int etatReglement) {
		this.etatReglement = etatReglement;
	}

	@Column(name = "remise", nullable = false)
	public double getRemise() {
		return this.remise;
	}

	public void setRemise(double remise) {
		this.remise = remise;
	}

	@Column(name = "etatFacture", nullable = false)
	public int getEtatFacture() {
		return this.etatFacture;
	}

	public void setEtatFacture(int etatFacture) {
		this.etatFacture = etatFacture;
	}

	@OneToMany( mappedBy = "facture", fetch=FetchType.EAGER)
	public Set<ReglementBean> getReglements() {
		return this.reglements;
	}

	public void setReglements(Set<ReglementBean> reglements) {
		this.reglements = reglements;
	}

	@OneToMany( mappedBy = "facture", fetch=FetchType.EAGER)
	public Set<FacturePatientBean> getFacturePatients() {
		return this.facturePatients;
	}

	public void setFacturePatients(Set<FacturePatientBean> facturePatients) {
		this.facturePatients = facturePatients;
	}

	@OneToMany( mappedBy = "facture", fetch=FetchType.EAGER)
	public Set<FactureClientBean> getFactureClients() {
		return this.factureClients;
	}

	public void setFactureClients(Set<FactureClientBean> factureClients) {
		this.factureClients = factureClients;
	}

}
