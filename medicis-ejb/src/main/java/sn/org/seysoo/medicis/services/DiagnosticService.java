/**
 * @Revision :01
 * @author   :Moctar Ibrahima Ba
 * @date     :11 oct. 2013 11:38:16
 * @URL		 :DiagnosticService.java
 * @Id       :
 * =======================================================
 * Copyright (c) 2013 by Seysoo,
 * Republique du Senegal
 * this softaware is the proprietary information of SEYSOO
 * All rights reseved
 */

package sn.org.seysoo.medicis.services;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.ImageBean;
import sn.org.seysoo.medicis.entity.InfosIrmBean;
import sn.org.seysoo.medicis.entity.InterpretationBean;
import sn.org.seysoo.medicis.entity.InterpretationTypeBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.TechniqueUtiliseBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * @author hp
 *
 */
@Stateful
@Name("diagnosticService")
@Scope(ScopeType.SESSION)
public class DiagnosticService implements IServiceDiagnostic {

	@PersistenceContext
	private EntityManager em;

	@In(create=true,required=false)
	private DB db;

	@Logger
	Log log;

	/* 
	 * Permet de recuperer un diagnostic a partir de l'Id
	 * @param id Id diagnostic
	 * @return diagnostic
	 */
	@Produces	
	public DiagnosticBean findDiagnostic(Integer id) {
		// TODO Auto-generated method stub
		try {
			DiagnosticBean diagnostic = em.find(DiagnosticBean.class, id);
			return diagnostic;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}

	}

	@Remove
	@Destroy
	public void destroy() {
	}

	/* 
	 * Permet de recuperer la liste des diagnostics a partir de l'Id du rendez-vous
	 *  * @param idRv id rendez-vous
	 * @return La liste des diagnostics
	 */
	public List<DiagnosticBean> findDiagnosticsRendezVous(int idRv) {
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("diagnostic");
			BasicDBObject query = new BasicDBObject("rendez_vous_id", idRv );
			DBCursor cursor = dbCollection.find(query);

			List<DiagnosticBean> listeDiagnostics = new ArrayList<DiagnosticBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idDiagnostic = (Integer) item.get("_id");
				DiagnosticBean diagnostic = em.find(DiagnosticBean.class, idDiagnostic);
				listeDiagnostics.add(diagnostic);
			}
			return listeDiagnostics;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet d'enregistrer (ajouter ou modifier)
	 *   @param diagnostic diagnostic à mettre à jour
	 */
	public void saveDiagnostic(DiagnosticBean diagnostic) {
		// TODO Auto-generated method stub
		try{
			//A modifier
			diagnostic.setDent(null);
			diagnostic.setFicheEchographieTr1(null);
			diagnostic.setFicheEchographieTr2Tr3(null);
			diagnostic.setFicheGyneco(null);
			diagnostic.setImage(null);
			diagnostic.setMedicament(null);
			diagnostic.setMontantExamen(diagnostic.getExamen().getPrix());
			diagnostic.setMotifExamen(diagnostic.getExamen().getLibelle());
			diagnostic.setNomMalade(diagnostic.getRendezVous().getPatient().getPrenom()+" "+diagnostic.getRendezVous().getPatient().getNom());
			if(diagnostic.getRendezVous().getPatient().getBeneficiaire()!=null){
				diagnostic.setIdclient(diagnostic.getRendezVous().getPatient().getBeneficiaire().getClientTaux().getClient().getId());
				double partInteresse=(diagnostic.getExamen().getPrix())*(1.0-((diagnostic.getRendezVous().getPatient().getBeneficiaire().getClientTaux().getTaux())/100.0));
				double partSociete=diagnostic.getExamen().getPrix()*((diagnostic.getRendezVous().getPatient().getBeneficiaire().getClientTaux().getTaux())/100);
				diagnostic.setPartInteresse(partInteresse);
				diagnostic.setPartSociete(partSociete);
			}
			else{
				diagnostic.setIdclient(null);
				diagnostic.setPartSociete(null);
				diagnostic.setPartInteresse(diagnostic.getExamen().getPrix());
			}
			RendezVousBean rv = diagnostic.getRendezVous();
			rv = em.merge(rv);
			diagnostic.setRendezVous(rv);
			InfosIrmBean infoIrm = diagnostic.getInfosIrm();
			if(infoIrm != null){
				infoIrm = em.merge(infoIrm);
				diagnostic.setInfosIrm(infoIrm);
			}
			
			em.merge(diagnostic);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/* 
	 *  Permet de recuprer la liste des examens pour un type et un service donné
	 *   @param idType Id yype
	 *   @param service le nom du service
	 *   @return La liste des examens
	 */
	public List<ExamenBean> findExamensType(int idType, String service){
		// TODO Auto-generated method stub
		try {
			System.out.println("idType "+idType);
			DBCollection dbCollection = db.getCollection("examen");
			BasicDBObject query = new BasicDBObject();
			if(idType != 0){
				query = new BasicDBObject("groupe_examen_id",idType);
			}
			DBCursor cursor = dbCollection.find(query);
			List<ExamenBean> listeExamens = new ArrayList<ExamenBean>();
			int i = 0;
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idExamen = (Integer) item.get("_id");
				ExamenBean examen = em.find(ExamenBean.class, idExamen);
				if(examen.getGroupeExamen().getService().getQualificatif().compareTo(service) == 0){
					if(service.compareTo(ConstUtil.SERVICE_RADIOLOGIE) != 0 || examen.getGroupeExamen().getLibelle().compareTo(ConstUtil.LIBELLE_IRM) != 0){
						listeExamens.add(examen);
						i++;
					}
				}
			}
			System.out.println("Total "+i);
			return listeExamens;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	public List<ExamenBean> findExamensType2(List<GroupeExamenBean> groupe, String service){
		// TODO Auto-generated method stub
		try {
			List<Integer> listgroupe=new ArrayList<Integer>();
			for (GroupeExamenBean groupeExamenBean : groupe) {
				listgroupe.add(groupeExamenBean.getId());
			}
			BasicDBList docIds = new BasicDBList();
			docIds.addAll(listgroupe);
			DBObject inClause = new BasicDBObject("$in", docIds);
			DBObject query = new BasicDBObject("groupe_examen_id", inClause);
			DBCollection dbCollection = db.getCollection("examen");
			DBCursor cursor = dbCollection.find(query);
			List<ExamenBean> listeExamens = new ArrayList<ExamenBean>();
			int i = 0;
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idExamen = (Integer) item.get("_id");
				ExamenBean examen = em.find(ExamenBean.class, idExamen);
				if(examen.getGroupeExamen().getService().getQualificatif().compareTo(service) == 0){
					listeExamens.add(examen);
					i++;
				}
			}
			return listeExamens;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer la liste des types d'examens non IRM 
	 *  @return La liste des types des examens
	 */
	public List<GroupeExamenBean> findListeTypesNonIrm(){
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("groupe_examen");
			String libelleIRM = ConstUtil.LIBELLE_IRM;
			BasicDBObject query = new BasicDBObject("libelle", new BasicDBObject("$ne", libelleIRM) );
			DBCursor cursor = dbCollection.find(query);
			List<GroupeExamenBean> listeGroupeExamens = new ArrayList<GroupeExamenBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idGroupeExamen = (Integer) item.get("_id");
				GroupeExamenBean groupeExamen = em.find(GroupeExamenBean.class, idGroupeExamen);
				listeGroupeExamens.add(groupeExamen);
			}
			return listeGroupeExamens;
		}
		catch (Exception e){
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	public List<GroupeExamenBean> findgroupeExamService(int service){
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("groupe_examen");
			BasicDBObject query = new BasicDBObject("service_id",service);
			DBCursor cursor = dbCollection.find(query);
			List<GroupeExamenBean> listeGroupeExamens = new ArrayList<GroupeExamenBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idGroupeExamen = (Integer) item.get("_id");
				GroupeExamenBean groupeExamen = em.find(GroupeExamenBean.class, idGroupeExamen);
				listeGroupeExamens.add(groupeExamen);
			}
			return listeGroupeExamens;
		}
		catch (Exception e){
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 * Permet de recuperer un examen a partir de l'Id
	 * @param id Id examen
	 * @return ExamenBean
	 */
	public ExamenBean findExamen(int id){
		// TODO Auto-generated method stub
		ExamenBean examen = em.find(ExamenBean.class, id);
		return examen;
	}

	/* 
	 *  Permet de recuprer la liste des medecins pour un service donné
	 *   @param service le nom du service
	 *    @return La liste des types des medecins
	 */
	public List<MedecinBean> findMedecinsService(String service) {
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("medecin");
			BasicDBObject query = new BasicDBObject();
			DBCursor cursor = dbCollection.find(query);
			List<MedecinBean> listeMedecins = new ArrayList<MedecinBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idMedecin = (Integer) item.get("_id");
				MedecinBean medecin = em.find(MedecinBean.class, idMedecin);
				if(medecin.getService().getQualificatif().compareTo(service) == 0){
					listeMedecins.add(medecin);
				}
			}
			return listeMedecins;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer la liste des salles pour un service donné
	 *   @param service le nom du service
	 *    @return La liste des salles
	 */
	public List<SalleBean> findSallesService(String service) {
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("salle");
			BasicDBObject query = new BasicDBObject();
			DBCursor cursor = dbCollection.find(query);
			List<SalleBean> listeSalles = new ArrayList<SalleBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idSalle = (Integer) item.get("_id");
				SalleBean salle = em.find(SalleBean.class, idSalle);
				if(salle.getService().getQualificatif().compareTo(service) == 0){
					listeSalles.add(salle);
				}
			}
			return listeSalles;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer la liste des interpretationType pour un service donné
	 *   @param service le nom du service
	 *    @return La liste des interpretationsType
	 */
	public List<InterpretationTypeBean> findInterpretationTypeService(String service) {
		// TODO Auto-generated method stub
		try {

			DBCollection dbCollection = db.getCollection("interpretation_type");
			BasicDBObject query = new BasicDBObject();
			DBCursor cursor = dbCollection.find(query);
			List<InterpretationTypeBean> listeInterpretationsType = new ArrayList<InterpretationTypeBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idInterpretationType = (Integer) item.get("_id");
				InterpretationTypeBean interpretationType = em.find(InterpretationTypeBean.class, idInterpretationType);
				if(interpretationType.getService().getQualificatif().compareTo(service) == 0){
					listeInterpretationsType.add(interpretationType);
				}
			}
			return listeInterpretationsType;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer la liste des techniques pour un service donné
	 *   @param service le nom du service
	 *    @return La liste des techniques
	 */
	public List<TechniqueUtiliseBean> findTechniquesService(String service) {
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("technique");
			BasicDBObject query = new BasicDBObject();
			DBCursor cursor = dbCollection.find(query);
			List<TechniqueUtiliseBean> listeTechniques = new ArrayList<TechniqueUtiliseBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idTechnique = (Integer) item.get("_id");
				TechniqueUtiliseBean technique = em.find(TechniqueUtiliseBean.class, idTechnique);
				if(technique.getService().getQualificatif().compareTo(service) == 0){
					listeTechniques.add(technique);
				}
			}
			return listeTechniques;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 * Permet de recuperer l'examen de type IRM
	 *  @return ExamenBean
	 */
	public ExamenBean findExamenIrm() {
		// TODO Auto-generated method stub
		try {
			DBCollection dbCollection = db.getCollection("examen");
			String libelleIRM = ConstUtil.LIBELLE_IRM;
			BasicDBObject query = new BasicDBObject("libelle",  libelleIRM);
			DBCursor cursor = dbCollection.find(query).sort(new BasicDBObject("_id",  1));
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idExamen = (Integer) item.get("_id");
				ExamenBean examen = em.find(ExamenBean.class, idExamen);
				return examen;
			}
			return null;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}


	public void saveInterpretations(InterpretationBean interprete){
		try {
			em.merge(interprete);
		} catch (Exception e) {
			System.out.println("save contantesRv EROR");
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	/* 
	 *  Permet de recuprer la liste des diagnostics d'un patient regroupés par service
	 *   @param idPatient Id patient
	 *   @param etat 
	 *    @return La liste des diagnostics regroupés par service
	 */
	@Produces
	public Map<String, List<DiagnosticBean>> listeDiagnostics(int idPatient,int etat){
		Map<String, List<DiagnosticBean>> listeTousDiagnostics = new HashMap<String, List<DiagnosticBean>>();
		List<DiagnosticBean> listDiagnosticsOdontologie=new ArrayList<DiagnosticBean>();
		List<DiagnosticBean> listDiagnosticsRadiologie=new ArrayList<DiagnosticBean>();
		List<DiagnosticBean> listDiagnosticsGynecologie=new ArrayList<DiagnosticBean>();
		try
		{
			DBCollection dbCollection = db.getCollection("diagnostic");
			BasicDBObject query = new BasicDBObject("etat", etat);
			DBCursor cursor = dbCollection.find(query);

			if(cursor.size()>0){
				while (cursor.hasNext()){
					DBObject item = cursor.next();
					int idDiagnostic = (Integer) item.get("_id");
					DiagnosticBean diagnostic = em.find(DiagnosticBean.class, idDiagnostic);
					RendezVousBean rv = diagnostic.getRendezVous();
					if(rv.getEtat().compareTo(ConstUtil.ETAT_RENDEZVOUS_CONFIRME) == 0){
						if(rv.getPatient().getId() == idPatient){
							if(rv.getService().getQualificatif().compareTo(ConstUtil.SERVICE_ODONTOLOGIE) == 0){
								listDiagnosticsOdontologie.add(diagnostic);
							}
							else if(rv.getService().getQualificatif().compareTo(ConstUtil.SERVICE_RADIOLOGIE) == 0){
								listDiagnosticsRadiologie.add(diagnostic);
							}
							else if(rv.getService().getQualificatif().compareTo(ConstUtil.SERVICE_GYNECOLOGIE) == 0){
								listDiagnosticsGynecologie.add(diagnostic);
							}
						}
					}
				}
				listeTousDiagnostics.put(ConstUtil.SERVICE_ODONTOLOGIE, listDiagnosticsOdontologie);
				listeTousDiagnostics.put(ConstUtil.SERVICE_RADIOLOGIE, listDiagnosticsRadiologie);
				listeTousDiagnostics.put(ConstUtil.SERVICE_GYNECOLOGIE, listDiagnosticsGynecologie);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
		return listeTousDiagnostics;
	}

	/* 
	 *  Permet de recuprer le dernier diagnostic de type IRM d'un patient
	 *   @param idPatient Id patient
	 *   @return DiagnosticBean
	 */
	public DiagnosticBean dernierDiagnosticIrm(int idPatient){
		try
		{
			DBCollection dbCollection = db.getCollection("diagnostic");
			ExamenBean examenIrm = this.findExamenIrm();
			int idExamenIrm = examenIrm.getId();
			System.out.println("Appel dernierDiagnosticIrm "+ idExamenIrm);
			BasicDBObject query = new BasicDBObject("examen_id", idExamenIrm);
			DBCursor cursor = dbCollection.find(query).sort(new BasicDBObject("date", -1));

			if(cursor.size()>0){
				while (cursor.hasNext()){
					DBObject item = cursor.next();
					int idDiagnostic = (Integer) item.get("_id");
					DiagnosticBean diagnostic = em.find(DiagnosticBean.class, idDiagnostic);
					RendezVousBean rv = diagnostic.getRendezVous();
					if(rv.getPatient().getId() == idPatient){						
						return diagnostic;
					}
				}
			}
			System.out.println("diagnostic null");
			return null;
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer la liste des images pour un diagnostic
	 *   @param idDiagnostic Id diagnostic
	 */
	public List<ImageBean> findListeImagesDiagnostic(){
		// a modifier prendre en compte le diagnostic
		try {
			DBCollection dbCollection = db.getCollection("image");
			BasicDBObject query = new BasicDBObject();
			DBCursor cursor = dbCollection.find(query);
			List<ImageBean> listeImages = new ArrayList<ImageBean>();
			while (cursor.hasNext()){
				DBObject item = cursor.next();
				int idImage = (Integer) item.get("_id");
				ImageBean image = em.find(ImageBean.class, idImage);
				listeImages.add(image);
			}
			return listeImages;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 *  Permet de recuprer les infos IRM du dernier diagnostic de type IRM d'un patient
	 *   @param idPatient Id patient
	 *    @return InfosIrmBean
	 */
	public InfosIrmBean dernierIrm(int idPatient) {
		// TODO Auto-generated method stub
		return null;
	}

	/* 
	 *  Permet de supprimer un diagnostic
	 *   @param diagnostic Diagnostic à supprimer
	 */
	public void supprimerDiagnostic(DiagnosticBean diagnostic) {
		// TODO Auto-generated method stub
		try {
			em.remove(em.find(DiagnosticBean.class, diagnostic.getId()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/* 
	 * Permet de recuperer une image a partir de l'Id
	 * @param id Id image
	 *  @return ImageBean
	 */
	public ImageBean findImage(Integer id) {
		// TODO Auto-generated method stub
		try {
			ImageBean image = em.find(ImageBean.class, id);
			return image;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 * Permet de recuperer un InterpretationType a partir de l'Id
	 * @param id Id interpretationType
	 * @return InterpretationTypeBean
	 */
	public InterpretationTypeBean findInterpretationType(
			int id) {
		// TODO Auto-generated method stub
		try {
			InterpretationTypeBean interType = em.find(InterpretationTypeBean.class, id);
			return interType;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

}
