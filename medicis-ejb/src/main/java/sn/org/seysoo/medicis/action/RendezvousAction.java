package sn.org.seysoo.medicis.action;

import static org.jboss.seam.ScopeType.SESSION;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.resource.cci.Record;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Renderer;
import org.jboss.seam.jsf.SetDataModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

import sn.org.seysoo.medicis.action.interfaces.IFactureService;
import sn.org.seysoo.medicis.action.interfaces.IRendezvous;
import sn.org.seysoo.medicis.action.interfaces.IRendezvousServices;
import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.action.interfaces.IServicePatient;
import sn.org.seysoo.medicis.action.interfaces.IpatientAction;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.ClientBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.GroupeSanguinBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.MotifBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.ServiceBean;
import sn.org.seysoo.medicis.entity.TypeClientBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.StringUtil;


/**
 * @author Khadija
 *
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("rendezVous")
public class RendezvousAction implements IRendezvous{

	@DataModel(value="lRendezvous")
	private List<RendezVousBean> lRendezvous; 

	@DataModelSelection(value="lRendezvous")
	
	@In(create=true,required=false)
	@Out(required=false, scope = SESSION)
	private RendezVousBean rvSelected ;

	//@DataModelSelection(value="lRendezvous")
	private RendezVousBean selectedRendezvous = new RendezVousBean();
	
	private DualListModel<ExamenBean> listeExamensTypeRv;
	
	@In(create = true, required = false)
	@Out(required=false, scope = SESSION)
	private RendezVousBean rendezVousBean=new RendezVousBean();

	//private RendezVousDataModel  listeDMRendezvous;
	
	@PersistenceContext
	private EntityManager em;

	private int idrvselected=0;


	private List <RendezVousBean> listRendezvous;
	private List<DiagnosticBean> listeDiagnostics;

	private String sexe;
	private String datenais;
	private String prenom;
	private String nom;
	private String email;
	private String dateRV;
	private String heureDeb;
	private String heureFin;
	private String tel;
	private String motif;
	private String service;
	private boolean newPatient = false;

	private RendezVousBean rendezvs;
	private PatientBean patien;
	private ServiceBean servi;
	private MotifBean moti;
	private GroupeSanguinBean grpSanguin;

	// new rv
	private String gs;
	private String prfil;
	private String nomPatient;
	private String prenomPatient;
	private String sexePatient;
	private String telPatient;
	private String emailPatient;
	private String datenaisPatient;
	private String dateRVPatient;
	private String heureDebPatient;
	private String motifPatient;
	private String servicePatient;
	private String dateNaiss;
	private String profession;
	private String tail;
	private String cni;
	private String telp;
	private String poids2;
	private String address;
	private String email2;
	private String profil;
	private String signespart;

	// affiliation
	private String sexeAff;
	private String prenomAff;
	private String nomAff;
	private String matrAff;
	private String policeAff;
	private String clienAff;
	private String tauxAff;
	private String telAff;
	private String emailAff;

	//dates
	private Date dateRendezvousP ;
	private Date dateNaissanceP ;
	
	private int idgroupeexam=0;
	@In(value="#{facesContext}")
	FacesContext facesContext;

	@In(value="#{facesContext.externalContext}")
	private ExternalContext extCtx;
	private String newpat=""+1;

	//Hash Map
	private Map<String,String> typesClient = new HashMap<String,String>();
	private Map<String,String> clientAssureurs = new HashMap<String,String>();
	private Map<String,String> tauxAssurances = new HashMap<String,String>();
	private Map<String,String> services = new HashMap<String,String>();
	private Map<String,String> motifs = new HashMap<String,String>();
	Map<String, String> listMotifParService = new HashMap<String, String>();
	private String out;

	// bouton control

	Record activeRecord;
	
	//periode rendezvous
	private Date dateDebRv;
	private Date dateFinRv;
	
	//periode rendezvous confirmés	
	private Date dateDebRvc;
	private Date dateFinRvc;
	
	//periode rendezvous confirmés	
	private Date dateDebRva;
	private Date dateFinRva;
	
	//date de reprogramme pour un rv
	private Date dateRepro;
	
	@In
	private FacesMessages facesMessages;

	@DataModel("listClient")
	private List<ClientBean> listClient;

	@DataModel("listTaux")
	private List<ClientTauxBean> listTaux;

	@DataModel("listService")
	private List<ServiceBean> listService;

	@DataModel("listMotif")
	private List<MotifBean> listMotif;

	//@DataModel("listMotifsService")
	private List<MotifBean> listMotifsService;
	
	@In(create = true, required = false)
	@Out(required=false, scope = SESSION)
	private PatientBean patientSelectioner;
	
	private List<GroupeExamenBean> groupeExam;
	private List<GroupeExamenBean> groupeExamSelectioner;
	private SetDataModel diagnosticsDataModel;
	
	private String choixExamen;
	private Boolean plusInfos;
	//			 List<MotifBean> listMotif = null; 

	
	@DataModel(value="lRendezvousConf")
	private List<RendezVousBean> lRendezvousConf; 

	@DataModelSelection(value="lRendezvousConf")
	@In(create=true,required=false)
	@Out(required=false, scope = SESSION)
	private RendezVousBean selectedRvConfirme ;

	@DataModel(value="lRendezvousAnnule")
	private List<RendezVousBean> lRendezvousAnnule; 


	@DataModelSelection(value="lRendezvousAnnule")
	@In(create=true,required=false)
	@Out(required=false, scope = SESSION)
	private RendezVousBean selectedRvannule ;

	@DataModel(value="lDiagnostiqueByRv")
	private List<DiagnosticBean> lDiagnostiqueByRv; 

	@DataModel(value="lDiagnostiqueByRvAn")
	private List<DiagnosticBean> lDiagnostiqueByRvAn; 

	@DataModel(value="lDiagnostiqueByRvCo")
	private List<DiagnosticBean> lDiagnostiqueByRvCo; 




	//@Produces
	// Liste de tous les rendez-vous  
	@Factory("lRendezvous")
	public void getAllRendezvous (){

		String etatDesRendezvous = "defaut";
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		//lRendezvous = rvService.listRendezvous();
		if (dateDebRv==null && dateFinRv==null)
		  lRendezvous=rvService.searchAllRendezvous(etatDesRendezvous);
		else
			lRendezvous=rvService.searchRendezvousByPeriod(etatDesRendezvous, dateDebRv, dateFinRv);

		///////////////////////////////////////////////////////////////////////////////////
		if(lRendezvous!=null )
			System.out.println("Taille liste rendez vous  =  "  + lRendezvous.size()); 

		else
			System.out.println("--> Liste RV NULL --"); 
	}
	
	// Liste des  rendez-vous déjé confirmés
	@Factory("lRendezvousConf")
	public void getRendezvousConfirmes (){

		String etatRendezvous = "confirme";
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		//lRendezvous = rvService.listRendezvous();
		if (dateDebRvc==null && dateFinRvc==null)
		lRendezvousConf=rvService.searchAllRendezvous(etatRendezvous);
		else
			lRendezvousConf=rvService.searchRendezvousByPeriod(etatRendezvous, dateDebRvc, dateFinRvc);


		if(lRendezvousConf!=null)
			System.out.println("Taille liste rendez vous confirme =  "  + lRendezvousConf.size()); 

		else
			System.out.println("--> Liste RV confirme NULL --"); 
	}


	// Liste des  rendez-vous déjé annulés
	@Factory("lRendezvousAnnule")
	public void getRendezvousAnnules(){

		String etatRendezvous = "annule";
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		//lRendezvous = rvService.listRendezvous();
		if (dateDebRva==null && dateFinRva==null)
			lRendezvousAnnule=rvService.searchAllRendezvous(etatRendezvous);
		else
			lRendezvousAnnule=rvService.searchRendezvousByPeriod(etatRendezvous, dateDebRva, dateFinRva);


		if(lRendezvousAnnule!=null)
			System.out.println("Taille liste rendez vous annule =  "  + lRendezvousAnnule.size()); 

		else
			System.out.println("--> Liste RV confirme NULL --"); 
	}

	//@Factory("lRendezvous")
	public void getRendezvousByPeriod (){

		String etatDesRendezvous = "defaut";
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		//lRendezvous = rvService.listRendezvous();
		lRendezvous=rvService.searchRendezvousByPeriod(etatDesRendezvous, dateDebRv, dateFinRv);

		if(lRendezvous!=null )
			System.out.println("Taille liste rendez vous p�riode du  "+dateDebRv+"  au"+dateFinRv+"  size = "  + lRendezvous.size()); 
		else
			System.out.println("--> Liste RV NULL --"); 
	}
	


	//Confirmer un rendez-vous
	public void confirmeRendezvous(){
		
		System.out.println(" **********************   dans confirmeRendezvous  ***************************************  ");
		
		int idrvselected =  (Integer) Contexts.getSessionContext().get("idrvselected");

		System.out.println("confirmeRendezvous  -- idrvselected = " + idrvselected); 

		if (idrvselected !=0){
			//IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
			System.out.println("dans if -- selectedRendezvous>0");

			RendezVousBean selectedRendezvous = em.find(RendezVousBean.class, idrvselected);
			
			System.out.println("selectedRendezvous etat :  " +selectedRendezvous.getEtat());
			System.out.println("nom :  " +selectedRendezvous.getPatient().getNom() +"    prenom :  " +   selectedRendezvous.getPatient().getPrenom());

			selectedRendezvous.setEtat(ConstUtil.ETAT_RENDEZVOUS_CONFIRME);
			selectedRendezvous.setDateConfirmation(new Date()); 
			//selectedRendezvous=rvService.EnregistrerRendezvous(selectedRvConfirme);
			System.out.println("dans if -- avant merge");
			selectedRendezvous=em.merge(selectedRendezvous) ;
			System.out.println("dans if -- apres merge");
			


		}
		
		
	}
	//confirmer une reprogrammation de rvget
	public void confirmeReprogrammationRV(){
		
		System.out.println(" **********************   dans confirmeReprogrammationRV  ***************************************  ");
		
		int idrvselected =  (Integer) Contexts.getSessionContext().get("idrvselected");

		System.out.println("confirmeReprogrammationRV  -- idrvselected = " + idrvselected); 

		if (idrvselected !=0){
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
			System.out.println("dans if -- selectedRendezvous>0");

			RendezVousBean selectedRendezvous = em.find(RendezVousBean.class, idrvselected);
			
			System.out.println("selectedRendezvous etat :  " +selectedRendezvous.getEtat());
			System.out.println("nom :  " +selectedRendezvous.getPatient().getNom() +"    prenom :  " +   selectedRendezvous.getPatient().getPrenom());

			selectedRendezvous.setEtat(ConstUtil.ETAT_RENDEZVOUS_DEFAUT);
			selectedRendezvous.setDateConfirmation(dateRepro); 
			//selectedRendezvous=rvService.EnregistrerRendezvous(selectedRvConfirme);
			System.out.println("dans if -- avant merge");
			selectedRendezvous=rvService.EnregistrerRendezvous(selectedRendezvous);
			System.out.println("dans if -- apres merge");
			
			//getRendezvousAnnules();
			


		}
		
			
	}

	// Methode pour charger la liste des motifs é partir d'un sevice. (dépendance entre 02 listes ou listes liées)
	 public void chargerMotif (ValueChangeEvent event){
		 
			System.out.println("---valueChanged---");
			System.out.println("---valueChanged - getNewValue : "+event.getNewValue());
			System.out.println("---valueChanged - getOldValue : "+event.getOldValue());
			
			IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

			//String idService=null;
			listMotifParService.clear();

			if(event.getNewValue()!=null ){
				String idService=((String) event.getNewValue());
				listMotifsService = rvService.listMotifbyService(new Integer(idService)); 

				System.out.println("idService  ====  "  +idService); 

				 if (listMotifsService!=null){
					 System.out.println("size liste motif : " + listMotifsService.size()); 
					 System.out.println("---------------"); 
					 for (MotifBean motif : listMotifsService){
					    System.out.println("libelle motif : " +motif.getLibelle()); 
					    listMotifParService.put(motif.getLibelle(),""+motif.getLibelle());
					   //services.put(listService.get(i).getLibelle(),""+listService.get(i).getId());
					 }
					 
					Map<String, String> treeMap = new TreeMap<String, String>(listMotifParService);
					listMotifParService = treeMap;
									 
				 }

			}
			

		}
	 
	



	
	// Modification d'un RV
	public void saveAndmodifyRV(){

		try {
			System.out.println("dans try --saveAndmodifyRV");

			IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			//SimpleDateFormat FORMATTER =  new SimpleDateFormat("EEE MMM dd HH:mm:ss 'UTC' yyyy",  Locale.ENGLISH);



			nom = rvSelected.getPatient().getNom();
			prenom = rvSelected.getPatient().getPrenom();
			//datenais = rvSelected.getPatient().getDateNissance().toString();

			System.out.println("datenaiss :  "  +rvSelected.getPatient().getDateNissance());


			tel = rvSelected.getPatient().getTelephonePatient();
			email = rvSelected.getPatient().getEmail();
			motif = rvSelected.getMotif().getLibelle();
			heureDeb = rvSelected.getHeure();
			//dateRV = rvSelected.getDate().toString();
			service = rvSelected.getService().getLibelle();
			//date = FORMATTER.parse(datenais.trim());

			System.out.println("dateRV :  "  +rvSelected.getDate());


			rvSelected.getPatient().setNom(nom); 
			rvSelected.getPatient().setPrenom(prenom); 
			rvSelected.getPatient().setDateNissance(rvSelected.getPatient().getDateNissance());
			rvSelected.getPatient().setTelephonePatient(tel);
			rvSelected.getPatient().setEmail(email);

			rvService.savePatients(rvSelected.getPatient());

			rvSelected.getService().setLibelle(service); 
			rvService.saveService(rvSelected.getService());


			rvSelected.getMotif().setLibelle(motif);
			rvService.saveMotif(rvSelected.getMotif());



			rvSelected.setHeure(heureDeb);
			rvSelected.setDate(rvSelected.getDate());
			rvService.saveRendezvous(rvSelected); 

			this.newPatient = true;

			facesMessages.add("modification effectuee avec succes!");

		} catch (Exception e) {

			facesMessages.add("erreur dans la modification")  ;
			System.out.println("--Modify Rv erreur--");
			e.printStackTrace();
			// TODO: handle exception
		}

	}


	// ajout d'un nouveau rv

	public void save_Rendezvous(){

		System.out.println("RendezVousAction --->> save_Rendezvous");

		//IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
		ClientTauxBean clientTauxBean = new  ClientTauxBean();
		ClientBean clientBean = new ClientBean();
		TypeClientBean typeClientBean = new TypeClientBean() ;



		try {
			System.out.println("dans try --ajout rendez vous");

			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			//SimpleDateFormat formatter =  new SimpleDateFormat("EEE MMM dd HH:mm:ss 'UTC' yyyy",  Locale.ENGLISH);


			RendezVousBean rendezVousBean = new RendezVousBean();
			PatientBean patientBean = new PatientBean();
			MotifBean motifBean = new MotifBean();
			ServiceBean serviceBean = new ServiceBean();
			GroupeSanguinBean groupeSanguinBean = new GroupeSanguinBean();
			BeneficiaireBean beneficiaireBean = new BeneficiaireBean();

			
			/** ServiceBean*/
			//serviceBean.setId(new Integer(1));
			serviceBean.setEtat("etat1") ;
			serviceBean.setLibelle(servicePatient); 
			serviceBean.setQualificatif("qualif1");
			serviceBean=em.merge(serviceBean);

			/** Client TypeClient ClientTaux */

			typeClientBean.setLibelle("client xy");
			typeClientBean=em.merge(typeClientBean); 

			//clientBean.setAdresse(ad);
			//clientBean.setContact("hjkffgfghfhjfh");
			clientBean.setDesignation(clienAff);
			//clientBean.setEcheance(1);
			//clientBean.setTelephone("00221338215856");
			//clientBean.setSolde(81000000);
			clientBean.setTypeClient(typeClientBean);
			clientBean = em.merge(clientBean);

			clientTauxBean.setTaux(Double.parseDouble(tauxAff));
			clientTauxBean.setClient(clientBean);
			clientTauxBean=em.merge(clientTauxBean);



			/** Beneficiaire*/
			beneficiaireBean.setEmail(emailAff);
			beneficiaireBean.setMatricule(matrAff);
			beneficiaireBean.setNom(nomAff);
			beneficiaireBean.setPolice(policeAff);
			beneficiaireBean.setPrenom(prenomAff);
			beneficiaireBean.setTelephone(telAff);
			beneficiaireBean.setClientTaux(clientTauxBean);
			beneficiaireBean = em.merge(beneficiaireBean);
			/** Motifs */
			//motifBean.setId(new Integer(1));
			//motifBean.setDesription(motifPatient) ;
			motifBean.setLibelle(motifPatient);
			motifBean.setService(serviceBean);
			motifBean=em.merge(motifBean);

			/** Groupe sanguin*/
			//groupeSanguinBean.setId(new Integer(1));
			groupeSanguinBean.setLibelle(gs);
			groupeSanguinBean=em.merge(groupeSanguinBean);

			/*Patient*/
			//patientBean.setId(new Integer(1));
			patientBean.setAdresse(address) ;
			//patientBean.setAffectionsLd("Rhume");
			//patientBean.setAllergies("allergie10") ;
			//patientBean.setAntecedentsFamiliaux("antecedant fam10");
			patientBean.setAntecedentsPersonnels(signespart);
			//patientBean.setBeneficiaire(beneficiaire);
			patientBean.setCni(cni);
			patientBean.setDateNissance(dateNaissanceP);
			patientBean.setEmail(email2);
			patientBean.setEtatDossier(new Integer(3)); 
			patientBean.setGroupeSanguin(groupeSanguinBean);
			patientBean.setNom(nomPatient);
			patientBean.setPrenom(prenomPatient);

			//patientBean.setNumDossier("000" +patientBean.getId());
			patientBean.setPoids(Double.parseDouble(poids2)); 
			//patientBean.setPreventionsDepistages("prevention3640");
			patientBean.setProfession(profession);
			patientBean.setSexe(sexePatient);
			patientBean.setTaille(Double.parseDouble(tail));
			patientBean.setUnitePoids("Kg");
			patientBean.setUniteTaille("metre");
			patientBean.setTelephonePatient(telPatient);
			//patientBean.setNumDossier("0008556");
			patientBean.setBeneficiaire(beneficiaireBean);
			patientBean=em.merge(patientBean);
			//patientBean.setNumDossier("0000" +patientBean.getId());
			patientBean=em.merge(patientBean);

			// Numero patient :
			patientBean.setNumDossier("000" +patientBean.getId());
			patientBean=em.merge(patientBean);


			/** Rendez vous */
			//rendezVousBean.setId(new Integer(1));
			rendezVousBean.setHeure(heureDebPatient);
			rendezVousBean.setDate(dateRendezvousP);
			rendezVousBean.setEtat("defaut") ;
			rendezVousBean.setMotif(motifBean);
			rendezVousBean.setPatient(patientBean);
			rendezVousBean.setService(serviceBean);
			rendezVousBean=em.merge(rendezVousBean);

			System.out.println("rendez vous id : "+rendezVousBean.getId());

			getAllRendezvous();

			facesMessages.add("Operation effectuee avec succes");

		
		}
		catch(PersistenceException e) {
			System.out.println("---saveRV exception : "+e.getMessage());
			e.printStackTrace();
		}



	}
	// fixer rv -save rv

	public void save_Rendezvous2 (){


		System.out.println("RendezVousAction --->> save_Rendezvous");

		//IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
		ClientTauxBean clientTauxBean = new  ClientTauxBean();
		ClientBean clientBean = new ClientBean();
		TypeClientBean typeClientBean = new TypeClientBean() ;



		try {
			System.out.println("dans try --ajout rendez vous");

			RendezVousBean rendezVousBean = new RendezVousBean();
			 RendezVousBean rendezvous = new RendezVousBean();
			PatientBean patientBean = new PatientBean();
			MotifBean motifBean = new MotifBean();
			//ServiceBean serviceBean = new ServiceBean();
			GroupeSanguinBean groupeSanguinBean = new GroupeSanguinBean();
			BeneficiaireBean beneficiaireBean = new BeneficiaireBean();
			
			if (rvSelected!=null)
			    rendezvous = em.find(RendezVousBean.class, rvSelected.getId()); 


			System.out.println("ndiayeeeeeeee datenaisPatient  :  "  +datenaisPatient); 

			

			/** ServiceBean*/
			//serviceBean.setId(new Integer(1));
			//serviceBean = rvSelected.getService();
			/*serviceBean.setEtat("etat11111") ;
			serviceBean.setLibelle(servicePatient); 
			serviceBean.setQualificatif("qualif1");
			serviceBean=em.merge(serviceBean);*/

			/** Client TypeClient ClientTaux */

			typeClientBean.setLibelle("client xy");
			typeClientBean=em.merge(typeClientBean); 

			//clientBean.setAdresse(ad);
			//clientBean.setContact("hjkffgfghfhjfh");
			clientBean.setDesignation(clienAff);
			//clientBean.setEcheance(1);
			//clientBean.setTelephone("00221338215856");
			//clientBean.setSolde(81000000);
			clientBean.setTypeClient(typeClientBean);
			clientBean = em.merge(clientBean);

			clientTauxBean.setTaux(Double.parseDouble(tauxAff));
			clientTauxBean.setClient(clientBean);
			clientTauxBean=em.merge(clientTauxBean);



			/** Beneficiaire*/
			beneficiaireBean.setEmail(emailAff);
			beneficiaireBean.setMatricule(matrAff);
			beneficiaireBean.setNom(nomAff);
			beneficiaireBean.setPolice(policeAff);
			beneficiaireBean.setPrenom(prenomAff);
			beneficiaireBean.setTelephone(telAff);
			beneficiaireBean.setClientTaux(clientTauxBean);
			beneficiaireBean = em.merge(beneficiaireBean);
			/** Motifs */
			//motifBean.setId(new Integer(1));
			//motifBean.setDesription(motifPatient) ;
			motifBean = rvSelected.getMotif();
			/*motifBean.setLibelle(motifPatient);
			motifBean.setService(serviceBean);
			
			motifBean=em.merge(motifBean);*/

			/** Groupe sanguin*/
			//groupeSanguinBean.setId(new Integer(1));
			groupeSanguinBean.setLibelle(gs);
			groupeSanguinBean=em.merge(groupeSanguinBean);

			/*Patient*/
			//patientBean.setId(new Integer(1));
			patientBean = rvSelected.getPatient();
			patientBean.setAdresse(address) ;
			//patientBean.setAffectionsLd("Rhume");
			//patientBean.setAllergies("allergie10") ;
			//patientBean.setAntecedentsFamiliaux("antecedant fam10");
			patientBean.setAntecedentsPersonnels(signespart);
			//patientBean.setBeneficiaire(beneficiaire);
			patientBean.setCni(cni);
			patientBean.setDateNissance(rvSelected.getPatient().getDateNissance());
			patientBean.setEmail(email2);
			patientBean.setEtatDossier(new Integer(3)); 
			patientBean.setGroupeSanguin(groupeSanguinBean);
			patientBean.setNom(rvSelected.getPatient().getNom());
			patientBean.setPrenom(rvSelected.getPatient().getPrenom());

			//patientBean.setNumDossier("000" +patientBean.getId());
			patientBean.setPoids(Double.parseDouble(poids2)); 
			//patientBean.setPreventionsDepistages("prevention3640");
			patientBean.setProfession(profession);
			patientBean.setSexe(rvSelected.getPatient().getSexe());
			patientBean.setTaille(Double.parseDouble(tail));
			patientBean.setUnitePoids("Kg");
			patientBean.setUniteTaille("metre");
			patientBean.setTelephonePatient(rvSelected.getPatient().getTelephonePatient());
			//patientBean.setNumDossier(rvSelected.getPatient().getNumDossier());
			patientBean.setBeneficiaire(beneficiaireBean);
			
			patientBean=em.merge(patientBean);
			//patientBean.setNumDossier("0000" +patientBean.getId());
			patientBean=em.merge(patientBean);

			/** Rendez vous */
			//rendezVousBean.setId(new Integer(1));
			rendezvous.setDate(rvSelected.getDate());
			rendezvous.setEtat(rvSelected.getEtat()) ;
			rendezvous.setMotif(motifBean);
			rendezvous.setPatient(patientBean);
			rendezvous=em.merge(rendezvous);

			System.out.println("rendez vous id : "+rendezVousBean.getId());

			facesMessages.add("Operation effectuee avec succes");


			
		}
		catch(PersistenceException e) {
			System.out.println("---saveRV exception : "+e.getMessage());
			e.printStackTrace();
		}




	}
	
	


	//récuperation de l'id du rv selection par servlet
	public void onSelectRV() {
		System.out.println("Appel de on select rendez-vous");
		//System.out.println("--onSelectRV---> idrvselected : "+idrvselected);

		HttpServletRequest request=(HttpServletRequest) extCtx.getRequest();
		idrvselected=Integer.parseInt(request.getParameter("form1:rdv_selection"));
		// Id dans la session pour etre récupérer dans la methode confirmation de l'annulation d'un RV
		Contexts.getSessionContext().set("idrvselected", idrvselected); 

	} 
	public void onSelectRVannule() {
		System.out.println("Appel de on select rendez-vous");
		//System.out.println("--onSelectRV---> idrvselected : "+idrvselected);

		HttpServletRequest request=(HttpServletRequest) extCtx.getRequest();
		idrvselected=Integer.parseInt(request.getParameter("form3:rdvann_selection"));
		// Id dans la session pour etre récupérer dans la methode confirmation de l'annulation d'un RV
		Contexts.getSessionContext().set("idrvselected", idrvselected); 

	} 
	
	public void onSelectRVConf() {
		System.out.println("Appel de on select rendez-vous");
		//System.out.println("--onSelectRV---> idrvselected : "+idrvselected);

		HttpServletRequest request=(HttpServletRequest) extCtx.getRequest();
		idrvselected=Integer.parseInt(request.getParameter("form2:rdvconf_selection"));
		// Id dans la session pour etre récupérer dans la methode confirmation de l'annulation d'un RV
		Contexts.getSessionContext().set("idrvselected", idrvselected); 

	} 
	
	//Confirmation annulation d'un RV
	public void confirmerAnnulation (ActionEvent actionEvent){

		System.out.println("dans confirmerAnnulation "); 

		int idrvselected =  (Integer) Contexts.getSessionContext().get("idrvselected");

		System.out.println("testtttttttttt  : idrvselected " + idrvselected); 

		if (idrvselected !=0){

			RendezVousBean selectedRendezvous = em.find(RendezVousBean.class, idrvselected);

			selectedRendezvous.setEtat("annule");
			selectedRendezvous=em.merge(selectedRendezvous) ;

			System.out.println("selectedRendezvous etat :  " +selectedRendezvous.getEtat());
			System.out.println("nom :  " +selectedRendezvous.getPatient().getNom() +"    prenom :  " +   selectedRendezvous.getPatient().getPrenom());
			facesMessages.add("Le rendez-vous est bien annulé!");


		}


	}
	
	// liste des diagnostics pour un RV
	
	public void listDiagnosticByRv () {
		
		System.out.println("******  dans liste des diagnostiques **********" ); 

		IFactureService factureService = (IFactureService) Component.getInstance("factureService");

		if (rvSelected!=null){
			
			System.out.println("rendez-vous  selectionne : " +rvSelected.getId()); 
			lDiagnostiqueByRv = factureService.getDiagnostics(rvSelected);

		}
		
		if (selectedRvannule!=null){
			
			System.out.println("rendez-vous  annulé selectionne : " +selectedRvannule.getId()); 
			lDiagnostiqueByRvAn = factureService.getDiagnostics(selectedRvannule);

		}
		
		if (selectedRvConfirme!=null){
			
			System.out.println("rendez-vous confirmé selectionne : " +selectedRvConfirme.getId()); 
			lDiagnostiqueByRvCo = factureService.getDiagnostics(selectedRvConfirme);

		}
		
		
		
		if (lDiagnostiqueByRv!=null)
			System.out.println("liste des diagnostiques par services  :  "  + lDiagnostiqueByRv.size()); 
		
		if (lDiagnostiqueByRvAn!=null)
			System.out.println("liste des diagnostiques par services  :  "  + lDiagnostiqueByRvAn.size()); 
		
		if (lDiagnostiqueByRvCo!=null)
			System.out.println("liste des diagnostiques par services  :  "  + lDiagnostiqueByRvCo.size()); 
		
		//getDiagnostics
	}

	/**** Méthode destroy et Remove **************/

	@Destroy
	public void destroy() {
	}

	@Remove
	public void remove() {
	}

	public List<RendezVousBean> getlRendezvous() {
		return lRendezvous;
	}

	public void setlRendezvous(List<RendezVousBean> lRendezvous) {
		this.lRendezvous = lRendezvous;
	}


	public RendezVousBean getRvSelected() {
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
		if (idrvselected!=0){

			this.rvSelected=rvService.findRendezvous(idrvselected);
			System.out.println("Id de Rv selected"+rvSelected.getId());
		}
		return rvSelected;
	}


	public void setRvSelected(RendezVousBean rvSelected) {
		this.rvSelected = rvSelected;
	}


	public String getSexe() {
		return sexe;
	}


	public void setSexe(String sexe) {
		this.sexe = sexe;
	}


	public String getDatenais() {
		return datenais;
	}


	public void setDatenais(String datenais) {
		this.datenais = datenais;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDateRV() {
		return dateRV;
	}


	public void setDateRV(String dateRV) {
		this.dateRV = dateRV;
	}


	public String getHeureDeb() {
		return heureDeb;
	}


	public void setHeureDeb(String heureDeb) {
		this.heureDeb = heureDeb;
	}


	public String getHeureFin() {
		return heureFin;
	}


	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getMotif() {
		return motif;
	}


	public void setMotif(String motif) {
		this.motif = motif;
	}


	public String getService() {
		return service;
	}


	public void setService(String service) {
		this.service = service;
	}


	public RendezVousBean getRendezvs() {
		return rendezvs;
	}


	public void setRendezvs(RendezVousBean rendezvs) {
		this.rendezvs = rendezvs;
	}


	public PatientBean getPatien() {
		return patien;
	}


	public void setPatien(PatientBean patien) {
		this.patien = patien;
	}


	public ServiceBean getServi() {
		return servi;
	}


	public void setServi(ServiceBean servi) {
		this.servi = servi;
	}


	public MotifBean getMoti() {
		return moti;
	}


	public void setMoti(MotifBean moti) {
		this.moti = moti;
	}


	public GroupeSanguinBean getGrpSanguin() {
		return grpSanguin;
	}


	public void setGrpSanguin(GroupeSanguinBean grpSanguin) {
		this.grpSanguin = grpSanguin;
	}


	/*public RendezVousDataModel getListeDMRendezvous() {
		return listeDMRendezvous;
	}


	public void setListeDMRendezvous(RendezVousDataModel listeDMRendezvous) {
		this.listeDMRendezvous = listeDMRendezvous;
	}
	 */

	public RendezVousBean getSelectedRendezvous() {
		return selectedRendezvous;
	}


	public void setSelectedRendezvous(RendezVousBean selectedRendezvous) {
		this.selectedRendezvous = selectedRendezvous;
	}



	public List <RendezVousBean> getListRendezvous() {
		return listRendezvous;
	}



	public void setListRendezvous(List <RendezVousBean> listRendezvous) {
		this.listRendezvous = listRendezvous;
	}

	// new rendez-vous

	public boolean isNewPatient() {
		return newPatient;
	}

	public void setNewPatient(boolean newPatient) {
		this.newPatient = newPatient;
	}

	public String getGs() {
		return gs;
	}

	public void setGs(String gs) {
		this.gs = gs;
	}

	public String getPrfil() {
		return prfil;
	}

	public void setPrfil(String prfil) {
		this.prfil = prfil;
	}

	public String getNomPatient() {
		return nomPatient;
	}

	public void setNomPatient(String nomPatient) {
		this.nomPatient = nomPatient;
	}

	public String getSexePatient() {
		return sexePatient;
	}

	public void setSexePatient(String sexePatient) {
		this.sexePatient = sexePatient;
	}

	public String getTelPatient() {
		return telPatient;
	}

	public void setTelPatient(String telPatient) {
		this.telPatient = telPatient;
	}

	public String getEmailPatient() {
		return emailPatient;
	}

	public void setEmailPatient(String emailPatient) {
		this.emailPatient = emailPatient;
	}

	public String getDatenaisPatient() {
		return datenaisPatient;
	}

	public void setDatenaisPatient(String datenaisPatient) {
		this.datenaisPatient = datenaisPatient;
	}

	public String getDateRVPatient() {
		return dateRVPatient;
	}

	public void setDateRVPatient(String dateRVPatient) {
		this.dateRVPatient = dateRVPatient;
	}

	public String getHeureDebPatient() {
		return heureDebPatient;
	}

	public void setHeureDebPatient(String heureDebPatient) {
		this.heureDebPatient = heureDebPatient;
	}

	public String getMotifPatient() {
		return motifPatient;
	}

	public void setMotifPatient(String motifPatient) {
		this.motifPatient = motifPatient;
	}

	public String getServicePatient() {
		return servicePatient;
	}

	public void setServicePatient(String servicePatient) {
		this.servicePatient = servicePatient;
	}

	public String getDateNaiss() {
		return dateNaiss;
	}

	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getTail() {
		return tail;
	}

	public void setTail(String tail) {
		this.tail = tail;
	}

	public String getCni() {
		return cni;
	}

	public void setCni(String cni) {
		this.cni = cni;
	}

	public String getTelp() {
		return telp;
	}

	public void setTelp(String telp) {
		this.telp = telp;
	}

	public String getPoids2() {
		return poids2;
	}

	public void setPoids2(String poids2) {
		this.poids2 = poids2;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public String getSignespart() {
		return signespart;
	}

	public void setSignespart(String signespart) {
		this.signespart = signespart;
	}

	public String getPrenomPatient() {
		return prenomPatient;
	}

	public void setPrenomPatient(String prenomPatient) {
		this.prenomPatient = prenomPatient;
	}



	// get et set Affiliation

	public String getSexeAff() {
		return sexeAff;
	}

	public void setSexeAff(String sexeAff) {
		this.sexeAff = sexeAff;
	}

	public String getPrenomAff() {
		return prenomAff;
	}

	public void setPrenomAff(String prenomAff) {
		this.prenomAff = prenomAff;
	}

	public String getNomAff() {
		return nomAff;
	}

	public void setNomAff(String nomAff) {
		this.nomAff = nomAff;
	}

	public String getMatrAff() {
		return matrAff;
	}

	public void setMatrAff(String matrAff) {
		this.matrAff = matrAff;
	}

	public String getPoliceAff() {
		return policeAff;
	}

	public void setPoliceAff(String policeAff) {
		this.policeAff = policeAff;
	}

	public String getClienAff() {
		return clienAff;
	}

	public void setClienAff(String clienAff) {
		this.clienAff = clienAff;
	}

	public String getTauxAff() {
		return tauxAff;
	}

	public void setTauxAff(String tauxAff) {
		this.tauxAff = tauxAff;
	}

	public String getTelAff() {
		return telAff;
	}

	public void setTelAff(String telAff) {
		this.telAff = telAff;
	}

	public String getEmailAff() {
		return emailAff;
	}

	public void setEmailAff(String emailAff) {
		this.emailAff = emailAff;
	}



	public Map<String,String> getClientAssureurs() {
//		IRendezvousServices rvService = (IRendezvousServices) Component.getInstance("rendezvousServices");
//
//		listClient = rvService.getAllClients(); 
//		for (int i=0; i<listClient.size(); i++ ){
//			clientAssureurs.put(listClient.get(i).getDesignation(),""+listClient.get(i).getId());
//		}
//
//		Map<String, String> treeMap = new TreeMap<String, String>(clientAssureurs);
//
//		clientAssureurs = treeMap;
		
		System.out.println("call to getClientAssureurs->>>>");
		
		findClientsByType();

		return clientAssureurs;
	}

	public void setClientAssureurs(Map<String,String> clientAssureurs) {
		this.clientAssureurs = clientAssureurs;
	}

	public List<ClientBean> getListClient() {

		return listClient;
	}

	public void setListClient(List<ClientBean> listClient) {
		this.listClient = listClient;
	}

	public Map<String,String> getTauxAssurances() {
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		listTaux = rvService.getTauxAssur(); 
		for (int i=0; i<listTaux.size(); i++ ){
			tauxAssurances.put(listTaux.get(i).getTaux()+" %",""+listTaux.get(i).getId());
		}

		Map<String, String> treeMap = new TreeMap<String, String>(tauxAssurances);

		tauxAssurances = treeMap;


		return tauxAssurances;
	}

	public void setTauxAssurances(Map<String,String> tauxAssurances) {
		this.tauxAssurances = tauxAssurances;
	}

	public List<ClientTauxBean> getListTaux() {
		return listTaux;
	}

	public void setListTaux(List<ClientTauxBean> listTaux) {
		this.listTaux = listTaux;
	}

	public Date getDateRendezvousP() {
		return dateRendezvousP;
	}

	public void setDateRendezvousP(Date dateRendezvousP) {
		this.dateRendezvousP = dateRendezvousP;
	}

	public Date getDateNaissanceP() {
		return dateNaissanceP;
	}

	public void setDateNaissanceP(Date dateNaissanceP) {
		this.dateNaissanceP = dateNaissanceP;
	}

	public String ouvrirFicheConsultationOdontologie(){
		String url = "/patient/dossier_patient/fiche_consultation/odontologie.xhtml";
		return url;
	}
	
	public String ouvrirFicheConsultationRadiologie(){
		System.out.println("appel ouvrirFicheConsultationRadiologie");
		String url = "/patient/dossier_patient/fiche_consultation/radiologie.xhtml";
		return url;
	}
	
	public String ouvrirFicheConsultationGynecologie(){
		String url = "/patient/dossier_patient/fiche_consultation/gynecologie.xhtml";
		return url;
	}

	public void ongroupeSelectionner(){
		System.out.println("Groupe selectioner");
		if(groupeExam!=null && groupeExam.size()>0){
			groupeExamSelectioner=new ArrayList<GroupeExamenBean>();
			HttpServletRequest request = (HttpServletRequest) extCtx.getRequest();
			for (GroupeExamenBean groupe : groupeExam ) {
				if(request.getParameter("fnouveauRendezvous:"+groupe.getLibelle()+"_input")!=null){
					System.out.println("Element selectionner "+groupe.getLibelle());
					groupeExamSelectioner.add(groupe);
				}
			}
		}else
		{
			System.out.println("Aucun groupe selectionner");
		}
	}
	public void onchoixExamen(){
		System.out.println("Dans on choix examen");
		System.out.println("Taille ="+listeExamensTypeRv.getTarget().size());
		Set<DiagnosticBean> listDiagnostics = new HashSet<DiagnosticBean>();
		List<ExamenBean> examensSelectionnes = listeExamensTypeRv.getTarget();
		for (Iterator iterator = examensSelectionnes.iterator(); iterator.hasNext();) {
			ExamenBean examenBean = (ExamenBean) iterator.next();
			DiagnosticBean diagnosticBean = new DiagnosticBean();
			diagnosticBean.setExamen(examenBean);
			MedecinBean medecinBean = new MedecinBean();
			diagnosticBean.setMedecin(medecinBean);
			SalleBean salleBean = new SalleBean();
			diagnosticBean.setSalle(salleBean);
			diagnosticBean.setInfosIrm(null);
			diagnosticBean.setEtat(ConstUtil.ETAT_DIAGNOSTIC_OUVERT);
			listDiagnostics.add(diagnosticBean);
		}
		//if(listDiagnostics.size() != 0){
			diagnosticsDataModel = new SetDataModel();
			diagnosticsDataModel.setWrappedData(listDiagnostics);
		//}
	}
	
	public DualListModel<ExamenBean> getListeExamensTypeRv() {
		try {
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component.getInstance("diagnosticService");
			String service = ConstUtil.SERVICE_RADIOLOGIE;
			if(groupeExamSelectioner==null)
				groupeExamSelectioner=new ArrayList<GroupeExamenBean>();
			List<ExamenBean> source = serviceDiagnostic.findExamensType2(groupeExamSelectioner, service);
			if (listeExamensTypeRv != null && listeExamensTypeRv.getTarget() != null) {
				listeExamensTypeRv.setSource(source);
			} else {
				List<ExamenBean> target = new ArrayList<ExamenBean>();
				source = new ArrayList<ExamenBean>();
				listeExamensTypeRv = new DualListModel<ExamenBean>(source, target);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return listeExamensTypeRv;
	}

	public void setListeExamensTypeRv(DualListModel<ExamenBean> listeExamensTypeRv) {
		this.listeExamensTypeRv = listeExamensTypeRv;
	}

	public Map<String,String> getMotifs() {
		
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		listMotif = rvService.getAllMotif(); 
		for (int i=0; i<listMotif.size(); i++ ){
			motifs.put(listMotif.get(i).getLibelle(),""+listMotif.get(i).getId());
		}

		Map<String, String> treeMap = new TreeMap<String, String>(motifs);

		motifs = treeMap;

		return motifs;
	}



	public void setMotifs(Map<String,String> motifs) {
		this.motifs = motifs;
	}



	public Map<String,String> getServices() {
		
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");

		try {
			listService = rvService.getAllServices(); 
			for (int i=0; i<listService.size(); i++ ){
				services.put(listService.get(i).getLibelle(),""+listService.get(i).getId());
			}

			Map<String, String> treeMap = new TreeMap<String, String>(services);

			services = treeMap;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("--> "+e.getMessage()+" <--");
			e.printStackTrace();
		}

		return services;

	}



	public void setServices(Map<String,String> services) {
		this.services = services;
	}



	public List<ServiceBean> getListService() {
		return listService;
	}



	public void setListService(List<ServiceBean> listService) {
		this.listService = listService;
	}



	public List<MotifBean> getListMotif() {
		return listMotif;
	}


	public void setListMotif(List<MotifBean> listMotif) {
		this.listMotif = listMotif;
	}

	public String getOut() {
		return out;
	}

	public void setOut(String out) {
		this.out = out;
	}

	public Map<String, String> getListMotifParService() {
		return listMotifParService;
	}

	public void setListMotifParService(Map<String, String> listMotifParService) {
		this.listMotifParService = listMotifParService;
	}

	public List<MotifBean> getListMotifsService() {
		return listMotifsService;
	}

	public void setListMotifsService(List<MotifBean> listMotifsService) {
		this.listMotifsService = listMotifsService;
	}
	public void onSelectPatient(){
		System.out.println("On trouve patienttttttttttttttttttt");
		HttpServletRequest request = (HttpServletRequest) extCtx.getRequest();
		IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
		this.patientSelectioner=new PatientBean();
		if(request.getParameter("patsel:pat_selection")!=null)
		patientSelectioner=servicepatient.findPatient(Integer.parseInt(request.getParameter("patsel:pat_selection")));
		this.rendezVousBean=new RendezVousBean();
		this.listeExamensTypeRv=new DualListModel<ExamenBean>();
		this.diagnosticsDataModel=new SetDataModel(); 
		this.groupeExam=new ArrayList<GroupeExamenBean>();
		System.out.println("Patient ="+patientSelectioner.getId());
	}
	public String nouveauRendezVous(){
		this.rendezVousBean=new RendezVousBean();
		this.listeExamensTypeRv=new DualListModel<ExamenBean>();
		this.diagnosticsDataModel=new SetDataModel(); 
		this.groupeExam=new ArrayList<GroupeExamenBean>();
		patientSelectioner=new PatientBean();
		return "/rendezvous/newRendezvous.seam";
	}
	public void updateFicheRecap(){
		IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
		rvSelected=new RendezVousBean();
		rvSelected.setPatient(patientSelectioner);
		rvSelected.setDate(rendezVousBean.getDate());
		rvSelected.setService(rvService.findServiceById(rendezVousBean.getService().getId()));
		lDiagnostiqueByRv=new ArrayList<DiagnosticBean>();
		if(diagnosticsDataModel!=null && diagnosticsDataModel.getRowCount()>0){
			Set<DiagnosticBean> listeDiagnostics = (Set<DiagnosticBean>) diagnosticsDataModel.getWrappedData();
				for (Iterator iterator = listeDiagnostics.iterator(); iterator.hasNext();){
					DiagnosticBean diagnosticBean = (DiagnosticBean) iterator
							.next();
					System.out.println("id medecin = "+diagnosticBean.getMedecin().getId()+" et idsalle = "+diagnosticBean.getSalle().getId());
					diagnosticBean.setSalle(rvService.findSalleById(diagnosticBean.getSalle().getId()));
					diagnosticBean.setMedecin(rvService.findMedecinById(diagnosticBean.getMedecin().getId()));
		lDiagnostiqueByRv.add(diagnosticBean);
		}
	}
		//rvSelected.sel
	}
	public void enregistrerRendeVous(){
		System.out.println("Nouveauuuuuuuuuuu Rendez-vousssssssssss");
		try{
			if(patientSelectioner!=null){
				System.out.println("Nom patient selectioner ="+patientSelectioner.getNom());
				IRendezvousServices rvService = ( IRendezvousServices) Component.getInstance("rendezvousServices");
				IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component.getInstance("diagnosticService");
				IServicePatient servicepatient = (IServicePatient) Component.getInstance("patientService");
				if(patientSelectioner.getId()==null){
					System.out.println("Nom pat select = "+patientSelectioner.getNom());
					patientSelectioner.setBeneficiaire(null);
					patientSelectioner.setGroupeSanguin(null);
					patientSelectioner=servicepatient.saveAndReturnpatient(patientSelectioner);
				}
				rendezVousBean.setPatient(patientSelectioner);
				rendezVousBean.setEtat(ConstUtil.ETAT_RENDEZVOUS_DEFAUT);
				rendezVousBean=rvService.EnregistrerRendezvous(rendezVousBean);
				List<ExamenBean> listExamSelectioner = listeExamensTypeRv.getTarget();
//				for (ExamenBean examenBean : listExamSelectioner) {
//					DiagnosticBean diagnosticBean=new DiagnosticBean();
//					diagnosticBean.setRendezVous(rendezVousBean);
//					diagnosticBean.setExamen(examenBean);
//					diagnosticBean.setDate(rendezVousBean.getDate());
//					serviceDiagnostic.saveDiagnostic(diagnosticBean);
//				}
				if(diagnosticsDataModel!=null && diagnosticsDataModel.getRowCount()>0){
				Set<DiagnosticBean> listeDiagnostics = (Set<DiagnosticBean>) diagnosticsDataModel.getWrappedData();
					for (Iterator iterator = listeDiagnostics.iterator(); iterator.hasNext();){
						DiagnosticBean diagnosticBean = (DiagnosticBean) iterator
								.next();
						if(diagnosticBean.getDate()==null)
							diagnosticBean.setDate(rendezVousBean.getDate());
						if(diagnosticBean.getMedecin()==null || diagnosticBean.getMedecin().getId()==null)
							diagnosticBean.setMedecin(null);
						if(diagnosticBean.getSalle()==null || diagnosticBean.getSalle().getId()==null)
							diagnosticBean.setSalle(null);
						diagnosticBean.setRendezVous(rendezVousBean);
						serviceDiagnostic.saveDiagnostic(diagnosticBean);
					}
				}
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Information", "Le rendez-vous est enregistré avec succés.");  
				RequestContext.getCurrentInstance().showMessageInDialog(message);
			}
			else
				System.out.println("Patient selectionner est null");
		}catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
				
	}
	public void newPatientSelected(){
		System.out.println("newPatientSelectedddddddddddddddddd");
		RequestContext context = RequestContext.getCurrentInstance();
		System.out.println("Val new pat = "+newpat);
		if(!newpat.equals("1")){
			context.execute("dlgpatient.show()");
		}
		else{
			System.out.println("Nouvea patient");
			if(this.patientSelectioner!=null){
				this.setPatientSelectioner(null);
				System.out.println("patient Selectioné nulllll");
				this.patientSelectioner=new PatientBean();
			}
		}
		//context.update(":fnouveauRendezvous:pinfopatient");
	}

	public List<RendezVousBean> getlRendezvousConf() {
		return lRendezvousConf;
	}

	public void setlRendezvousConf(List<RendezVousBean> lRendezvousConf) {
		this.lRendezvousConf = lRendezvousConf;
	}

	public RendezVousBean getSelectedRvConfirme() {
		return selectedRvConfirme;
	}

	public void setSelectedRvConfirme(RendezVousBean selectedRvConfirme) {
		this.selectedRvConfirme = selectedRvConfirme;
	}


	public Date getDateDebRv() {
		return dateDebRv;
	}

	public void setDateDebRv(Date dateDebRv) {
		this.dateDebRv = dateDebRv;
	}

	public Date getDateFinRv() {
		return dateFinRv;
	}

	public void setDateFinRv(Date dateFinRv) {
		this.dateFinRv = dateFinRv;
	}

	public Date getDateDebRvc() {
		return dateDebRvc;
	}

	public void setDateDebRvc(Date dateDebRvc) {
		this.dateDebRvc = dateDebRvc;
	}

	public Date getDateFinRvc() {
		return dateFinRvc;
	}

	public void setDateFinRvc(Date dateFinRvc) {
		this.dateFinRvc = dateFinRvc;
	}

	public List<RendezVousBean> getlRendezvousAnnule() {
		return lRendezvousAnnule;
	}

	public void setlRendezvousAnnule(List<RendezVousBean> lRendezvousAnnule) {
		this.lRendezvousAnnule = lRendezvousAnnule;
	}

	public Date getDateDebRva() {
		return dateDebRva;
	}

	public void setDateDebRva(Date dateDebRva) {
		this.dateDebRva = dateDebRva;
	}

	public Date getDateFinRva() {
		return dateFinRva;
	}

	public void setDateFinRva(Date dateFinRva) {
		this.dateFinRva = dateFinRva;
	}

	public RendezVousBean getSelectedRvannule() {
		return selectedRvannule;
	}

	public void setSelectedRvannule(RendezVousBean selectedRvannule) {
		this.selectedRvannule = selectedRvannule;
	}

	public Date getDateRepro() {
		return dateRepro;
	}

	public void setDateRepro(Date dateRepro) {
		this.dateRepro = dateRepro;
	}

	public void chargergroupeExam (ValueChangeEvent event){
		try{
			IServiceDiagnostic serviceDiagnostic = (IServiceDiagnostic) Component.getInstance("diagnosticService");
			if(groupeExam!=null){
				groupeExam.clear();
			}
			if(event.getNewValue()!=null ){
				Integer idService= (Integer)event.getNewValue();
				groupeExam=new ArrayList<GroupeExamenBean>();
				groupeExam=serviceDiagnostic.findgroupeExamService(new Integer(idService));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<GroupeExamenBean> getGroupeExam() {
		return groupeExam;
	}

	public void setGroupeExam(List<GroupeExamenBean> groupeExam) {
		this.groupeExam = groupeExam;
	}

	public List<GroupeExamenBean> getGroupeExamSelectioner() {
		return groupeExamSelectioner;
	}

	public void setGroupeExamSelectioner(List<GroupeExamenBean> groupeExamSelectioner) {
		this.groupeExamSelectioner = groupeExamSelectioner;
	}

	public int getIdgroupeexam() {
		return idgroupeexam;
	}

	public void setIdgroupeexam(int idgroupeexam) {
		this.idgroupeexam = idgroupeexam;
	}

	public String getChoixExamen() {
		System.out.println("Dans on choix examen");
		System.out.println("Taille ="+listeExamensTypeRv.getTarget().size());
		return choixExamen;
	}

	public void setChoixExamen(String choixExamen) {
		this.choixExamen = choixExamen;
	}

	public SetDataModel getDiagnosticsDataModel() {
		return diagnosticsDataModel;
	}

	public void setDiagnosticsDataModel(SetDataModel diagnosticsDataModel) {
		this.diagnosticsDataModel = diagnosticsDataModel;
	}
	public void AfficherPlusInfos(){
		System.out.println("Plus d'infoooooooooooooooooooooooos");
		this.plusInfos=true;
	}
	public void AfficherMoinsInfos(){
		System.out.println("Moins d'infoooooooooooooooooooooooos");
		this.plusInfos=false;
	}

	public Boolean getPlusInfos() {
		return plusInfos;
	}

	public void setPlusInfos(Boolean plusInfos) {
		this.plusInfos = plusInfos;
	}

	public String getNewpat() {
		return newpat;
	}

	public void setNewpat(String newpat) {
		this.newpat = newpat;
	}

	public PatientBean getPatientSelectioner() {
		return patientSelectioner;
	}

	public void setPatientSelectioner(PatientBean patientSelectioner) {
		this.patientSelectioner = patientSelectioner;
	}
	 
	public List<DiagnosticBean> getlDiagnostiqueByRv() {
		return lDiagnostiqueByRv;
	}

	public void setlDiagnostiqueByRv(List<DiagnosticBean> lDiagnostiqueByRv) {
		this.lDiagnostiqueByRv = lDiagnostiqueByRv;
	}

	public List<DiagnosticBean> getlDiagnostiqueByRvAn() {
		return lDiagnostiqueByRvAn;
	}

	public void setlDiagnostiqueByRvAn(List<DiagnosticBean> lDiagnostiqueByRvAn) {
		this.lDiagnostiqueByRvAn = lDiagnostiqueByRvAn;
	}

	public List<DiagnosticBean> getlDiagnostiqueByRvCo() {
		return lDiagnostiqueByRvCo;
	}

	public void setlDiagnostiqueByRvCo(List<DiagnosticBean> lDiagnostiqueByRvCo) {
		this.lDiagnostiqueByRvCo = lDiagnostiqueByRvCo;
	}

	public Map<String,String> getTypesClient() {
		IRendezvousServices rvService = (IRendezvousServices) Component.getInstance("rendezvousServices");

		List<TypeClientBean> listTypesClient = new ArrayList<TypeClientBean>();
		listTypesClient = rvService.getTypesClient(); 
		if (!typesClient.isEmpty())
			typesClient.clear();
		for (int i=0; i<listTypesClient.size(); i++ ){
			typesClient.put(listTypesClient.get(i).getLibelle(),""+listTypesClient.get(i).getId());
		}
		
		return typesClient;
	}

	public void setTypesClient(Map<String,String> typesClient) {
		this.typesClient = typesClient;
	}
	
	public void findClientsByType(){
		System.out.println("call to findClientsByType->>>>");
		
		HttpServletRequest request = (HttpServletRequest) extCtx.getRequest();
		IRendezvousServices rvService = (IRendezvousServices) Component.getInstance("rendezvousServices");

		System.out.println("Type Client request: "+request.getParameter("formNouveauPatient:typeClient_input"));
		if (!StringUtil.isNullOrEmpty(request.getParameter("formNouveauPatient:typeClient_input"))){
			listClient = rvService.getClientsByType(new Integer(request.getParameter("formNouveauPatient:typeClient_input")));
			if(!clientAssureurs.isEmpty())
				clientAssureurs.clear();
			for (int i=0; i<listClient.size(); i++ ){
				clientAssureurs.put(listClient.get(i).getDesignation(),""+listClient.get(i).getId());
			}
		}

	}
	
}
