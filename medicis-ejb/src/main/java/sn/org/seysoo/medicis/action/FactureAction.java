package sn.org.seysoo.medicis.action;


import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;

import sn.org.seysoo.medicis.action.interfaces.IFacture;
import sn.org.seysoo.medicis.action.interfaces.IFactureService;
import sn.org.seysoo.medicis.action.interfaces.IGenerateReport;
import sn.org.seysoo.medicis.action.interfaces.IServiceDiagnostic;
import sn.org.seysoo.medicis.entity.BanqueBean;
import sn.org.seysoo.medicis.entity.BeneficiaireBean;
import sn.org.seysoo.medicis.entity.ClientTauxBean;
import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.FactureBean;
import sn.org.seysoo.medicis.entity.FactureClientBean;
import sn.org.seysoo.medicis.entity.FacturePatientBean;
import sn.org.seysoo.medicis.entity.PatientBean;
import sn.org.seysoo.medicis.entity.ReglementBean;
import sn.org.seysoo.medicis.entity.RendezVousBean;
import sn.org.seysoo.medicis.entity.TypeReglementBean;
import sn.org.seysoo.medicis.util.ConstUtil;

import com.jaspersoft.mongodb.MongoDbDataSource;
import com.jaspersoft.mongodb.connection.MongoDbConnection;

@Stateless
@Scope(ScopeType.SESSION)
@Name("factureAction")
public class FactureAction implements IFacture{
	
	@PersistenceContext
	private EntityManager em;
	
	@DataModel(value="listFacturesClient")
	private List<FactureClientBean> listFacturesClient;
	
	@DataModel(value="listFacturesPatient")
	private List<FacturePatientBean> listFacturesPatient;
	
	@DataModelSelection(value="listFacturesClient")
	private FactureClientBean factureClientSelected;
	
	@DataModelSelection(value="listFacturesPatient")
	@Out(required=false)
	private FacturePatientBean facturePatientSelected;
	
	@DataModel(value="listDiagnostic")
	private List<DiagnosticBean> listDiagnostic;
	
	@DataModel(value="listPatients")
	private List<PatientBean> listPatients;
	
	@DataModel(value="listRendezvou")
	private List<RendezVousBean> listRendezvou;
	
	@DataModel(value="listDiagnostiq")
	private List<DiagnosticBean> listDiagnostiq;

	private double totalFC=0.0;
	private double netApayer=0.0;
	private double tauxp;
	
	private Date dateDebutFactureClient;
	private Date dateFinFactureClient;
	private int idclient;
	
	@In
    private FacesMessages facesMessages;

	@In
	Redirect redirect;

	@In(value = "#{facesContext}")
	FacesContext facesContext;

	@In(value = "#{facesContext.externalContext}")
	private ExternalContext extCtx;
	
	@In(create=true,required=false)
	private MongoDbConnection connection;
	
	@In(create = true, required = false)
	@Out(required = false)
	private RendezVousBean rvSelected;
	
	@In(create=true,required=false)
	String date1;

	private double total ;
	private String chiffreEnLettre;
	private double montantDu;
	
	private int choixRegelement;
	private double montantEscpece;
	private double montantCheque;
	private String numeroCheque;
	private String banque;
	private String devise;
	
	//private int etatFact=2;
	
	
	
	public List<FactureClientBean> getListFacturesClient() {
		return listFacturesClient;
	}

	public void setListFacturesClient(List<FactureClientBean> listFacturesClient) {
		this.listFacturesClient = listFacturesClient;
	}

	public List<FacturePatientBean> getListFacturesPatient() {
		return listFacturesPatient;
	}

	public void setListFacturesPatient(List<FacturePatientBean> listFacturesPatient) {
		this.listFacturesPatient = listFacturesPatient;
	}

	public FactureClientBean getFactureClientSelected() {
		return factureClientSelected;
	}

	public void setFactureClientSelected(FactureClientBean factureClientSelected) {
		this.factureClientSelected = factureClientSelected;
	}

	public FacturePatientBean getFacturePatientSelected() {
		return facturePatientSelected;
	}

	public void setFacturePatientSelected(FacturePatientBean facturePatientSelected) {
		this.facturePatientSelected = facturePatientSelected;
	}
	
	//@Produces
	@Factory("listFacturesClient")
	public void listFacturesClient() {
		IFactureService factureService = (IFactureService) Component.getInstance("factureService");
		listFacturesClient = factureService.getFactureClient();
		if (listFacturesClient!=null)
			System.out.println("listFacturesClient  size : " + listFacturesClient.size());
		


	}
	
	//Facturation client
	// Liste des patients et b�n�ficiaires  � partir d'un client donn�
	@Produces	
	@Factory("listDiagnostiq")
	public void infoClients (){
		
		try {
			
			 System.out.println("Dans try info client  :  "); 
			
			List<BeneficiaireBean> listeBeneficiaires = new ArrayList<BeneficiaireBean>();
			
			 listPatients =new ArrayList<PatientBean>();
			 listRendezvou = new ArrayList<RendezVousBean>();
			 listDiagnostiq= new ArrayList<DiagnosticBean>();
			 
	        if (factureClientSelected!=null){
	        	System.out.println("factureClientSelected  N� : "+factureClientSelected.getId());
	        	
	    		Set<ClientTauxBean> listTaux = factureClientSelected.getClient().getClientTauxes(); 
	    		
		   if (listTaux!=null){
			   System.out.println("TTTTTTTTTT taux size  : "+listTaux.size());

			   
			   
			   Iterator<ClientTauxBean> it = listTaux.iterator();
			   while (it.hasNext()) {
				ClientTauxBean clientTauxBean = (ClientTauxBean) it.next();
				Set<BeneficiaireBean> liste = clientTauxBean.getBeneficiaires();

				listeBeneficiaires.addAll(liste);
				
			}
				
			   
			   //size liste des b�n�ficiaires
			    if(listeBeneficiaires!=null){
			        System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbb listeBeneficiaires size  : "+listeBeneficiaires.size());
			        if(listeBeneficiaires.size()>0)
			        	
			        	System.out.println(" beneficiaire  N�: "+listeBeneficiaires.get(0).getId()+" " +listeBeneficiaires.get(0).getPrenom()+ "    " +listeBeneficiaires.get(0).getNom());
			    	
			    }
			   //if (listeBeneficiaires.size()>0)
			   //System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbb beneficiaire  : "+listeBeneficiaires.get(0).getPrenom()+ "    " +listeBeneficiaires.get(0).getNom());

				
				for (int i = 0; i < listeBeneficiaires.size(); i++) {
					
					Set<PatientBean> liste =  listeBeneficiaires.get(i).getPatients();
					listPatients.addAll(liste);
				}
	          
				//Size liste des patients
				if (listPatients!=null){
				  System.out.println("pppppppppppppppppp  *************listPatients size  xxxxxx :  " +listPatients.size());
				    if(listPatients.size()>0)
					    System.out.println(" patient N�: "+listPatients.get(0).getId()+" " +listPatients.get(0).getPrenom()+" " +listPatients.get(0).getNom());

				}
				
				for (int i = 0; i < listPatients.size(); i++) {
					
					Set<RendezVousBean> liste =  listPatients.get(i).getRendezVous();
					listRendezvou.addAll(liste);
				}
				
				//size liste des rendezvous
				if (listRendezvou!=null){
					System.out.println("RRRRRRRRRR  size  rendezvous  :    "  +listRendezvou.size()); 
					  if(listRendezvou.size()>0)
						  System.out.println(" rendezvous N�:  " +listRendezvou.get(0).getId());

				}
				

				for (int i = 0; i < listRendezvou.size(); i++) {
					
					Set<DiagnosticBean> liste =  listRendezvou.get(i).getDiagnostics();
					listDiagnostiq.addAll(liste);
					
				}
				
	          
				//size liste des diagnostiques
				if (listDiagnostiq!=null){
					System.out.println("infoClients--- >  size diagssssssssssssssss     :     " +listDiagnostiq.size());
					if (listDiagnostiq.size()>0)
						  System.out.println(" diagnostique N�:  " +listDiagnostiq.get(0).getId());

					
				}
				
				//if (listDiagnostiq!=null)
				if (listDiagnostiq.size()>0){

					totalFC=0.0;
					for (int i = 0; i < listDiagnostiq.size(); i++){ 
						totalFC=totalFC+listDiagnostiq.get(i).getExamen().getPrix().doubleValue();
						
						System.out.println(" examen [" + i +"] = "+ listDiagnostiq.get(i).getExamen().getPrix().doubleValue());
					}
				}
				
				netApayer= totalFC - factureClientSelected.getFacture().getRemise();
				
				System.out.println("netApayer   :  "+netApayer);
				System.out.println("totalFC   :  "+totalFC);
				
						//totalFC - factureClientSelected.getFacture().getRemise();
				
	         }

		   }else
			   
				System.out.println("factureClientSelected nullllllllllllllll");
 
	        

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("dans catch info client :");
			e.printStackTrace();
		}

	}
	
	// Liste des factures patients
	@Produces
	@Factory("listFacturesPatient")
	public void listFacturesPatient() {
		
		IFactureService factureService = (IFactureService) Component.getInstance("factureService");
		listFacturesPatient = factureService.listFacturesPatient();
		
		if (listFacturesPatient!=null)
			System.out.println("size liste facture patient : "+listFacturesPatient.size()); 
		
			
	}

	//@Produces
	//@Factory("listDiagnostic")
	//Liste des diagnostiques � partir d'un rv donn�
	public void listDiagnostic() {
		IFactureService factureService = (IFactureService) Component.getInstance("factureService");
		
		listDiagnostic = null;
		tauxp=0.0;
		
		if (facturePatientSelected!=null){
			
		    tauxp=100-facturePatientSelected.getRendezVous().getPatient().getBeneficiaire().getClientTaux().getTaux();

		    listDiagnostic = factureService.getDiagnostics(facturePatientSelected.getRendezVous());
		      total=0.0;
		      for (int k=0;k<listDiagnostic.size();k++){
		    	  
		    	  total += listDiagnostic.get(k).getExamen().getPrix().doubleValue();
		    	  
		      }
		      
		      long chiffre = (long)total;
		      chiffreEnLettre=ConvertirEnLettre.convert(chiffre); 
		      montantDu=0.0;
		      if (total !=0)
		    	  montantDu = total * (tauxp/100);
		      else
		    	  montantDu=0.0;
		      
		      System.out.println("montant duuuuuuuuu = "+montantDu); 
		}
		
		if (listDiagnostic!=null)
			System.out.println("size liste diagggggggggg : "+listDiagnostic.size()); 
		
	}
	public void imprimerPDFPatient(){
		try{
			if(facturePatientSelected!=null){
				IServiceDiagnostic diagnosticService = (IServiceDiagnostic) Component.getInstance("diagnosticService");
				IGenerateReport generateReport = (IGenerateReport) Component.getInstance("GenerateReport");
				System.out.println("idfacture = "+facturePatientSelected.getId()+" et Montannnt = "+facturePatientSelected.getFacture().getMontant());
				Map<String, Object> parameters = new HashMap<String, Object>();
				String PATH_JR_FACTUREPATIENT = ApplicationContexteAction.getRealPathJasper() + "facturepatient.jrxml";
				String fichierPdfFACTUREPATIENT = ApplicationContexteAction.getRealPathPdf() + "facturepatient"+facturePatientSelected.getId()+".pdf";
				List<DiagnosticBean> diagnostic = diagnosticService.findDiagnosticsRendezVous(facturePatientSelected.getRendezVous().getId());
		    	Double montant=0.0;
		    	for (DiagnosticBean diagnosticBean : diagnostic) {
		    		if(diagnosticBean.getPartInteresse()!=null)
					montant+=diagnosticBean.getPartInteresse();
				}
				parameters.put(MongoDbDataSource.CONNECTION, connection);
		        parameters.put("numfacture",facturePatientSelected.getId());
		        parameters.put("datefacture",date1);
		        parameters.put("matricule",facturePatientSelected.getRendezVous().getPatient().getId());
		        parameters.put("pathlogo",ApplicationContexteAction.getRealPathlogo());
		        parameters.put("idRendezvous",facturePatientSelected.getRendezVous().getId());
		        parameters.put("montant",montant);
		        parameters.put("remise",facturePatientSelected.getFacture().getRemise());
		        parameters.put("netapayer",montant-facturePatientSelected.getFacture().getRemise());
		        parameters.put("idpatient",facturePatientSelected.getRendezVous().getPatient().getId());
		        parameters.put("REPORT_CONNECTION",connection);
				generateReport.exportReportToPdfFile(PATH_JR_FACTUREPATIENT, fichierPdfFACTUREPATIENT, parameters);
				System.out.println(fichierPdfFACTUREPATIENT);
				HttpServletResponse reponse = (HttpServletResponse) extCtx.getResponse();
				ServletOutputStream os=reponse.getOutputStream();
				reponse.setContentType("application/pdf");
				reponse.addHeader("Content-disposition", "attachment; filename=\"" + "facture.pdf"+"\"");
				FileInputStream in = new FileInputStream(fichierPdfFACTUREPATIENT);
				int c;
				while((c=in.read())!= -1){
					os.write(c);
					}
				os.flush();
				os.close();
				facesContext.responseComplete();
				}
				else
					System.out.println("idfacture est NULLLLLLLLLLLLLLLLLLL");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String genegerePDFPatient(){
		System.out.println("D�but------------ g�nerer facture patient");
		IGenerateReport generateReport = (IGenerateReport) Component.getInstance("GenerateReport");
		IFactureService factureService = (IFactureService) Component.getInstance("factureService");
		IServiceDiagnostic diagnosticService = (IServiceDiagnostic) Component.getInstance("diagnosticService");
		Map<String, Object> parameters = new HashMap<String, Object>();
		String PATH_JR_FACTUREPATIENT = ApplicationContexteAction.getRealPathJasper() + "facturepatient.jrxml";
		String fichierPdfFACTUREPATIENT = ApplicationContexteAction.getRealPathPdf() + "facturepatient"+rvSelected.getId()+".pdf";
		System.out.println("Rendez vous selected = "+rvSelected.getId());
        try {
        	
        	FactureBean facture=new FactureBean();
        	FacturePatientBean facturepatient=new FacturePatientBean();
        	System.out.println("D�but------------ insertion facture ");
        	
        	List<DiagnosticBean> diagnostic = diagnosticService.findDiagnosticsRendezVous(rvSelected.getId());
        	Double montant=0.0;
        	for (DiagnosticBean diagnosticBean : diagnostic) {
				montant+=diagnosticBean.getExamen().getPrix();
			}
        	facture.setDate(new Date());
        	facture.setEtatReglement(0);
        	facture.setMontant(montant);
        	facture.setRemise(0.0);
        	facture.setNumero(""+facture.getId());
        	facture.setEtatFacture(0);
        	facture=factureService.EnregistrerFacture(facture);
        	
        	
        	
        	System.out.println("fin ------------ insertion facture ID = "+facture.getId());
        	
        	facturepatient.setFacture(facture);
        	facturepatient.setRendezVous(rvSelected);
        	facturepatient=factureService.EnregistrerFacturePatient(facturepatient);
        	
			parameters.put(MongoDbDataSource.CONNECTION, connection);
	        parameters.put("numfacture",facture.getId());
	        parameters.put("datefacture",date1);
	        parameters.put("matricule",rvSelected.getPatient().getId());
	        parameters.put("pathlogo",ApplicationContexteAction.getRealPathlogo());
	        parameters.put("idRendezvous",rvSelected.getId());
	        parameters.put("montant",montant);
	        parameters.put("remise",facture.getRemise());
	        parameters.put("netapayer",montant-facture.getRemise());
	        parameters.put("idpatient",rvSelected.getPatient().getId());
	        parameters.put("REPORT_CONNECTION",connection);
			generateReport.exportReportToPdfFile(PATH_JR_FACTUREPATIENT, fichierPdfFACTUREPATIENT, parameters);
			System.out.println(fichierPdfFACTUREPATIENT);
			HttpServletResponse reponse = (HttpServletResponse) extCtx.getResponse();
			ServletOutputStream os=reponse.getOutputStream();
			reponse.setContentType("application/pdf");
			reponse.addHeader("Content-disposition", "attachment; filename=\"" + "facture.pdf"+"\"");
			FileInputStream in = new FileInputStream(fichierPdfFACTUREPATIENT);
			int c;
			while((c=in.read())!= -1){
				os.write(c);
				}
			os.flush();
			os.close();
			facesContext.responseComplete();
			System.out.println("fin-------------------g�nerer facture patient");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
        return fichierPdfFACTUREPATIENT;
	}
	
	//paiement facture patient
	public void saveFactureP (){
		
		System.out.println("************** saveFactureP  ********* " );
		
		FactureBean facture = facturePatientSelected.getFacture();
		ReglementBean reglement = new ReglementBean();
		TypeReglementBean typeReglement = new TypeReglementBean();
		BanqueBean banques = new BanqueBean();
		
		System.out.println("choixRegelement = " +choixRegelement);

		if (choixRegelement==1)
		{
			//Espece
			typeReglement.setLibelle(ConstUtil.REGLEMENT_ESPECE);
			//typeReglement.setDevise(devise);
			typeReglement=em.merge(typeReglement);
			
			reglement.setDate(new Date());
			reglement.setFacture(facture);
			reglement.setMontant(montantEscpece);
			reglement.setTypeReglement(typeReglement);
			reglement.setDevise(devise);

			reglement=em.merge(reglement);
			
			System.out.println("************  montant Du ************  :  "  +montantDu); 
			System.out.println("************  montant Espece ************  :  "  +montantEscpece); 
			if (montantDu-montantEscpece==0){
				facture.setEtatFacture(ConstUtil.FACTURE_REGLE);
				montantDu=0.0;
			}

			else
				facture.setEtatFacture(ConstUtil.FACTURE_NONREGLE);

			
			facture=em.merge(facture);
			
			
			
			
		}else if (choixRegelement==2) {
			//Cheque
			
			
			
			typeReglement.setLibelle(ConstUtil.REGLEMENT_CHEQUE);
			//typeReglement.setBanque(banque);
			//typeReglement.setNumcheque(numeroCheque);
			typeReglement=em.merge(typeReglement);
			
			banques.setNomBanque(banque);
			banques=em.merge(banques);
			
			reglement.setDate(new Date());
			reglement.setFacture(facture);
			reglement.setMontant(montantCheque);
			reglement.setTypeReglement(typeReglement);
			reglement.setNumcheque(numeroCheque);
			reglement.setBanque(banques);

			reglement=em.merge(reglement);
			
			System.out.println("************  montantDu ************  :  "  +montantDu); 
			if (montantDu-montantCheque==0){
				facture.setEtatFacture(ConstUtil.FACTURE_REGLE);
				montantDu=0.0;
			}

			else
				facture.setEtatFacture(ConstUtil.FACTURE_NONREGLE);

			
			facture=em.merge(facture);
			
			}
		else{
			facesMessages.add("Choisissez le type de paiement: cheque ou espece");
			System.out.println("elseeeeeeeeee  choixRegelement   =   "+  choixRegelement);  
		}
	}

	
	public void genereFactureClient(){
		try{
			System.out.println("D�but G�nerer facture Client !!!");
			//R�cup�ration de la liste des diagnostics pour le client � un p�riode donn�es.
			IFactureService factureService = (IFactureService) Component.getInstance("factureService");
			IGenerateReport generateReport = (IGenerateReport) Component.getInstance("GenerateReport");
			List<DiagnosticBean> diagnostic = factureService.getDiagnosticClientParPeriode(idclient, dateDebutFactureClient, dateFinFactureClient);
			FactureBean facture=new FactureBean();
			FactureClientBean factureClient=new FactureClientBean();
			//Calcul du montant de la facture.
	    	Double montant=0.0;
	    	for (DiagnosticBean diagnosticBean : diagnostic) {
				montant+=diagnosticBean.getExamen().getPrix();
			}
	    	//Cr�ation de l'objet Facture
	    	facture.setDate(new Date());
	    	facture.setEtatReglement(0);
	    	facture.setMontant(montant);
	    	facture.setRemise(0.0);
	    	facture.setEtatFacture(0);
	    	facture=factureService.EnregistrerFacture(facture);
	    	facture.setNumero(""+facture.getId());
	    	facture=factureService.EnregistrerFacture(facture);

	    	
	    	factureClient.setFacture(facture);
	    	factureClient.setClient(factureService.findClientById(idclient));
	    	factureClient.setDateDebut(dateDebutFactureClient);
	    	factureClient.setDateFin(dateFinFactureClient);
	    	factureClient.setTermeYn(0);
	    	factureClient=factureService.EnregistrerFactureClient(factureClient);
			System.out.println("Taille diagnostic="+diagnostic.size());
			System.out.println("Id Facture ="+factureClient.getId());
			System.out.println("Id Facture Client ="+factureClient.getId());
			System.out.println("Fin G�nerer facture Client !!!");
			System.out.println("Fin g�neration facture");
			
			System.out.println("d�but cr�ation pdfffffffffffffff");
			String PATH_JR_FACTURECLIENT = ApplicationContexteAction.getRealPathJasper() + "factureclient.jrxml";
			String fichierPdfFACTURECLIENT = ApplicationContexteAction.getRealPathPdf() + "factureclient"+idclient+".pdf";
			String periode;
			Date datedeb = dateDebutFactureClient;
			Date datefin = dateFinFactureClient;
			SimpleDateFormat formater = null;
			DateFormat fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
			DateFormat fullDateFormat1 = DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
			fullDateFormat.format(datedeb);
			fullDateFormat1.format(datefin);
			formater = new SimpleDateFormat("dd MMMM yyyy");
			periode=formater.format(datedeb)+" � "+formater.format(datefin);
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(MongoDbDataSource.CONNECTION, connection);
			parameters.put("idclient",idclient);
	        parameters.put("numfacture",facture.getId());
	        parameters.put("datefacture",date1);
	        parameters.put("pathlogo",ApplicationContexteAction.getRealPathlogo());
	        parameters.put("periode",periode);
	        parameters.put("StartDate",dateDebutFactureClient);
	        parameters.put("EndDate",dateFinFactureClient);
	        parameters.put("montant",montant);
	        parameters.put("remise",facture.getRemise());
	        parameters.put("netapayer",montant-facture.getRemise());
	        parameters.put("REPORT_CONNECTION",connection);
	        generateReport.exportReportToPdfFile(PATH_JR_FACTURECLIENT, fichierPdfFACTURECLIENT, parameters);
			System.out.println(fichierPdfFACTURECLIENT);
			HttpServletResponse reponse = (HttpServletResponse) extCtx.getResponse();
			ServletOutputStream os=reponse.getOutputStream();
			reponse.setContentType("application/pdf");
			reponse.addHeader("Content-disposition", "attachment; filename=\"" + "factureClient.pdf"+"\"");
			FileInputStream in = new FileInputStream(fichierPdfFACTURECLIENT);
			int c;
			while((c=in.read())!= -1){
				os.write(c);
				}
			os.flush();
			os.close();
			facesContext.responseComplete();
			System.out.println("fin-------------------g�nerer facture patient");
			System.out.println("Fin cr�ation pdfffffffffffffff");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
    @Remove
    public void remove() {
    }

	public List<DiagnosticBean> getListDiagnostic() {
		return listDiagnostic;
	}

	public void setListDiagnostic(List<DiagnosticBean> listDiagnostic) {
		this.listDiagnostic = listDiagnostic;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getChiffreEnLettre() {
		return chiffreEnLettre;
	}

	public void setChiffreEnLettre(String chiffreEnLettre) {
		this.chiffreEnLettre = chiffreEnLettre;
	}

	public double getMontantDu() {
		return montantDu;
	}

	public void setMontantDu(double montantDu) {
		this.montantDu = montantDu;
	}

	public int getChoixRegelement() {
		return choixRegelement;
	}

	public void setChoixRegelement(int choixRegelement) {
		this.choixRegelement = choixRegelement;
	}

	public double getMontantEscpece() {
		return montantEscpece;
	}

	public void setMontantEscpece(double montantEscpece) {
		this.montantEscpece = montantEscpece;
	}

	public double getMontantCheque() {
		return montantCheque;
	}

	public void setMontantCheque(double montantCheque) {
		this.montantCheque = montantCheque;
	}

	public String getNumeroCheque() {
		return numeroCheque;
	}

	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	public String getBanque() {
		return banque;
	}

	public void setBanque(String banque) {
		this.banque = banque;
	}

	public String getDevise() {
		return devise;
	}

	public void setDevise(String devise) {
		this.devise = devise;
	}

	public List<PatientBean> getListPatients() {
		return listPatients;
	}

	public void setListPatients(List<PatientBean> listPatients) {
		this.listPatients = listPatients;
	}

	public List<RendezVousBean> getListRendezvou() {
		return listRendezvou;
	}

	public void setListRendezvou(List<RendezVousBean> listRendezvou) {
		this.listRendezvou = listRendezvou;
	}

	

	public List<DiagnosticBean> getListDiagnostiq() {
		return listDiagnostiq;
	}

	public void setListDiagnostiq(List<DiagnosticBean> listDiagnostiq) {
		this.listDiagnostiq = listDiagnostiq;
	}

	public double getTotalFC() {
		return totalFC;
	}

	public void setTotalFC(double totalFC) {
		this.totalFC = totalFC;
	}

	public double getNetApayer() {
		return netApayer;
	}

	public void setNetApayer(double netApayer) {
		this.netApayer = netApayer;
	}

	public Date getDateDebutFactureClient() {
		return dateDebutFactureClient;
	}

	public void setDateDebutFactureClient(Date dateDebutFactureClient) {
		this.dateDebutFactureClient = dateDebutFactureClient;
	}

	public Date getDateFinFactureClient() {
		return dateFinFactureClient;
	}

	public void setDateFinFactureClient(Date dateFinFactureClient) {
		this.dateFinFactureClient = dateFinFactureClient;
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}

	public double getTauxp() {
		return tauxp;
	}

	public void setTauxp(double tauxp) {
		this.tauxp = tauxp;
	}
	
    
}
