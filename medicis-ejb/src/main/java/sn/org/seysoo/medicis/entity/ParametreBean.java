/**
 * 
 * Author Tidiane
 */
package sn.org.seysoo.medicis.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.jboss.seam.annotations.Name;
import org.jboss.solder.core.Veto;

/**
 * @author Tidiane
 *
 */
@Entity
@Veto
@Indexed
@Table(name = "parametre")
@Name("parametreBean")
public class ParametreBean implements Serializable {
	@Field
	private Integer id;
	private String code;
	private Integer valeur;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="code",unique=true)
	@NotNull
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="valeur")
	public Integer getValeur() {
		return valeur;
	}
	public void setValeur(Integer valeur) {
		this.valeur = valeur;
	}
	
	

}
