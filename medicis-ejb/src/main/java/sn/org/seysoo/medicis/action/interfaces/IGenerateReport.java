package sn.org.seysoo.medicis.action.interfaces;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

 /**
 * @author OmarGueye
 * @version
 */
@Local
public interface IGenerateReport {
	
	public void exportReportToPdfFile(String fichierIn, String fichierOut, Map params);
	
	public void viewReportPdf(String fichierIn, Map params, String name);
	
	public void downloadReportPdf(String fichierIn, Map params, String name);
	
	public List<byte[]> exportReportToPdfByte(String fichierIn, Map params);
	
	public void combineToDownloadReportsPdf(List<byte[]> reports, String fileName) throws Exception;
	
	public void combineToGerateFileReportsPdf(List<byte[]> reports, String fichierOut) throws Exception;
	
	public void destroy();
}