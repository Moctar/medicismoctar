package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import sn.org.seysoo.medicis.entity.BanqueBean;

@Local
public interface IBanque {

	public List<BanqueBean> getListBanques();

	public void setListBanques(List<BanqueBean> listBanques) ;
	
	public Map<String,String> getBanques();
	
	public void setBanques(Map<String,String> banques);

}
