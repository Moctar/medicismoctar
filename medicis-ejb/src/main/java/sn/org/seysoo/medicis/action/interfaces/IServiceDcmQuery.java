package sn.org.seysoo.medicis.action.interfaces;

public interface IServiceDcmQuery {
	public String query(String remoteAE, String hostname, int port);
	 public void remove();
}
