package sn.org.seysoo.medicis.action.interfaces;

import java.util.List;
import java.util.Map;

import sn.org.seysoo.medicis.entity.DiagnosticBean;
import sn.org.seysoo.medicis.entity.ExamenBean;
import sn.org.seysoo.medicis.entity.GroupeExamenBean;
import sn.org.seysoo.medicis.entity.ImageBean;
import sn.org.seysoo.medicis.entity.InterpretationBean;
import sn.org.seysoo.medicis.entity.InterpretationTypeBean;
import sn.org.seysoo.medicis.entity.MedecinBean;
import sn.org.seysoo.medicis.entity.SalleBean;
import sn.org.seysoo.medicis.entity.TechniqueUtiliseBean;

public interface IServiceDiagnostic {
	public DiagnosticBean findDiagnostic(Integer id);

	public List<DiagnosticBean> findDiagnosticsRendezVous(int idRv);

	public void saveDiagnostic(DiagnosticBean diagnostic);

	public List<ExamenBean> findExamensType(int idType, String service);

	public List<GroupeExamenBean> findListeTypesNonIrm();
	
	public ExamenBean findExamen(int id);

	public List<MedecinBean> findMedecinsService(String service);

	public List<SalleBean> findSallesService(String service);

	public ExamenBean findExamenIrm();
	
	public void saveInterpretations(InterpretationBean interprete);
	
	public Map<String, List<DiagnosticBean>> listeDiagnostics(int idPatient,int anterieur);

	public List<ImageBean> findListeImagesDiagnostic();
	
	public DiagnosticBean dernierDiagnosticIrm(int idPatient);

	public void destroy();

	public void supprimerDiagnostic(DiagnosticBean diagnosticBean);

	public ImageBean findImage(Integer id);
	
	public List<GroupeExamenBean> findgroupeExamService(int service);
	
	public List<ExamenBean> findExamensType2(List<GroupeExamenBean> groupe, String service);
	
	public List<InterpretationTypeBean> findInterpretationTypeService(String service);

	public List<TechniqueUtiliseBean> findTechniquesService(String service);

	public InterpretationTypeBean findInterpretationType(
			int id);;
}
