/**
 * 
 */
package sn.org.seysoo.medicis.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import sn.org.seysoo.medicis.action.interfaces.IChart;
import sn.org.seysoo.medicis.entity.CaracteristiquesBean;
import sn.org.seysoo.medicis.util.ConstUtil;
import sn.org.seysoo.medicis.util.ConvertUtil;
import sn.org.seysoo.medicis.util.DateUtil;

/**
 * @author OmGue
 *
 */

@Stateful
@Scope(ScopeType.SESSION)
@Name("chartAction")
public class ChartAction implements IChart {

	private CartesianChartModel categoryModel;

	private int indiceMax;
	
	private int indiceMin;
	
	private String categoryTitle;
	
	public ChartAction() {
		initChart();
	}

	/**
	 * initChart()
	 * 
	 * Initialisation du graphique à vide.
	 */
	public void initChart() {
		//System.out.println("---initChart - Aucun---");
		categoryModel = new CartesianChartModel();
		indiceMax = 0;
		indiceMin = 0;
		categoryTitle = "Aucun";
		
		ChartSeries variable = new ChartSeries();  
        variable.setLabel("Variable");  
        variable.set("0", 0);  
        categoryModel.addSeries(variable); 
  
		ChartSeries normale = new ChartSeries();  
        normale.setLabel("Normale");
        normale.set("0", 0);  
        categoryModel.addSeries(normale);  
	}
	
	/**
	 * initCharte(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract)
	 * 
	 * Initialisation du graphique avec la courbe de valeurs de la taille
	 */
	public void initCharte(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract) {
		//System.out.println("---initChart - Taille---");
		//createCategoryTaille(caractList);
		createCategoryModel(caractList, constantesList, title, min, max, typeCaract);
	}

	/**
	 * createCategoryModel(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract)
	 * 
	 * création des courbes d'évolution des différentes caractéristiques à partir d'une de valeurs
	 */
	public void createCategoryModel(List<CaracteristiquesBean> caractList, List constantesList, String title, int min, int max, int typeCaract) {
		if(caractList!=null && caractList.size()!=0) {
			System.out.println("---"+title+" nb : "+caractList.size());
			categoryModel = new CartesianChartModel();
			indiceMax = max;
			indiceMin = min;
			categoryTitle = title;
			int start = 0;
			if(caractList.size()>4)
				start = caractList.size()-4;
			
	        ChartSeries variable = new ChartSeries();
	        variable.setLabel("Prises");
	        
	        ChartSeries variable2 = new ChartSeries();
	        if(typeCaract == ConstUtil.CODE_TYPE_CARACT_PA) {
	        	variable2.setLabel("Prises2");
	        }

	        int k = 0;
	        
	        for(int i=start; i<caractList.size(); i++) {
	        	k++;
	        	CaracteristiquesBean caract = caractList.get(i);
	        	String x_value = ""+k+"-"+ConvertUtil.convertToString(caract.getDate(), ConstUtil.DATE_PATTERN_DDMM);
	        	variable.set(x_value, caract.getValeur());
	        	if(typeCaract == ConstUtil.CODE_TYPE_CARACT_PA) {
	        		variable2.set(x_value, caract.getValeur2());
	        	}
	        }
	  
	        categoryModel.addSeries(variable);
	        if(typeCaract == ConstUtil.CODE_TYPE_CARACT_PA) {
	        	categoryModel.addSeries(variable2);
	        }
		}
		else {
			initChart();
		}
	}

	@Destroy
	@Remove
	public void destroy(){
	}
	
	/* getters and setters */
	
	public CartesianChartModel getCategoryModel() {
		return categoryModel;
	}

	public int getIndiceMax() {
		return indiceMax;
	}

	public void setIndiceMax(int indiceMax) {
		this.indiceMax = indiceMax;
	}

	public int getIndiceMin() {
		return indiceMin;
	}

	public void setIndiceMin(int indiceMin) {
		this.indiceMin = indiceMin;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}
}
