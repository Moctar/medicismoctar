package sn.org.seysoo.medicis.util;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import sn.org.seysoo.medicis.action.ApplicationContexteAction;

/**
 * @author OmarGueye
 * @version
 *
 * Classe utilitaire des constantes
 */
public class ConstUtil {

	public static final String      SPACE                           = " ";

	public static final String      SLASH                           = " / ";

	public static final String      SLASHNOSPACE            = "/";

	public static final String      UNDERSCORE                      = " _ ";

	public static final String      UNDERSCORENOSPACE       = "_";

	public static final String      TIRET                           = " - ";

	public static final String      TIRETNOSPACE            = "-";

	public static final String      DATE_PATTERN            = "dd/MM/yyyy";

	public static final String      DATE_FORMAT_US          = "MM/dd/yyyy";

	public static final String      DATE_FORMAT_ISO         = "yyyy-MM-dd";

	public static final String      DATE_FORMAT_ISO_NO              = "yyyyMMdd";

	public static final String      TIME_PATTERN            = "HH:mm";

	public static final String      DATE_TIME_SEC_PATTERN = "dd/MM/yyyy HH:mm:ss";

	public static final String      DATE_TIME = "dd/MM/yyyy HH:mm";

	public static final String      DATE_JMAH_COMPLET  = "EEEE d MMMM yyyy - HH:mm";
	
	public static final String      DATE_PATTERN_yyyy            = "yyyy";
	
	public static final String      DATE_PATTERN_MMYY            = "MM/yy";
	
	public static final String      DATE_PATTERN_DDMM            = "dd/MM";
	
	public static final String      DATE_PATTERN_EEEE            = "EEEE";

	public static ResourceBundle bundle = ApplicationContexteAction.getBundle();


	public static final int ETAT_DIAGNOSTIC_OUVERT = 0;
	
	public static final int ETAT_DIAGNOSTIC_FERME = 1;

        public static final String USER_ALF = bundle.getString("USER_ALF");                             // serveur dev
        public static final String HOST_ALF = bundle.getString("HOST_ALF");
        public static final String PORT_ALF = bundle.getString("PORT_ALF");
        public static final String PWD_ALF = bundle.getString("PWD_ALF");
        public static final String dossCrenaux="creneauxporteur";
        // Types de fichiers
        public static final String FICHIER_PDF = "pdf";
        public static final String FICHIER_JPEG = "jpg";
        public static final String FICHIER_EXCEL = "xls";
        public static final String FICHIER_EXCELX = "xlsx";
        public static final String FICHIER_PNG = "png";
        public static final String FICHIER_BMP = "bmp";
        public static final String FICHIER_TIF = "tif";
        public static final String FICHIER_CSV = "csv";
        public static final String URI_OPEN_FILE_ALF = "/alfresco/d/d/workspace/SpacesStore/";
        public static final int DEUX_MEGABYTE = 1048576*2;
        public static final String allowedTypes[] = new String[]{"pdf", "jpg","jpeg", "xls", "xlsx", "png", "bmp", "tif"};
        
        public static final String SERVICE_ODONTOLOGIE = "odontologie";
        public static final String SERVICE_RADIOLOGIE = "radiologie";
        public static final String SERVICE_GYNECOLOGIE = "gynecologie";
        
        public static final String REGLEMENT_ESPECE = "espece";
        public static final String REGLEMENT_CHEQUE = "cheque";
        
        
        public static int  FACTURE_REGLE = 1;
        public static int  FACTURE_NONREGLE = 0;
        public static String ETAT_RENDEZVOUS_DEFAUT="defaut";
        public static String ETAT_RENDEZVOUS_CONFIRME="confirme";
        public static String ETAT_RENDEZVOUS_ANNULE="annule";
        
//      Code parametres
        public static String CodeParaDernierNumDossier = "numDoss";
        
        
        
        public static boolean isAllowed(String type)
        {
                ArrayList<String> list = new ArrayList<String>();
                for (int i = 0; i < allowedTypes.length; i++) {
                        list.add(allowedTypes[i]);
                }
                
                return list.contains(type);
        }
        
        private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
          private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

		public static final String LIBELLE_IRM = "IRM";

          public static String toSlug(String input) {
            String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
            String normalized = Normalizer.normalize(nowhitespace, Form.NFD);
            String slug = NONLATIN.matcher(normalized).replaceAll("");
            return slug.toLowerCase(Locale.ENGLISH);
          }
          
    // SESSION TIMEOUT (en secondes -> 10 minutes)
    public static final int SESSION_TIMEOUT = 600;
    
    // Codes des types de caracteristiques d'un patient
    public static final int CODE_TYPE_CARACT_TAILLE = 1;
    public static final int CODE_TYPE_CARACT_TEMPERATURE = 2;
    public static final int CODE_TYPE_CARACT_POIDS = 3;
    public static final int CODE_TYPE_CARACT_PA = 4;
    public static final int CODE_TYPE_CARACT_POULS = 5;
    
}